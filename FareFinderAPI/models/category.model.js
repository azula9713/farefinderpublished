const { DataTypes, STRING } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        categoryName: { type: DataTypes.STRING, allowNull: false },
        categoryDescription: { type: DataTypes.STRING, allowNull: true }
    };

    return sequelize.define('Category', attributes);
}