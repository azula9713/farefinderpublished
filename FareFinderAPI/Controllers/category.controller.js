const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../middleware/validate_request');
const categoryService = require('../service/category.service');

const prefix = '/category'
    // routes
router.post(prefix + '/create', createSchema, create);
router.get(prefix + '/all', getAllCategories);
router.get(prefix + '/current', getCurrent);
router.get(prefix + '/:id', getById);
router.put(prefix + '/:id', updateSchema, update);
router.delete(prefix + '/:id', _delete);

module.exports = router;

function createSchema(req, res, next) {
    const schema = Joi.object({
        categoryName: Joi.string().required(),
        categoryDescription: Joi.string()
    });
    validateRequest(req, next, schema);
}

function create(req, res, next) {
    console.log(req.body);
    categoryService.create(req.body)
        .then(() => res.json({ message: 'Creating Category successful' }))
        .catch(next);
}

function getAllCategories(req, res, next) {
    categoryService.getAll()
        .then(category => res.json(category))
        .catch(next);
}

function getCurrent(req, res, next) {
    res.json(req.category);
}

function getById(req, res, next) {
    categoryService.getById(req.params.id)
        .then(category => res.json(category))
        .catch(next);
}

function updateSchema(req, res, next) {
    const schema = Joi.object({
        categoryName: Joi.string().empty(''),
        categoryDescription: Joi.string().empty(''),
    });
    validateRequest(req, next, schema);
}

function update(req, res, next) {
    categoryService.update(req.params.id, req.body)
        .then(category => res.json(category))
        .catch(next);
}

function _delete(req, res, next) {
    categoryService.delete(req.params.id)
        .then(() => res.json({ message: 'Category deleted successfully' }))
        .catch(next);
}