const express = require("express");
const router = express.Router();
const Joi = require("joi");
const validateRequest = require("../middleware/validate_request");
const postService = require("../service/post.service");

const prefix = "/post";

// routes
router.post(prefix + "/create", createSchema, create);
router.get(prefix + "/all", getAllPosts);
router.get(prefix + "/current", getCurrent);
router.get(prefix + "/:id", getById);
router.put(prefix + "/:id", updateSchema, update);
router.delete(prefix + "/:id", _delete);
router.get(prefix + "/findby/q", getByCategory);

module.exports = router;

function createSchema(req, res, next) {
  const schema = Joi.object({
    city: Joi.string().required(),
    country: Joi.string().required(),
    category: Joi.string().required(),
    // description: Joi.string(),
    // img: Joi.string(),
  });
  validateRequest(req, next, schema);
}

function create(req, res, next) {
  console.log(req.body);
  postService
    .create(req.body)
    .then(() => res.json({ message: "Creating Post successful" }))
    .catch(next);
}

function getAllPosts(req, res, next) {
  postService
    .getAll()
    .then((post) => res.json(post))
    .catch(next);
}

function getCurrent(req, res, next) {
  res.json(req.post);
}

function getById(req, res, next) {
  postService
    .getById(req.params.id)
    .then((post) => res.json(post))
    .catch(next);
}

function updateSchema(req, res, next) {
  const schema = Joi.object({
    city: Joi.string().empty(""),
    country: Joi.string().empty(""),
    category: Joi.string().empty(""),
    description: Joi.string().empty(""),
    img: Joi.string().empty(""),
  });
  validateRequest(req, next, schema);
}

function update(req, res, next) {
  postService
    .update(req.params.id, req.body)
    .then((post) => res.json(post))
    .catch(next);
}

function _delete(req, res, next) {
  postService
    .delete(req.params.id)
    .then(() => res.json({ message: "Post deleted successfully" }))
    .catch(next);
}

async function getByCategory(req, res, next) {
  queryparam = req.query.category;
  postService
    .getPostByCat(queryparam)
    .then((post) => res.json(post))
    .catch(next);
}
