'use strict';

var express = require('express');
var app = express();
var cors = require('cors');
var Logger = require('./Logger').Logger;
var bodyParser = require("body-parser");
var prefix = '/farefinders.api';
var exphbs = require('express-handlebars');
const path = require('path');
var port = process.env.port | 3001;

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var corsOptions = {
    origin: 'http://localhost:3001',
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    preflightContinue: true,
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors());
app.options("*", cors());
// Add headers
app.use(function(req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});



try {
    // app.set('view engine', 'html');
    // app.engine('html', require('hbs').__express);
    // app.use(express.static('public'));
    // app.get('*', (req, res) => {
    //     res.render('/index.html')
    // })
   
    app.use(prefix, require('./Controllers/HomeRoute'));
    app.use(prefix, require('./Controllers/Flight'));
    app.use(prefix, require('./Controllers/AirLine'));
    app.use(prefix, require('./Controllers/AirPort'));
    app.use(prefix, require('./Controllers/user.controller'));
    app.use(prefix, require('./Controllers/category.controller'));
    app.use(prefix, require('./Controllers/post.controller'));
    app.use(prefix, require('./Controllers/destination.controller'));

    app.use(express.static(path.join(__dirname, "public")));
    app.get("/*", (req, res) => {
        res.sendFile(path.join(__dirname, "public/index.html"));
      });


    app.listen((port), function() {
        Logger.info("NodeJs api hosted successfully for port " + port);
        console.log("NodeJs api hosted successfully for port " + port);

    });

} catch (ex) {
    console.log(ex)
}