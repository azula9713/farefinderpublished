export class User {
  id: string;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  userrole: string;
  country: string;
  token: string;
}