import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  SimpleChanges,
} from "@angular/core";
import { FormArray } from "@angular/forms";
import { MainServicesService } from "src/app/MainService/main-services.service";
import { HomeService } from "src/app/home/service/home.service";

declare var $: any;

@Component({
  selector: "app-ticket",
  templateUrl: "./ticket.component.html",
  styleUrls: ["./ticket.component.css"],
})
export class TicketComponent implements OnInit {
  constructor(
    private mainServicesService: MainServicesService,
    private HomeService: HomeService
  ) {
    this.switchScrenn();
  }

  @Output() setValueEmitter = new EventEmitter<any>();
  @Input() isActiveTicketAgenPriceCheckForm: boolean = false;
  @Input() selectedTicketObject: any;
  @Input() cabinclassNusersInfo: any;
  @Input() currencyRate: any;
  @Input() directSearchId: any;
  @Input() inDirectSearchId: any;

  isTicketAvilable: boolean = false;
  isActiveTicketsPage: boolean = false;
  segment: any;
  airLines = [];
  airPorts = [];
  isWeb: boolean = false;
  isMobile: boolean = false;

  ngOnInit(): void {
    this.getAllAirLines();
    this.getAllAirPorts();
  }

  ngOnChanges(changes: SimpleChanges) {
    // debugger;
    let availableAgentsArray = this.selectedTicketObject.Agents;
    for (let i = 0; i < availableAgentsArray.length; i++) {
      let termsUrl = this.selectedTicketObject.Agents[i].termsUrl;
      let url = "";
      let params = {};
      // console.log(termsUrl);
      if (this.selectedTicketObject.IsDirect == true) {
        // console.log('Direct Id',this.directSearchId);
        params = {
          termsUrl: termsUrl,
          searchId: this.directSearchId,
        };
      } else {
        // console.log('Indirect Id', this.inDirectSearchId);
        params = {
          termsUrl: termsUrl,
          searchId: this.inDirectSearchId,
        };
      }
      // search api for get link of agent website
      this.mainServicesService.getAgentSiteLink(params).subscribe((res) => {
        url = res.url;

        this.selectedTicketObject.Agents[i] = {
          agentId: this.selectedTicketObject.Agents[i].agentId,
          agentName: this.selectedTicketObject.Agents[i].agentName,
          agentTicketCurrency: this.selectedTicketObject.Agents[i]
            .agentTicketCurrency,
          agentTicketPrice: this.selectedTicketObject.Agents[i]
            .agentTicketPrice,
          termsUrl: this.selectedTicketObject.Agents[i].termsUrl,
          agentWebLink: url,
        };
      });
    }

    console.log(this.selectedTicketObject.Agents);
    if (
      changes.selectedTicketObject &&
      changes.selectedTicketObject.currentValue.Id
    ) {
      this.isTicketAvilable = true;
      this.getTicketDataSet(changes.selectedTicketObject);
    } else {
      this.isTicketAvilable = false;
    }
  }

  switchScrenn() {
    if (window.screen.width <= 600) {
      //mobile
      this.isWeb = false;
      this.isMobile = true;
    } else {
      //web
      this.isMobile = false;
      this.isWeb = true;
    }
  }

  backToResultPage() {
    this.setValueEmitter.emit(true);
  }

  getTicketDataSet(ticketObj) {
    // debugger;
    this.segment = ticketObj.currentValue.Segment;
  }

  getFlightInfo(requestName, segmentIndex) {
    // console.log('INIT RESULt FUNCTIon is called, 444444444444444444')
    if (requestName === "name") {
      return this.cabinclassNusersInfo.detail[segmentIndex][
        "DisplayFlightTypeName"
      ];
    } else {
      return this.cabinclassNusersInfo.detail[segmentIndex]["date"];
    }
  }

  get avilableAgents() {
    // console.log('CURRENCY RATES:::::::', this.currencyRate);
    return this.selectedTicketObject.Agents as FormArray;
  }

  async getAllAirLines() {
    this.airLines = await this.mainServicesService.getAllAirLines();
  }
  async getAllAirPorts() {
    this.airPorts = await this.HomeService.getFlights();
  }

  getLogo(Segment, type) {
    // console.log('INIT RESULt FUNCTIon is called, 2222222222222222')

    if (type === "Web") {
      if (Segment.Detail.length === 1) {
        return (
          "http://pics.avs.io/100/50/" + Segment.Detail[0].airLine + ".png"
        );
      } else {
        let airLineNames = "";
        let airLineName = "";
        Segment.Detail.forEach((element) => {
          airLineName = this.airLines.filter(
            (airLine) => airLine.code == element.airLine
          )[0].name;
          airLineNames = airLineNames + "  " + airLineName + ",";
        });

        return airLineNames;
      }
    } else if (type === "Mobile") {
      if (Segment.Detail.length === 1) {
        return "http://pics.avs.io/50/20/" + Segment.Detail[0].airLine + ".png";
      } else {
        let airLineNames = "";
        let airLineName = "";
        Segment.Detail.forEach((element) => {
          airLineName = this.airLines.filter(
            (airLine) => airLine.code == element.airLine
          )[0].name;
          airLineNames = airLineNames + "  " + airLineName + ",";
        });

        return airLineNames;
      }
    }
  }

  getLogoInDetailLevel(SegmentDetail, type) {
    if (type === "Web") {
      return "http://pics.avs.io/80/50/" + SegmentDetail.airLine + ".png";
    } else {
      return "http://pics.avs.io/50/20/" + SegmentDetail.airLine + ".png";
    }
  }

  checkPossibleToGetLogo(Segment, Type) {
    if (Type === "Web" || "Mobile") {
      if (Segment.Detail.length === 1) return true;
      else return false;
    }
  }

  getAirPort(airPortCode) {
    return (
      airPortCode +
      "   " +
      this.airPorts.find((airports) => airports.flightCode === airPortCode)[
        "flightName"
      ]
    );
  }

  getGbpPrice(price, currency) {
    let exRate = this.currencyRate[currency] / this.currencyRate.gbp;
    return (price * exRate).toFixed(2);
  }

  agentRedirect(termsUrl) {
    let url = "";
    let params = {};
    // console.log(termsUrl);
    if (this.selectedTicketObject.IsDirect == true) {
      // console.log('Direct Id',this.directSearchId);
      params = {
        termsUrl: termsUrl,
        searchId: this.directSearchId,
      };
    } else {
      // console.log('Indirect Id', this.inDirectSearchId);
      params = {
        termsUrl: termsUrl,
        searchId: this.inDirectSearchId,
      };
    }
    // search api for get link of agent website
    this.mainServicesService.getAgentSiteLink(params).subscribe((res) => {
      // console.log('Agetn Site Result',res);
      // window.open(res.url, "_blank");

      url = res.url;

      // let url = res.url;
      // var a = window.document.createElement("a");
      // a.target = '_blank';
      // a.href = url;

      // var e = window.document.createEvent("MouseEvents");
      // e.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
      // a.dispatchEvent(e);
    });

    return url;
  }
}
