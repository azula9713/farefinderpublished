

var OutPutJsonArray = [
    {
        "proposals": [

            {
                "terms": {
                    "112": {
                        "currency": "gbp",
                        "price": 683,
                        "unified_price": 58481,
                        "url": 11200000,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null
                            ],
                            [
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30"
                            ],
                            [
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC"
                            ],
                            [
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0
                            ],
                            [
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4
                            ],
                            [
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30;Y_1PC30": {
                            "currency": "gbp",
                            "price": 683,
                            "unified_price": 58481,
                            "url": 11200000,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null
                                ],
                                [
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30"
                                ],
                                [
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC"
                                ],
                                [
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0
                                ],
                                [
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4
                                ],
                                [
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "06:35",
                                "arrival_timestamp": 1593138900,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "20:40",
                                "departure_timestamp": 1593114000,
                                "duration": 415,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593153300,
                                "local_departure_timestamp": 1593117600,
                                "marketing_carrier": "EK",
                                "number": "4",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.707291666666666,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.645833333333332,
                            "detailed": {
                                "transfer": 10,
                                "arrival_time": 8.229166666666666,
                                "departure_time": 10
                            }
                        }
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "18:40",
                                "arrival_timestamp": 1593452400,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "14:15",
                                "departure_timestamp": 1593425700,
                                "duration": 445,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593456000,
                                "local_departure_timestamp": 1593440100,
                                "marketing_carrier": "EK",
                                "number": "3",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.724791666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.759166666666667,
                            "detailed": {
                                "arrival_time": 9.733333333333334,
                                "departure_time": 9.0625,
                                "transfer": 10
                            }
                        }
                    }
                ],
                "total_duration": 860,
                "stops_airports": [
                    "DXB",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 0,
                "max_stop_duration": 0,
                "carriers": [
                    "EK"
                ],
                "segment_durations": [
                    415,
                    445
                ],
                "segments_time": [
                    [
                        1593117600,
                        1593153300
                    ],
                    [
                        1593440100,
                        1593456000
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "2f9accfe99afda0e800b8942684d8134",
                "is_direct": true,
                "flight_weight": 6.112293684301916e-7,
                "popularity": 6.112293684301916e-7,
                "segments_rating": 9.68361111111111,
                "tags": [
                    "convenient_ticket",
                    "direct"
                ]
            },
            {
                "terms": {
                    "112": {
                        "currency": "gbp",
                        "price": 683,
                        "unified_price": 58481,
                        "url": 11200001,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null
                            ],
                            [
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30"
                            ],
                            [
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC"
                            ],
                            [
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0
                            ],
                            [
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4
                            ],
                            [
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30;Y_1PC30": {
                            "currency": "gbp",
                            "price": 683,
                            "unified_price": 58481,
                            "url": 11200001,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null
                                ],
                                [
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30"
                                ],
                                [
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC"
                                ],
                                [
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0
                                ],
                                [
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4
                                ],
                                [
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "00:20",
                                "arrival_timestamp": 1593116400,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "14:20",
                                "departure_timestamp": 1593091200,
                                "duration": 420,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593130800,
                                "local_departure_timestamp": 1593094800,
                                "marketing_carrier": "EK",
                                "number": "2",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.917291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.35,
                            "detailed": {
                                "transfer": 10,
                                "arrival_time": 7.666666666666667,
                                "departure_time": 9.083333333333334
                            }
                        }
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "18:40",
                                "arrival_timestamp": 1593452400,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "14:15",
                                "departure_timestamp": 1593425700,
                                "duration": 445,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593456000,
                                "local_departure_timestamp": 1593440100,
                                "marketing_carrier": "EK",
                                "number": "3",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.724791666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.759166666666667,
                            "detailed": {
                                "arrival_time": 9.733333333333334,
                                "departure_time": 9.0625,
                                "transfer": 10
                            }
                        }
                    }
                ],
                "total_duration": 865,
                "stops_airports": [
                    "DXB",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 0,
                "max_stop_duration": 0,
                "carriers": [
                    "EK"
                ],
                "segment_durations": [
                    420,
                    445
                ],
                "segments_time": [
                    [
                        1593094800,
                        1593130800
                    ],
                    [
                        1593440100,
                        1593456000
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "63fa074aa98376d9f8843e8a1da51eab",
                "is_direct": true,
                "flight_weight": 0,
                "popularity": 0,
                "segments_rating": 9.48638888888889,
                "tags": [
                    "convenient_ticket",
                    "direct"
                ]
            },
            {
                "terms": {
                    "112": {
                        "currency": "gbp",
                        "price": 683,
                        "unified_price": 58481,
                        "url": 11200002,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null
                            ],
                            [
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30"
                            ],
                            [
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC"
                            ],
                            [
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0
                            ],
                            [
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4
                            ],
                            [
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30;Y_1PC30": {
                            "currency": "gbp",
                            "price": 683,
                            "unified_price": 58481,
                            "url": 11200002,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null
                                ],
                                [
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30"
                                ],
                                [
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC"
                                ],
                                [
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0
                                ],
                                [
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4
                                ],
                                [
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "06:35",
                                "arrival_timestamp": 1593138900,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "20:40",
                                "departure_timestamp": 1593114000,
                                "duration": 415,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593153300,
                                "local_departure_timestamp": 1593117600,
                                "marketing_carrier": "EK",
                                "number": "4",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.707291666666666,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.645833333333332,
                            "detailed": {
                                "arrival_time": 8.229166666666666,
                                "departure_time": 10,
                                "transfer": 10
                            }
                        }
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "12:25",
                                "arrival_timestamp": 1593429900,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "07:45",
                                "departure_timestamp": 1593402300,
                                "duration": 460,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593433500,
                                "local_departure_timestamp": 1593416700,
                                "marketing_carrier": "EK",
                                "number": "1",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.777291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.590833333333332,
                            "detailed": {
                                "arrival_time": 9.516666666666666,
                                "departure_time": 8.4375,
                                "transfer": 10
                            }
                        }
                    }
                ],
                "total_duration": 875,
                "stops_airports": [
                    "DXB",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 0,
                "max_stop_duration": 0,
                "carriers": [
                    "EK"
                ],
                "segment_durations": [
                    415,
                    460
                ],
                "segments_time": [
                    [
                        1593117600,
                        1593153300
                    ],
                    [
                        1593416700,
                        1593433500
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "486c4cd44941e54617be4be6802fcda3",
                "is_direct": true,
                "flight_weight": 0,
                "popularity": 0,
                "segments_rating": 9.609166666666665,
                "tags": [
                    "convenient_ticket",
                    "direct"
                ]
            },
            {
                "terms": {
                    "112": {
                        "currency": "gbp",
                        "price": 683,
                        "unified_price": 58481,
                        "url": 11200003,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null
                            ],
                            [
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30"
                            ],
                            [
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC"
                            ],
                            [
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0
                            ],
                            [
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4
                            ],
                            [
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30;Y_1PC30": {
                            "currency": "gbp",
                            "price": 683,
                            "unified_price": 58481,
                            "url": 11200003,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null
                                ],
                                [
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30"
                                ],
                                [
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC"
                                ],
                                [
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0
                                ],
                                [
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4
                                ],
                                [
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "00:20",
                                "arrival_timestamp": 1593116400,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "14:20",
                                "departure_timestamp": 1593091200,
                                "duration": 420,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593130800,
                                "local_departure_timestamp": 1593094800,
                                "marketing_carrier": "EK",
                                "number": "2",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.917291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.35,
                            "detailed": {
                                "arrival_time": 7.666666666666667,
                                "departure_time": 9.083333333333334,
                                "transfer": 10
                            }
                        }
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "12:25",
                                "arrival_timestamp": 1593429900,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "07:45",
                                "departure_timestamp": 1593402300,
                                "duration": 460,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593433500,
                                "local_departure_timestamp": 1593416700,
                                "marketing_carrier": "EK",
                                "number": "1",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.777291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.590833333333332,
                            "detailed": {
                                "arrival_time": 9.516666666666666,
                                "departure_time": 8.4375,
                                "transfer": 10
                            }
                        }
                    }
                ],
                "total_duration": 880,
                "stops_airports": [
                    "DXB",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 0,
                "max_stop_duration": 0,
                "carriers": [
                    "EK"
                ],
                "segment_durations": [
                    420,
                    460
                ],
                "segments_time": [
                    [
                        1593094800,
                        1593130800
                    ],
                    [
                        1593416700,
                        1593433500
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "a698c4295f163ea7d220be2a96dd84c3",
                "is_direct": true,
                "flight_weight": 0,
                "popularity": 0,
                "segments_rating": 9.430277777777777,
                "tags": [
                    "convenient_ticket",
                    "direct"
                ]
            },
            {
                "terms": {
                    "112": {
                        "currency": "gbp",
                        "price": 1459,
                        "unified_price": 125003,
                        "url": 11200004,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null
                            ],
                            [
                                null,
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30"
                            ],
                            [
                                "1PC30",
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC"
                            ],
                            [
                                "1PC",
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0
                            ],
                            [
                                0,
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4
                            ],
                            [
                                4,
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30;Y_1PC30,Y_1PC30": {
                            "currency": "gbp",
                            "price": 1459,
                            "unified_price": 125003,
                            "url": 11200004,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null
                                ],
                                [
                                    null,
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30"
                                ],
                                [
                                    "1PC30",
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC"
                                ],
                                [
                                    "1PC",
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0
                                ],
                                [
                                    0,
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4
                                ],
                                [
                                    4,
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "06:35",
                                "arrival_timestamp": 1593138900,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "20:40",
                                "departure_timestamp": 1593114000,
                                "duration": 415,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593153300,
                                "local_departure_timestamp": 1593117600,
                                "marketing_carrier": "EK",
                                "number": "4",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.707291666666666,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.645833333333332,
                            "detailed": {
                                "arrival_time": 8.229166666666666,
                                "departure_time": 10,
                                "transfer": 10
                            }
                        }
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "FRA",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "13:15",
                                "arrival_timestamp": 1593429300,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "08:25",
                                "departure_timestamp": 1593404700,
                                "duration": 410,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593436500,
                                "local_departure_timestamp": 1593419100,
                                "marketing_carrier": "EK",
                                "number": "45",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.742291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            },
                            {
                                "aircraft": "32Q",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "22:10",
                                "arrival_timestamp": 1593465000,
                                "delay": 495,
                                "departure": "FRA",
                                "departure_date": "2020-06-29",
                                "departure_time": "21:30",
                                "departure_timestamp": 1593459000,
                                "duration": 100,
                                "equipment": "32Q",
                                "local_arrival_timestamp": 1593468600,
                                "local_departure_timestamp": 1593466200,
                                "marketing_carrier": "LH",
                                "number": "922",
                                "operating_carrier": "LH",
                                "operated_by": "LH",
                                "rating": 8.123750000000001,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 6.75,
                            "detailed": {
                                "arrival_time": 9.833333333333332,
                                "departure_time": 9.270833333333332,
                                "transfer": 6.75
                            }
                        },
                        "transfers": [
                            {
                                "at": "FRA",
                                "to": "FRA",
                                "airports": [
                                    "FRA"
                                ],
                                "airlines": [
                                    "EK",
                                    "LH"
                                ],
                                "country_code": "DE",
                                "city_code": "FRA",
                                "visa_rules": {
                                    "required": false
                                },
                                "night_transfer": false,
                                "tags": [],
                                "duration_seconds": 29700,
                                "duration": {
                                    "seconds": 29700
                                }
                            }
                        ]
                    }
                ],
                "total_duration": 1420,
                "stops_airports": [
                    "DXB",
                    "FRA",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 1,
                "max_stop_duration": 495,
                "min_stop_duration": 495,
                "carriers": [
                    "EK",
                    "LH"
                ],
                "segment_durations": [
                    415,
                    1005
                ],
                "segments_time": [
                    [
                        1593117600,
                        1593153300
                    ],
                    [
                        1593419100,
                        1593468600
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "cf2b31e14d4d01eb22699915276433ca",
                "is_direct": false,
                "flight_weight": 0,
                "popularity": 0,
                "segments_rating": 7.715277777777778,
                "tags": [
                    "long_layover",
                    "long_transfer",
                    "uncomfortable_seats",
                    "convenient_ticket"
                ]
            },
            {
                "terms": {
                    "112": {
                        "currency": "gbp",
                        "price": 1459,
                        "unified_price": 125003,
                        "url": 11200005,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null
                            ],
                            [
                                null,
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30"
                            ],
                            [
                                "1PC30",
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC"
                            ],
                            [
                                "1PC",
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0
                            ],
                            [
                                0,
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4
                            ],
                            [
                                4,
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30;Y_1PC30,Y_1PC30": {
                            "currency": "gbp",
                            "price": 1459,
                            "unified_price": 125003,
                            "url": 11200005,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null
                                ],
                                [
                                    null,
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30"
                                ],
                                [
                                    "1PC30",
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC"
                                ],
                                [
                                    "1PC",
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0
                                ],
                                [
                                    0,
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4
                                ],
                                [
                                    4,
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "00:20",
                                "arrival_timestamp": 1593116400,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "14:20",
                                "departure_timestamp": 1593091200,
                                "duration": 420,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593130800,
                                "local_departure_timestamp": 1593094800,
                                "marketing_carrier": "EK",
                                "number": "2",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.917291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.35,
                            "detailed": {
                                "arrival_time": 7.666666666666667,
                                "departure_time": 9.083333333333334,
                                "transfer": 10
                            }
                        }
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "FRA",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "13:15",
                                "arrival_timestamp": 1593429300,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "08:25",
                                "departure_timestamp": 1593404700,
                                "duration": 410,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593436500,
                                "local_departure_timestamp": 1593419100,
                                "marketing_carrier": "EK",
                                "number": "45",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.742291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            },
                            {
                                "aircraft": "32Q",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "22:10",
                                "arrival_timestamp": 1593465000,
                                "delay": 495,
                                "departure": "FRA",
                                "departure_date": "2020-06-29",
                                "departure_time": "21:30",
                                "departure_timestamp": 1593459000,
                                "duration": 100,
                                "equipment": "32Q",
                                "local_arrival_timestamp": 1593468600,
                                "local_departure_timestamp": 1593466200,
                                "marketing_carrier": "LH",
                                "number": "922",
                                "operating_carrier": "LH",
                                "operated_by": "LH",
                                "rating": 8.123750000000001,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 6.75,
                            "detailed": {
                                "arrival_time": 9.833333333333332,
                                "departure_time": 9.270833333333332,
                                "transfer": 6.75
                            }
                        },
                        "transfers": [
                            {
                                "at": "FRA",
                                "to": "FRA",
                                "airports": [
                                    "FRA"
                                ],
                                "airlines": [
                                    "EK",
                                    "LH"
                                ],
                                "country_code": "DE",
                                "city_code": "FRA",
                                "visa_rules": {
                                    "required": false
                                },
                                "night_transfer": false,
                                "tags": [],
                                "duration_seconds": 29700,
                                "duration": {
                                    "seconds": 29700
                                }
                            }
                        ]
                    }
                ],
                "total_duration": 1425,
                "stops_airports": [
                    "DXB",
                    "FRA",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 1,
                "max_stop_duration": 495,
                "min_stop_duration": 495,
                "carriers": [
                    "EK",
                    "LH"
                ],
                "segment_durations": [
                    420,
                    1005
                ],
                "segments_time": [
                    [
                        1593094800,
                        1593130800
                    ],
                    [
                        1593419100,
                        1593468600
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "7345c13f1bf28e2b7c304f726a6af1db",
                "is_direct": false,
                "flight_weight": 0,
                "popularity": 0,
                "segments_rating": 7.616666666666667,
                "tags": [
                    "convenient_ticket",
                    "uncomfortable_seats",
                    "long_layover",
                    "long_transfer"
                ]
            },
            {
                "terms": {
                    "112": {
                        "currency": "gbp",
                        "price": 1475,
                        "unified_price": 126428,
                        "url": 11200006,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null
                            ],
                            [
                                null,
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30"
                            ],
                            [
                                "1PC30",
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC"
                            ],
                            [
                                "1PC",
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0
                            ],
                            [
                                0,
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4
                            ],
                            [
                                4,
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30;Y_1PC30,Y_1PC30": {
                            "currency": "gbp",
                            "price": 1475,
                            "unified_price": 126428,
                            "url": 11200006,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null
                                ],
                                [
                                    null,
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30"
                                ],
                                [
                                    "1PC30",
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC"
                                ],
                                [
                                    "1PC",
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0
                                ],
                                [
                                    0,
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4
                                ],
                                [
                                    4,
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "06:35",
                                "arrival_timestamp": 1593138900,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "20:40",
                                "departure_timestamp": 1593114000,
                                "duration": 415,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593153300,
                                "local_departure_timestamp": 1593117600,
                                "marketing_carrier": "EK",
                                "number": "4",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.707291666666666,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.645833333333332,
                            "detailed": {
                                "arrival_time": 8.229166666666666,
                                "departure_time": 10,
                                "transfer": 10
                            }
                        }
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "FRA",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "13:15",
                                "arrival_timestamp": 1593429300,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "08:25",
                                "departure_timestamp": 1593404700,
                                "duration": 410,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593436500,
                                "local_departure_timestamp": 1593419100,
                                "marketing_carrier": "EK",
                                "number": "45",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.742291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            },
                            {
                                "aircraft": "32Q",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-30",
                                "arrival_time": "08:40",
                                "arrival_timestamp": 1593502800,
                                "delay": 1125,
                                "departure": "FRA",
                                "departure_date": "2020-06-30",
                                "departure_time": "08:00",
                                "departure_timestamp": 1593496800,
                                "duration": 100,
                                "equipment": "32Q",
                                "local_arrival_timestamp": 1593506400,
                                "local_departure_timestamp": 1593504000,
                                "marketing_carrier": "LH",
                                "number": "900",
                                "operating_carrier": "LH",
                                "operated_by": "LH",
                                "rating": 8.05375,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 6.5,
                            "detailed": {
                                "departure_time": 9.270833333333332,
                                "transfer": 6.5,
                                "arrival_time": 10
                            }
                        },
                        "transfers": [
                            {
                                "at": "FRA",
                                "to": "FRA",
                                "airports": [
                                    "FRA"
                                ],
                                "airlines": [
                                    "EK",
                                    "LH"
                                ],
                                "country_code": "DE",
                                "city_code": "FRA",
                                "visa_rules": {
                                    "required": false
                                },
                                "night_transfer": true,
                                "tags": [],
                                "duration_seconds": 67500,
                                "duration": {
                                    "seconds": 67500
                                }
                            }
                        ]
                    }
                ],
                "total_duration": 2050,
                "stops_airports": [
                    "DXB",
                    "FRA",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 1,
                "max_stop_duration": 1125,
                "min_stop_duration": 1125,
                "carriers": [
                    "EK",
                    "LH"
                ],
                "segment_durations": [
                    415,
                    1635
                ],
                "segments_time": [
                    [
                        1593117600,
                        1593153300
                    ],
                    [
                        1593419100,
                        1593506400
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "00fd31f996d8df4b634d6e36d322bc38",
                "is_direct": false,
                "flight_weight": 0,
                "popularity": 0,
                "segments_rating": 7.548611111111111,
                "tags": [
                    "night_transfer",
                    "long_transfer",
                    "uncomfortable_seats",
                    "has_night_transfer",
                    "overnight_layover",
                    "long_layover"
                ]
            },
            {
                "terms": {
                    "112": {
                        "currency": "gbp",
                        "price": 1475,
                        "unified_price": 126428,
                        "url": 11200007,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null
                            ],
                            [
                                null,
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30"
                            ],
                            [
                                "1PC30",
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC"
                            ],
                            [
                                "1PC",
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0
                            ],
                            [
                                0,
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4
                            ],
                            [
                                4,
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30;Y_1PC30,Y_1PC30": {
                            "currency": "gbp",
                            "price": 1475,
                            "unified_price": 126428,
                            "url": 11200007,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null
                                ],
                                [
                                    null,
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30"
                                ],
                                [
                                    "1PC30",
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC"
                                ],
                                [
                                    "1PC",
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0
                                ],
                                [
                                    0,
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4
                                ],
                                [
                                    4,
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "00:20",
                                "arrival_timestamp": 1593116400,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "14:20",
                                "departure_timestamp": 1593091200,
                                "duration": 420,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593130800,
                                "local_departure_timestamp": 1593094800,
                                "marketing_carrier": "EK",
                                "number": "2",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.917291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.35,
                            "detailed": {
                                "transfer": 10,
                                "arrival_time": 7.666666666666667,
                                "departure_time": 9.083333333333334
                            }
                        }
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "FRA",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "13:15",
                                "arrival_timestamp": 1593429300,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "08:25",
                                "departure_timestamp": 1593404700,
                                "duration": 410,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593436500,
                                "local_departure_timestamp": 1593419100,
                                "marketing_carrier": "EK",
                                "number": "45",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.742291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            },
                            {
                                "aircraft": "32Q",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-30",
                                "arrival_time": "08:40",
                                "arrival_timestamp": 1593502800,
                                "delay": 1125,
                                "departure": "FRA",
                                "departure_date": "2020-06-30",
                                "departure_time": "08:00",
                                "departure_timestamp": 1593496800,
                                "duration": 100,
                                "equipment": "32Q",
                                "local_arrival_timestamp": 1593506400,
                                "local_departure_timestamp": 1593504000,
                                "marketing_carrier": "LH",
                                "number": "900",
                                "operating_carrier": "LH",
                                "operated_by": "LH",
                                "rating": 8.05375,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 6.5,
                            "detailed": {
                                "arrival_time": 10,
                                "departure_time": 9.270833333333332,
                                "transfer": 6.5
                            }
                        },
                        "transfers": [
                            {
                                "at": "FRA",
                                "to": "FRA",
                                "airports": [
                                    "FRA"
                                ],
                                "airlines": [
                                    "EK",
                                    "LH"
                                ],
                                "country_code": "DE",
                                "city_code": "FRA",
                                "visa_rules": {
                                    "required": false
                                },
                                "night_transfer": true,
                                "tags": [],
                                "duration_seconds": 67500,
                                "duration": {
                                    "seconds": 67500
                                }
                            }
                        ]
                    }
                ],
                "total_duration": 2055,
                "stops_airports": [
                    "DXB",
                    "FRA",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 1,
                "max_stop_duration": 1125,
                "min_stop_duration": 1125,
                "carriers": [
                    "EK",
                    "LH"
                ],
                "segment_durations": [
                    420,
                    1635
                ],
                "segments_time": [
                    [
                        1593094800,
                        1593130800
                    ],
                    [
                        1593419100,
                        1593506400
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "0e0712da6fdd296c4bc353b4471c48d0",
                "is_direct": false,
                "flight_weight": 0,
                "popularity": 0,
                "segments_rating": 7.45,
                "tags": [
                    "overnight_layover",
                    "long_layover",
                    "night_transfer",
                    "long_transfer",
                    "has_night_transfer",
                    "uncomfortable_seats"
                ]
            },
            {
                "terms": {
                    "112": {
                        "currency": "gbp",
                        "price": 1659,
                        "unified_price": 142172,
                        "url": 11200008,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null,
                                null
                            ],
                            [
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30",
                                "1PC30"
                            ],
                            [
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC",
                                "1PC"
                            ],
                            [
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0,
                                0
                            ],
                            [
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4,
                                4
                            ],
                            [
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30,Y_1PC30;Y_1PC30": {
                            "currency": "gbp",
                            "price": 1659,
                            "unified_price": 142172,
                            "url": 11200008,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null,
                                    null
                                ],
                                [
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30",
                                    "1PC30"
                                ],
                                [
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC",
                                    "1PC"
                                ],
                                [
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0,
                                    0
                                ],
                                [
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4,
                                    4
                                ],
                                [
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Airbus A320 (sharklets)",
                                "arrival": "ZRH",
                                "arrival_date": "2020-06-25",
                                "arrival_time": "21:35",
                                "arrival_timestamp": 1593113700,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "18:55",
                                "departure_timestamp": 1593107700,
                                "duration": 100,
                                "equipment": "32A",
                                "local_arrival_timestamp": 1593120900,
                                "local_departure_timestamp": 1593111300,
                                "marketing_carrier": "LX",
                                "number": "327",
                                "operating_carrier": "LX",
                                "operated_by": "LX",
                                "rating": 7.984861111111111,
                                "technical_stops": null,
                                "trip_class": "Y"
                            },
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "23:45",
                                "arrival_timestamp": 1593200700,
                                "delay": 1070,
                                "departure": "ZRH",
                                "departure_date": "2020-06-26",
                                "departure_time": "15:25",
                                "departure_timestamp": 1593177900,
                                "duration": 380,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593215100,
                                "local_departure_timestamp": 1593185100,
                                "marketing_carrier": "EK",
                                "number": "88",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.864791666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 6.5,
                            "detailed": {
                                "arrival_time": 8.25,
                                "departure_time": 10,
                                "transfer": 6.5
                            }
                        },
                        "transfers": [
                            {
                                "at": "ZRH",
                                "to": "ZRH",
                                "airports": [
                                    "ZRH"
                                ],
                                "airlines": [
                                    "LX",
                                    "EK"
                                ],
                                "country_code": "CH",
                                "city_code": "ZRH",
                                "visa_rules": {
                                    "required": false
                                },
                                "night_transfer": true,
                                "tags": [],
                                "duration_seconds": 64200,
                                "duration": {
                                    "seconds": 64200
                                }
                            }
                        ]
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "18:40",
                                "arrival_timestamp": 1593452400,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "14:15",
                                "departure_timestamp": 1593425700,
                                "duration": 445,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593456000,
                                "local_departure_timestamp": 1593440100,
                                "marketing_carrier": "EK",
                                "number": "3",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.724791666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.759166666666667,
                            "detailed": {
                                "arrival_time": 9.733333333333334,
                                "departure_time": 9.0625,
                                "transfer": 10
                            }
                        }
                    }
                ],
                "total_duration": 1995,
                "stops_airports": [
                    "ZRH",
                    "DXB",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 1,
                "max_stop_duration": 1070,
                "min_stop_duration": 1070,
                "carriers": [
                    "LX",
                    "EK"
                ],
                "segment_durations": [
                    1550,
                    445
                ],
                "segments_time": [
                    [
                        1593111300,
                        1593215100
                    ],
                    [
                        1593440100,
                        1593456000
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "c1c5eef78c93b97a98777d25370633b5",
                "is_direct": false,
                "flight_weight": 0,
                "popularity": 0,
                "segments_rating": 7.586388888888888,
                "tags": [
                    "night_transfer",
                    "long_transfer",
                    "has_night_transfer",
                    "overnight_layover",
                    "long_layover"
                ]
            },
            {
                "terms": {
                    "112": {
                        "currency": "gbp",
                        "price": 1659,
                        "unified_price": 142172,
                        "url": 11200009,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null,
                                null
                            ],
                            [
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30",
                                "1PC30"
                            ],
                            [
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC",
                                "1PC"
                            ],
                            [
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0,
                                0
                            ],
                            [
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4,
                                4
                            ],
                            [
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30,Y_1PC30;Y_1PC30": {
                            "currency": "gbp",
                            "price": 1659,
                            "unified_price": 142172,
                            "url": 11200009,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null,
                                    null
                                ],
                                [
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30",
                                    "1PC30"
                                ],
                                [
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC",
                                    "1PC"
                                ],
                                [
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0,
                                    0
                                ],
                                [
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4,
                                    4
                                ],
                                [
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Airbus A320 (sharklets)",
                                "arrival": "ZRH",
                                "arrival_date": "2020-06-25",
                                "arrival_time": "21:35",
                                "arrival_timestamp": 1593113700,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "18:55",
                                "departure_timestamp": 1593107700,
                                "duration": 100,
                                "equipment": "32A",
                                "local_arrival_timestamp": 1593120900,
                                "local_departure_timestamp": 1593111300,
                                "marketing_carrier": "LX",
                                "number": "327",
                                "operating_carrier": "LX",
                                "operated_by": "LX",
                                "rating": 7.984861111111111,
                                "technical_stops": null,
                                "trip_class": "Y"
                            },
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "23:45",
                                "arrival_timestamp": 1593200700,
                                "delay": 1070,
                                "departure": "ZRH",
                                "departure_date": "2020-06-26",
                                "departure_time": "15:25",
                                "departure_timestamp": 1593177900,
                                "duration": 380,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593215100,
                                "local_departure_timestamp": 1593185100,
                                "marketing_carrier": "EK",
                                "number": "88",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.864791666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 6.5,
                            "detailed": {
                                "arrival_time": 8.25,
                                "departure_time": 10,
                                "transfer": 6.5
                            }
                        },
                        "transfers": [
                            {
                                "at": "ZRH",
                                "to": "ZRH",
                                "airports": [
                                    "ZRH"
                                ],
                                "airlines": [
                                    "LX",
                                    "EK"
                                ],
                                "country_code": "CH",
                                "city_code": "ZRH",
                                "visa_rules": {
                                    "required": false
                                },
                                "night_transfer": true,
                                "tags": [],
                                "duration_seconds": 64200,
                                "duration": {
                                    "seconds": 64200
                                }
                            }
                        ]
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "12:25",
                                "arrival_timestamp": 1593429900,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "07:45",
                                "departure_timestamp": 1593402300,
                                "duration": 460,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593433500,
                                "local_departure_timestamp": 1593416700,
                                "marketing_carrier": "EK",
                                "number": "1",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.777291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 9.590833333333332,
                            "detailed": {
                                "arrival_time": 9.516666666666666,
                                "departure_time": 8.4375,
                                "transfer": 10
                            }
                        }
                    }
                ],
                "total_duration": 2010,
                "stops_airports": [
                    "ZRH",
                    "DXB",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 1,
                "max_stop_duration": 1070,
                "min_stop_duration": 1070,
                "carriers": [
                    "LX",
                    "EK"
                ],
                "segment_durations": [
                    1550,
                    460
                ],
                "segments_time": [
                    [
                        1593111300,
                        1593215100
                    ],
                    [
                        1593416700,
                        1593433500
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "cb19459dca8e2244448a2c8541e4af0c",
                "is_direct": false,
                "flight_weight": 0,
                "popularity": 0,
                "segments_rating": 7.530277777777777,
                "tags": [
                    "has_night_transfer",
                    "overnight_layover",
                    "long_layover",
                    "night_transfer",
                    "long_transfer"
                ]
            },
            {
                "terms": {
                    "112": {
                        "currency": "gbp",
                        "price": 2435,
                        "unified_price": 208694,
                        "url": 11200010,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null,
                                null
                            ],
                            [
                                null,
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30",
                                "1PC30"
                            ],
                            [
                                "1PC30",
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC",
                                "1PC"
                            ],
                            [
                                "1PC",
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0,
                                0
                            ],
                            [
                                0,
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4,
                                4
                            ],
                            [
                                4,
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30,Y_1PC30;Y_1PC30,Y_1PC30": {
                            "currency": "gbp",
                            "price": 2435,
                            "unified_price": 208694,
                            "url": 11200010,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null,
                                    null
                                ],
                                [
                                    null,
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30",
                                    "1PC30"
                                ],
                                [
                                    "1PC30",
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC",
                                    "1PC"
                                ],
                                [
                                    "1PC",
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0,
                                    0
                                ],
                                [
                                    0,
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4,
                                    4
                                ],
                                [
                                    4,
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Airbus A320 (sharklets)",
                                "arrival": "ZRH",
                                "arrival_date": "2020-06-25",
                                "arrival_time": "21:35",
                                "arrival_timestamp": 1593113700,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "18:55",
                                "departure_timestamp": 1593107700,
                                "duration": 100,
                                "equipment": "32A",
                                "local_arrival_timestamp": 1593120900,
                                "local_departure_timestamp": 1593111300,
                                "marketing_carrier": "LX",
                                "number": "327",
                                "operating_carrier": "LX",
                                "operated_by": "LX",
                                "rating": 7.984861111111111,
                                "technical_stops": null,
                                "trip_class": "Y"
                            },
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "23:45",
                                "arrival_timestamp": 1593200700,
                                "delay": 1070,
                                "departure": "ZRH",
                                "departure_date": "2020-06-26",
                                "departure_time": "15:25",
                                "departure_timestamp": 1593177900,
                                "duration": 380,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593215100,
                                "local_departure_timestamp": 1593185100,
                                "marketing_carrier": "EK",
                                "number": "88",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.864791666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 6.5,
                            "detailed": {
                                "arrival_time": 8.25,
                                "departure_time": 10,
                                "transfer": 6.5
                            }
                        },
                        "transfers": [
                            {
                                "at": "ZRH",
                                "to": "ZRH",
                                "airports": [
                                    "ZRH"
                                ],
                                "airlines": [
                                    "LX",
                                    "EK"
                                ],
                                "country_code": "CH",
                                "city_code": "ZRH",
                                "visa_rules": {
                                    "required": false
                                },
                                "night_transfer": true,
                                "tags": [],
                                "duration_seconds": 64200,
                                "duration": {
                                    "seconds": 64200
                                }
                            }
                        ]
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "FRA",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "13:15",
                                "arrival_timestamp": 1593429300,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "08:25",
                                "departure_timestamp": 1593404700,
                                "duration": 410,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593436500,
                                "local_departure_timestamp": 1593419100,
                                "marketing_carrier": "EK",
                                "number": "45",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.742291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            },
                            {
                                "aircraft": "32Q",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "22:10",
                                "arrival_timestamp": 1593465000,
                                "delay": 495,
                                "departure": "FRA",
                                "departure_date": "2020-06-29",
                                "departure_time": "21:30",
                                "departure_timestamp": 1593459000,
                                "duration": 100,
                                "equipment": "32Q",
                                "local_arrival_timestamp": 1593468600,
                                "local_departure_timestamp": 1593466200,
                                "marketing_carrier": "LH",
                                "number": "922",
                                "operating_carrier": "LH",
                                "operated_by": "LH",
                                "rating": 8.123750000000001,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 6.75,
                            "detailed": {
                                "arrival_time": 9.833333333333332,
                                "departure_time": 9.270833333333332,
                                "transfer": 6.75
                            }
                        },
                        "transfers": [
                            {
                                "at": "FRA",
                                "to": "FRA",
                                "airports": [
                                    "FRA"
                                ],
                                "airlines": [
                                    "EK",
                                    "LH"
                                ],
                                "country_code": "DE",
                                "city_code": "FRA",
                                "visa_rules": {
                                    "required": false
                                },
                                "night_transfer": false,
                                "tags": [],
                                "duration_seconds": 29700,
                                "duration": {
                                    "seconds": 29700
                                }
                            }
                        ]
                    }
                ],
                "total_duration": 2555,
                "stops_airports": [
                    "ZRH",
                    "DXB",
                    "FRA",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 1,
                "max_stop_duration": 1070,
                "min_stop_duration": 495,
                "carriers": [
                    "LX",
                    "EK",
                    "LH"
                ],
                "segment_durations": [
                    1550,
                    1005
                ],
                "segments_time": [
                    [
                        1593111300,
                        1593215100
                    ],
                    [
                        1593419100,
                        1593468600
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "4b3ac43ab2371ea7e2386d2387c2bf62",
                "is_direct": false,
                "flight_weight": 0,
                "popularity": 0,
                "segments_rating": 6.583333333333333,
                "tags": [
                    "long_transfer",
                    "night_transfer",
                    "uncomfortable_seats",
                    "has_night_transfer",
                    "overnight_layover",
                    "long_layover"
                ]
            },

            {
                "terms": { 
                    "112": {
                        "currency": "gbp",
                        "price": 2451,
                        "unified_price": 210119,
                        "url": 11200011,
                        "multiplier": 0.0009015645863294601,
                        "proposal_multiplier": 1,
                        "flight_additional_tariff_infos": [
                            [
                                null,
                                null
                            ],
                            [
                                null,
                                null
                            ]
                        ],
                        "flights_baggage": [
                            [
                                "1PC30",
                                "1PC30"
                            ],
                            [
                                "1PC30",
                                "1PC30"
                            ]
                        ],
                        "flights_handbags": [
                            [
                                "1PC",
                                "1PC"
                            ],
                            [
                                "1PC",
                                "1PC"
                            ]
                        ],
                        "baggage_source": [
                            [
                                0,
                                0
                            ],
                            [
                                0,
                                0
                            ]
                        ],
                        "handbags_source": [
                            [
                                4,
                                4
                            ],
                            [
                                4,
                                4
                            ]
                        ]
                    }
                },
                "xterms": {
                    "112": {
                        "Y_1PC30,Y_1PC30;Y_1PC30,Y_1PC30": {
                            "currency": "gbp",
                            "price": 2451,
                            "unified_price": 210119,
                            "url": 11200011,
                            "multiplier": 0.0009015645863294601,
                            "proposal_multiplier": 1,
                            "flight_additional_tariff_infos": [
                                [
                                    null,
                                    null
                                ],
                                [
                                    null,
                                    null
                                ]
                            ],
                            "flights_baggage": [
                                [
                                    "1PC30",
                                    "1PC30"
                                ],
                                [
                                    "1PC30",
                                    "1PC30"
                                ]
                            ],
                            "flights_handbags": [
                                [
                                    "1PC",
                                    "1PC"
                                ],
                                [
                                    "1PC",
                                    "1PC"
                                ]
                            ],
                            "baggage_source": [
                                [
                                    0,
                                    0
                                ],
                                [
                                    0,
                                    0
                                ]
                            ],
                            "handbags_source": [
                                [
                                    4,
                                    4
                                ],
                                [
                                    4,
                                    4
                                ]
                            ]
                        }
                    }
                },
                "validating_carrier": "EK",
                "segment": [
                    {
                        "flight": [
                            {
                                "aircraft": "Airbus A320 (sharklets)",
                                "arrival": "ZRH",
                                "arrival_date": "2020-06-25",
                                "arrival_time": "21:35",
                                "arrival_timestamp": 1593113700,
                                "delay": 0,
                                "departure": "LHR",
                                "departure_date": "2020-06-25",
                                "departure_time": "18:55",
                                "departure_timestamp": 1593107700,
                                "duration": 100,
                                "equipment": "32A",
                                "local_arrival_timestamp": 1593120900,
                                "local_departure_timestamp": 1593111300,
                                "marketing_carrier": "LX",
                                "number": "327",
                                "operating_carrier": "LX",
                                "operated_by": "LX",
                                "rating": 7.984861111111111,
                                "technical_stops": null,
                                "trip_class": "Y"
                            },
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "DXB",
                                "arrival_date": "2020-06-26",
                                "arrival_time": "23:45",
                                "arrival_timestamp": 1593200700,
                                "delay": 1070,
                                "departure": "ZRH",
                                "departure_date": "2020-06-26",
                                "departure_time": "15:25",
                                "departure_timestamp": 1593177900,
                                "duration": 380,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593215100,
                                "local_departure_timestamp": 1593185100,
                                "marketing_carrier": "EK",
                                "number": "88",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.864791666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 6.5,
                            "detailed": {
                                "arrival_time": 8.25,
                                "departure_time": 10,
                                "transfer": 6.5
                            }
                        },
                        "transfers": [
                            {
                                "at": "ZRH",
                                "to": "ZRH",
                                "airports": [
                                    "ZRH"
                                ],
                                "airlines": [
                                    "LX",
                                    "EK"
                                ],
                                "country_code": "CH",
                                "city_code": "ZRH",
                                "visa_rules": {
                                    "required": false
                                },
                                "night_transfer": true,
                                "tags": [],
                                "duration_seconds": 64200,
                                "duration": {
                                    "seconds": 64200
                                }
                            }
                        ]
                    },
                    {
                        "flight": [
                            {
                                "aircraft": "Boeing 777-300ER",
                                "arrival": "FRA",
                                "arrival_date": "2020-06-29",
                                "arrival_time": "13:15",
                                "arrival_timestamp": 1593429300,
                                "delay": 0,
                                "departure": "DXB",
                                "departure_date": "2020-06-29",
                                "departure_time": "08:25",
                                "departure_timestamp": 1593404700,
                                "duration": 410,
                                "equipment": "77W",
                                "local_arrival_timestamp": 1593436500,
                                "local_departure_timestamp": 1593419100,
                                "marketing_carrier": "EK",
                                "number": "45",
                                "operating_carrier": "EK",
                                "operated_by": "EK",
                                "rating": 8.742291666666667,
                                "technical_stops": null,
                                "trip_class": "Y"
                            },
                            {
                                "aircraft": "32Q",
                                "arrival": "LHR",
                                "arrival_date": "2020-06-30",
                                "arrival_time": "08:40",
                                "arrival_timestamp": 1593502800,
                                "delay": 1125,
                                "departure": "FRA",
                                "departure_date": "2020-06-30",
                                "departure_time": "08:00",
                                "departure_timestamp": 1593496800,
                                "duration": 100,
                                "equipment": "32Q",
                                "local_arrival_timestamp": 1593506400,
                                "local_departure_timestamp": 1593504000,
                                "marketing_carrier": "LH",
                                "number": "900",
                                "operating_carrier": "LH",
                                "operated_by": "LH",
                                "rating": 8.05375,
                                "technical_stops": null,
                                "trip_class": "Y"
                            }
                        ],
                        "rating": {
                            "total": 6.5,
                            "detailed": {
                                "arrival_time": 10,
                                "departure_time": 9.270833333333332,
                                "transfer": 6.5
                            }
                        },
                        "transfers": [
                            {
                                "at": "FRA",
                                "to": "FRA",
                                "airports": [
                                    "FRA"
                                ],
                                "airlines": [
                                    "EK",
                                    "LH"
                                ],
                                "country_code": "DE",
                                "city_code": "FRA",
                                "visa_rules": {
                                    "required": false
                                },
                                "night_transfer": true,
                                "tags": [],
                                "duration_seconds": 67500,
                                "duration": {
                                    "seconds": 67500
                                }
                            }
                        ]
                    }
                ],
                "total_duration": 3185,
                "stops_airports": [
                    "ZRH",
                    "DXB",
                    "FRA",
                    "LHR"
                ],
                "is_charter": false,
                "max_stops": 1,
                "max_stop_duration": 1125,
                "min_stop_duration": 1070,
                "carriers": [
                    "LX",
                    "EK",
                    "LH"
                ],
                "segment_durations": [
                    1550,
                    1635
                ],
                "segments_time": [
                    [
                        1593111300,
                        1593215100
                    ],
                    [
                        1593419100,
                        1593506400
                    ]
                ],
                "segments_airports": [
                    [
                        "LHR",
                        "DXB"
                    ],
                    [
                        "DXB",
                        "LHR"
                    ]
                ],
                "sign": "11be47bbe1df7e1c4c1a9c490ab4edee",
                "is_direct": false,
                "flight_weight": 0,
                "popularity": 0,
                "segments_rating": 6.5,
                "tags": [
                    "uncomfortable_seats",
                    "has_night_transfer",
                    "overnight_layover",
                    "long_layover",
                    "night_transfer",
                    "long_transfer"
                ]
            }
        ],
		



        "airports": {
            "DXB": {
                "name": "Dubai Airport",
                "city": "Dubai",
                "city_code": "DXB",
                "country": "United Arab Emirates",
                "country_code": "AE",
                "time_zone": "Asia/Dubai",
                "rates": 167,
                "average_rate": 4.23,
                "coordinates": {
                    "lat": 25.248665,
                    "lon": 55.352917
                }
            },
            "FRA": {
                "name": "Frankfurt Airport",
                "city": "Frankfurt",
                "city_code": "FRA",
                "country": "Germany",
                "country_code": "DE",
                "time_zone": "Europe/Berlin",
                "rates": 196,
                "average_rate": 4.08,
                "coordinates": {
                    "lon": 8.570773,
                    "lat": 50.050735
                }
            },
            "LHR": {
                "name": "London Heathrow Airport",
                "city": "London",
                "city_code": "LON",
                "country": "United Kingdom",
                "country_code": "GB",
                "time_zone": "Europe/London",
                "rates": 111,
                "average_rate": 4.14,
                "coordinates": {
                    "lat": 51.469604,
                    "lon": -0.453566
                }
            },
            "ZRH": {
                "name": "Zurich Airport",
                "city": "Zurich",
                "city_code": "ZRH",
                "country": "Switzerland",
                "country_code": "CH",
                "time_zone": "Europe/Zurich",
                "rates": 57,
                "average_rate": 4.14,
                "coordinates": {
                    "lat": 47.450603,
                    "lon": 8.561746
                }
            }
        },
        "search_id": "c8320278-8bed-457b-ab6b-4aab6bf1ab58",
        "chunk_id": "9574c92e-b343-11ea-831f-9aafcec3742d",
        "meta": {
            "uuid": "c8320278-8bed-457b-ab6b-4aab6bf1ab58",
            "gates": [
                {
                    "count": 12,
                    "good_count": 12,
                    "bad_count": null,
                    "duration": 2.189705,
                    "id": 112,
                    "gate_label": "flightnetwork_api",
                    "merged_codeshares": 0,
                    "error": {
                        "code": 0,
                        "tos": ""
                    },
                    "created_at": 1592691343965091335,
                    "server_name": "ams/start-chain-wt5wm",
                    "cache": false,
                    "cache_search_uuid": ""
                },

                {
                    "count": 12,
                    "good_count": 12,
                    "bad_count": null,
                    "duration": 2.189705,
                    "id": 113,
                    "gate_label": "flightnetwork_api",
                    "merged_codeshares": 0,
                    "error": {
                        "code": 0,
                        "tos": ""
                    },
                    "created_at": 1592691343965091335,
                    "server_name": "ams/start-chain-wt5wm",
                    "cache": false,
                    "cache_search_uuid": ""
                }
                
            ]
        },
        "airlines": {
            "EK": {
                "id": 215,
                "iata": "EK",
                "average_rate": 3.81,
                "rates": 1824,
                "name": "Emirates"
            },
            "LH": {
                "id": 342,
                "iata": "LH",
                "average_rate": 3.44,
                "rates": 1417,
                "name": "Lufthansa",
                "alliance_name": "Star Alliance",
                "site_name": "Lufthansa.com"
            },
            "LX": {
                "id": 472,
                "iata": "LX",
                "average_rate": 3.57,
                "rates": 424,
                "name": "Swiss International Air Lines",
                "alliance_name": "Star Alliance",
                "site_name": "SWISS"
            }
        },
        "gates_info": {
            "112": {
                "productivity": 1,
                "id": 112,
                "label": "FlightNetwork",
                "payment_methods": [
                    "card"
                ],
                "mobile_version": false,
                "currency_code": "rub"
            },
            
            "113": {
                "productivity": 1,
                "id": 112,
                "label": "FlightNetwork",
                "payment_methods": [
                    "card"
                ],
                "mobile_version": false,
                "currency_code": "rub"
            }

        },
        "flight_info": {
            "YEK4": {
                "amenities": {
                    "wifi": {
                        "exists": true,
                        "paid": true,
                        "type": "wifi",
                        "summary": "paid_wifi"
                    },
                    "layout": {
                        "row_layout": "3_4_3",
                        "type": "forward",
                        "quality": "standard"
                    },
                    "power": {
                        "exists": true,
                        "paid": false,
                        "type": "power_usb",
                        "summary": "free_power_usb"
                    },
                    "food": {
                        "exists": true,
                        "paid": false,
                        "type": "meal",
                        "summary": "free_meal"
                    },
                    "entertainment": {
                        "exists": true,
                        "paid": false,
                        "type": "on_demand",
                        "summary": "free_on_demand"
                    },
                    "beverage": {
                        "type": "alcoholic_and_nonalcoholic",
                        "quality": "standard",
                        "alcoholic_paid": false,
                        "nonalcoholic_paid": false,
                        "exists": true
                    }
                },
                "seat": {
                    "width_description": "standard",
                    "width": 43,
                    "pitch": 81,
                    "type": "above_average_legroom"
                },
                "delay": {
                    "min": 1,
                    "max": 896,
                    "ontime_percent": 0.6666666666666666,
                    "mean": 60
                }
            },
            "YEK3": {
                "amenities": {
                    "wifi": {
                        "exists": true,
                        "paid": true,
                        "type": "wifi",
                        "summary": "paid_wifi"
                    },
                    "layout": {
                        "row_layout": "3_4_3",
                        "type": "forward",
                        "quality": "standard"
                    },
                    "power": {
                        "exists": true,
                        "paid": false,
                        "type": "power_usb",
                        "summary": "free_power_usb"
                    },
                    "food": {
                        "exists": true,
                        "paid": false,
                        "type": "meal",
                        "summary": "free_meal"
                    },
                    "entertainment": {
                        "exists": true,
                        "paid": false,
                        "type": "on_demand",
                        "summary": "free_on_demand"
                    },
                    "beverage": {
                        "type": "alcoholic_and_nonalcoholic",
                        "quality": "standard",
                        "alcoholic_paid": false,
                        "nonalcoholic_paid": false,
                        "exists": true
                    }
                },
                "seat": {
                    "width_description": "standard",
                    "width": 43,
                    "pitch": 81,
                    "type": "above_average_legroom"
                },
                "delay": {
                    "min": 0,
                    "max": 328,
                    "ontime_percent": 0.5192307692307693,
                    "mean": 37
                }
            },
            "YEK1": {
                "amenities": {
                    "wifi": {
                        "exists": true,
                        "paid": true,
                        "type": "wifi",
                        "summary": "paid_wifi"
                    },
                    "layout": {
                        "row_layout": "3_4_3",
                        "type": "forward",
                        "quality": "standard"
                    },
                    "power": {
                        "exists": true,
                        "paid": false,
                        "type": "power_usb",
                        "summary": "free_power_usb"
                    },
                    "food": {
                        "exists": true,
                        "paid": false,
                        "type": "meal",
                        "summary": "free_meal"
                    },
                    "entertainment": {
                        "exists": true,
                        "paid": false,
                        "type": "on_demand",
                        "summary": "free_on_demand"
                    },
                    "beverage": {
                        "type": "alcoholic_and_nonalcoholic",
                        "quality": "standard",
                        "alcoholic_paid": false,
                        "nonalcoholic_paid": false,
                        "exists": true
                    }
                },
                "seat": {
                    "width_description": "standard",
                    "width": 43,
                    "pitch": 81,
                    "type": "above_average_legroom"
                },
                "delay": {
                    "min": 0,
                    "max": 111,
                    "ontime_percent": 0.5675675675675675,
                    "mean": 21
                }
            },
            "YLH922": {
                "amenities": {
                    "wifi": {
                        "exists": false,
                        "paid": false,
                        "summary": "no_wifi"
                    },
                    "layout": {
                        "row_layout": "3_3",
                        "type": "forward",
                        "quality": "standard"
                    },
                    "power": {
                        "exists": true,
                        "paid": false,
                        "type": "usb",
                        "summary": "free_usb"
                    },
                    "food": {
                        "exists": true,
                        "paid": false,
                        "summary": "free_food"
                    },
                    "entertainment": {
                        "exists": false,
                        "paid": false,
                        "type": "none",
                        "summary": "no_none"
                    },
                    "beverage": {
                        "type": "alcoholic_and_nonalcoholic",
                        "quality": "standard",
                        "alcoholic_paid": false,
                        "nonalcoholic_paid": false,
                        "exists": true
                    }
                },
                "seat": {
                    "width_description": "standard",
                    "width": 43,
                    "pitch": 73,
                    "type": "below_average_legroom"
                },
                "delay": {
                    "min": 0,
                    "max": 63,
                    "ontime_percent": 0.8852459016393442,
                    "mean": 16
                }
            },
            "YLH900": {
                "amenities": {
                    "wifi": {
                        "exists": false,
                        "paid": false,
                        "summary": "no_wifi"
                    },
                    "layout": {
                        "row_layout": "3_3",
                        "type": "forward",
                        "quality": "standard"
                    },
                    "power": {
                        "exists": true,
                        "paid": false,
                        "type": "usb",
                        "summary": "free_usb"
                    },
                    "food": {
                        "exists": true,
                        "paid": false,
                        "summary": "free_food"
                    },
                    "entertainment": {
                        "exists": false,
                        "paid": false,
                        "type": "none",
                        "summary": "no_none"
                    },
                    "beverage": {
                        "type": "alcoholic_and_nonalcoholic",
                        "quality": "standard",
                        "alcoholic_paid": false,
                        "nonalcoholic_paid": false,
                        "exists": true
                    }
                },
                "seat": {
                    "width_description": "standard",
                    "width": 43,
                    "pitch": 73,
                    "type": "below_average_legroom"
                },
                "delay": {
                    "min": 1,
                    "max": 122,
                    "ontime_percent": 0.5892857142857143,
                    "mean": 24
                }
            },
            "YEK2": {
                "amenities": {
                    "wifi": {
                        "exists": true,
                        "paid": true,
                        "type": "wifi",
                        "summary": "paid_wifi"
                    },
                    "layout": {
                        "row_layout": "3_4_3",
                        "type": "forward",
                        "quality": "standard"
                    },
                    "power": {
                        "exists": true,
                        "paid": false,
                        "type": "power_usb",
                        "summary": "free_power_usb"
                    },
                    "food": {
                        "exists": true,
                        "paid": false,
                        "type": "meal",
                        "summary": "free_meal"
                    },
                    "entertainment": {
                        "exists": true,
                        "paid": false,
                        "type": "on_demand",
                        "summary": "free_on_demand"
                    },
                    "beverage": {
                        "type": "alcoholic_and_nonalcoholic",
                        "quality": "standard",
                        "alcoholic_paid": false,
                        "nonalcoholic_paid": false,
                        "exists": true
                    }
                },
                "seat": {
                    "width_description": "standard",
                    "width": 43,
                    "pitch": 81,
                    "type": "above_average_legroom"
                },
                "delay": {
                    "min": 0,
                    "max": 44,
                    "ontime_percent": 0.8260869565217391,
                    "mean": 12
                }
            },
            "YEK45": {
                "amenities": {
                    "wifi": {
                        "exists": true,
                        "paid": true,
                        "type": "wifi",
                        "summary": "paid_wifi"
                    },
                    "layout": {
                        "row_layout": "3_4_3",
                        "type": "forward",
                        "quality": "standard"
                    },
                    "power": {
                        "exists": true,
                        "paid": false,
                        "type": "power_usb",
                        "summary": "free_power_usb"
                    },
                    "food": {
                        "exists": true,
                        "paid": false,
                        "type": "meal",
                        "summary": "free_meal"
                    },
                    "entertainment": {
                        "exists": true,
                        "paid": false,
                        "type": "on_demand",
                        "summary": "free_on_demand"
                    },
                    "beverage": {
                        "type": "alcoholic_and_nonalcoholic",
                        "quality": "standard",
                        "alcoholic_paid": false,
                        "nonalcoholic_paid": false,
                        "exists": true
                    }
                },
                "seat": {
                    "width_description": "standard",
                    "width": 43,
                    "pitch": 81,
                    "type": "above_average_legroom"
                },
                "delay": {
                    "min": 1,
                    "max": 233,
                    "ontime_percent": 0.5,
                    "mean": 27
                }
            },
            "YLX327": {
                "amenities": {
                    "wifi": {
                        "exists": false,
                        "paid": false,
                        "summary": "no_wifi"
                    },
                    "layout": {
                        "row_layout": "3_3",
                        "type": "forward",
                        "quality": "standard"
                    },
                    "power": {
                        "exists": false,
                        "paid": false,
                        "type": "none",
                        "summary": "no_none"
                    },
                    "food": {
                        "exists": true,
                        "paid": false,
                        "summary": "free_food"
                    },
                    "entertainment": {
                        "exists": false,
                        "paid": false,
                        "type": "none",
                        "summary": "no_none"
                    },
                    "beverage": {
                        "type": "alcoholic_and_nonalcoholic",
                        "quality": "standard",
                        "alcoholic_paid": false,
                        "nonalcoholic_paid": false,
                        "exists": true
                    }
                },
                "seat": {
                    "width_description": "wider",
                    "width": 43,
                    "pitch": 74,
                    "type": "below_average_legroom"
                },
                "delay": {
                    "min": 0,
                    "max": 58,
                    "ontime_percent": 0.7868852459016393,
                    "mean": 20
                }
            },
            "YEK88": {
                "amenities": {
                    "wifi": {
                        "exists": true,
                        "paid": true,
                        "type": "wifi",
                        "summary": "paid_wifi"
                    },
                    "layout": {
                        "row_layout": "3_4_3",
                        "type": "forward",
                        "quality": "standard"
                    },
                    "power": {
                        "exists": true,
                        "paid": false,
                        "type": "power_usb",
                        "summary": "free_power_usb"
                    },
                    "food": {
                        "exists": true,
                        "paid": false,
                        "type": "meal",
                        "summary": "free_meal"
                    },
                    "entertainment": {
                        "exists": true,
                        "paid": false,
                        "type": "on_demand",
                        "summary": "free_on_demand"
                    },
                    "beverage": {
                        "type": "alcoholic_and_nonalcoholic",
                        "quality": "standard",
                        "alcoholic_paid": false,
                        "nonalcoholic_paid": false,
                        "exists": true
                    }
                },
                "seat": {
                    "width_description": "standard",
                    "width": 43,
                    "pitch": 81,
                    "type": "above_average_legroom"
                },
                "delay": {
                    "min": 0,
                    "max": 183,
                    "ontime_percent": 0.9682539682539683,
                    "mean": 29
                }
            }
        },
        "filters_boundary": {
            "flights_duration": {
                "max": 1635,
                "min": 415
            },
            "departure_time_1": {
                "max": "14:15",
                "min": "07:45"
            },
            "departure_minutes_1": {
                "max": 855,
                "min": 465
            },
            "arrival_time_1": {
                "max": "22:10",
                "min": "08:40"
            },
            "arrival_datetime_0": {
                "max": 1593215100,
                "min": 1593130800
            },
            "airports": {
                "arrival": [
                    "DXB"
                ],
                "departure": [
                    "LHR"
                ]
            },
            "stops_duration": {
                "max": 1125,
                "min": 495
            },
            "price": {
                "max": 210119,
                "min": 58481
            },
            "stops_count": {
                "0": 58481,
                "1": 125003
            },
            "departure_time_0": {
                "max": "20:40",
                "min": "14:20"
            },
            "departure_minutes_0": {
                "max": 1240,
                "min": 860
            },
            "arrival_time_0": {
                "max": "23:45",
                "min": "00:20"
            },
            "arrival_datetime_1": {
                "max": 1593506400,
                "min": 1593433500
            }
        },
        "segments": [
            {
                "origin": "LON",
                "origin_country": "GB",
                "original_origin": "LON",
                "destination": "DXB",
                "destination_country": "AE",
                "original_destination": "DXB",
                "date": "2020-06-25",
                "depart_date": "2020-06-25"
            },
            {
                "origin": "DXB",
                "origin_country": "AE",
                "original_origin": "DXB",
                "destination": "LON",
                "destination_country": "GB",
                "original_destination": "LON",
                "date": "2020-06-29",
                "depart_date": "2020-06-29"
            }
        ],
        "market": "gb",
        "clean_marker": "189998",
        "open_jaw": false,
        "currency": "rub",
        "initiated_at": "2020-06-20 22:15:43.965091"
    }


]



module.exports = OutPutJsonArray;
