import { BrowserModule } from "@angular/platform-browser";
import { Injectable, NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AppComponent } from "./app.component";
import { NavigationModule } from "./navigation/navigation.module";
import { HomeModule } from "./home/home.module";
import { FlyListModule } from "./fly-list/fly-list.module";
import { CookieService } from "ngx-cookie-service";
import { AddPostComponent } from "./components/add-posts/add-posts.component";
import { PostDetailsComponent } from "./components/post-details/post-details.component";
import { PostsListComponent } from "./components/posts-list/posts-list.component";
import { FormsModule } from "@angular/forms";
import {
  HttpClientModule,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from "@angular/common/http";
import { HomeComponent } from "./components/home/home.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { BoardAdminComponent } from "./components/board-admin/board-admin.component";
import { BoardUserComponent } from "./components/board-user/board-user.component";
import { BoardModeratorComponent } from "./components/board-moderator/board-moderator.component";
import { authInterceptorProviders } from "./helpers/auth.interceptor";
import { AppBootstrapModule } from "./app-bootstrap/app-bootstrap.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AngularMaterialModule } from "./angular-material/angular-material.module";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { ChartsComponent } from "./components/charts/charts.component";
import { ChartsModule } from "ng2-charts";
import { AddCategoryComponent } from "./components/add-category/add-category.component";
import { DragDropModule } from "@angular/cdk/drag-drop";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { PostsCategoryComponent } from "./components/posts-category/posts-category.component";
import { RecentPostsComponent } from "./components/recent-posts/recent-posts.component";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { CategoriesComponent } from "./components/categories/categories.component";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { AlertComponent } from "./components/alert/alert.component";
import { NgxWebstorageModule } from "ngx-webstorage";
import { DestinationAddComponent } from "./components/destination-add/destination-add.component";
import { DestinationListComponent } from "./components/destination-list/destination-list.component";
import { DestinationDetailsComponent } from "./components/destination-details/destination-details.component";
import { AboutUsComponent } from "./components/about-us/about-us.component";
import { TermsConditionsComponent } from "./components/terms-conditions/terms-conditions.component";
import { CookiePolicyComponent } from "./components/cookie-policy/cookie-policy.component";
import { PrivacyPolicyComponent } from "./components/privacy-policy/privacy-policy.component";
import { TopbarComponent } from "./components/topbar/topbar.component";
import { ErrorNotfoundComponent } from "./components/error-notfound/error-notfound.component";
import { ErrorInternalserverComponent } from "./components/error-internalserver/error-internalserver.component";
import { UserSidebarComponent } from "./components/user-sidebar/user-sidebar.component";
import { UserTableComponent } from "./components/user-table/user-table.component";
import { AddApiComponent } from "./components/add-api/add-api.component";
import { PendingUsersComponent } from "./components/pending-users/pending-users.component";

@NgModule({
  declarations: [
    AppComponent,
    AddPostComponent,
    PostDetailsComponent,
    PostsListComponent,
    HomeComponent,
    ProfileComponent,
    BoardAdminComponent,
    BoardUserComponent,
    BoardModeratorComponent,
    ChartsComponent,
    AddCategoryComponent,
    PostsCategoryComponent,
    RecentPostsComponent,
    SidebarComponent,
    CategoriesComponent,
    AboutUsComponent,
    AlertComponent,
    DestinationAddComponent,
    DestinationListComponent,
    DestinationDetailsComponent,
    AboutUsComponent,
    TermsConditionsComponent,
    CookiePolicyComponent,
    PrivacyPolicyComponent,
    TopbarComponent,
    ErrorNotfoundComponent,
    ErrorInternalserverComponent,
    UserSidebarComponent,
    UserTableComponent,
    AddApiComponent,
    PendingUsersComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NavigationModule,
    HomeModule,
    FlyListModule,
    FormsModule,
    HttpClientModule,
    AppBootstrapModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    ChartsModule,
    DragDropModule,
    MatButtonModule,
    MatCheckboxModule,
    NgxWebstorageModule.forRoot(),
  ],
  providers: [CookieService, authInterceptorProviders],
  bootstrap: [AppComponent],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}

// @Injectable()
// export class ErrorInterceptor implements HttpInterceptor {
//     constructor(private accountService: AccountService) {}

//     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//         return next.handle(request).pipe(catchError(err => {
//             if (err.status === 401) {
//                 // auto logout if 401 response returned from api
//                 this.accountService.logout();
//             }

//             const error = err.error.message || err.statusText;
//             return throwError(error);
//         }))
//     }
// }
