import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PostService } from "src/app/services/posts.service";

@Component({
  selector: "app-posts-list",
  templateUrl: "./posts-list.component.html",
  styleUrls: ["./posts-list.component.css"],
})
export class PostsListComponent implements OnInit {
  posts: any;
  currentpost = null;
  currentIndex = -1;
  title = "";
  img = "";

  constructor(
    private postService: PostService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.retrieveposts();
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.posts, event.previousIndex, event.currentIndex);
    // this.postService.update(this.currentpost.id, this.currentpost)
    //   .subscribe(
    //     response => {
    //       console.log(response);
    //     },
    //     error => {
    //       console.log(error);
    //     });
  }

  retrieveposts(): void {
    this.postService.getAll().subscribe(
      (data) => {
        this.posts = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  deletepost(): void {
    this.postService.delete(this.currentpost.id).subscribe(
      (response) => {
        console.log(response);
        location.reload();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  refreshList(): void {
    this.retrieveposts();
    this.currentpost = null;
    this.currentIndex = -1;
  }

  setActivepost(post, index): void {
    this.currentpost = post;
    this.currentIndex = index;
    this.img = this.currentpost.img;
  }

  removeAllposts(): void {
    // this.postService.deleteAll()
    //   .subscribe(
    //     response => {
    //       console.log(response);
    //       this.retrieveposts();
    //     },
    //     error => {
    //       console.log(error);
    //     });
  }

  searchTitle(): void {
    //   this.postService.findByTitle(this.title)
    //     .subscribe(
    //       data => {
    //         this.posts = data;
    //         console.log(data);
    //       },
    //       error => {
    //         console.log(error);
    //       });
    // }
  }
}
