const config = require('../config.json');
const db = require('../helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Category.findAll();
}

async function getById(id) {
    return await getCategory(id);
}

async function create(params) {
    // validate
    if (await db.Category.findOne({ where: { categoryName: params.categoryName } })) {
        throw 'CategoryName "' + params.categoryName + '" is already existing';
    }

    // save categry
    await db.Category.create(params);
}

async function update(id, params) {
    const category = await getCategory(id);

    // validate
    const categoryNameChanged = params.categoryName && category.categoryName !== params.categoryName;
    if (categoryNameChanged && await db.Category.findOne({ where: { categoryName: params.categoryName } })) {
        throw 'CategoryName "' + params.categoryName + '" is already taken';
    }
    // copy params to category and save
    Object.assign(category, params);
    await category.save();
}

async function _delete(id) {
    const category = await getCategory(id);
    await category.destroy();
}

// helper functions

async function getCategory(id) {
    const category = await db.Category.findByPk(id);
    if (!category) throw 'Category not found';
    return category;
}