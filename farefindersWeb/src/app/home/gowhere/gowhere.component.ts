import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gowhere',
  templateUrl: './gowhere.component.html',
  styleUrls: ['./gowhere.component.css']
})
export class GowhereComponent implements OnInit {
  mobile: boolean;
  web: boolean;

  constructor() { }

  ngOnInit(): void {
    if (window.screen.width < 480) { // 768px portrait
      this.mobile = true;
    }else{
      this.web=true;
    }
  }

}
