import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { first } from "rxjs/operators";
import { AccountService } from "src/app/services/account.service";
import { AlertService } from "src/app/services/alert.service";
import { DestinationService } from "src/app/services/destination.service";

@Component({
  selector: "app-destination-add",
  templateUrl: "./destination-add.component.html",
  styleUrls: ["./destination-add.component.css"],
})
export class DestinationAddComponent implements OnInit {
  form: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private destinationService: DestinationService,
    private formBuilder: FormBuilder,
    private AccountService: AccountService,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.AccountService.getUserPrivilage();
    this.form = this.formBuilder.group({
      destinationName: ["", Validators.required],
      destinationImage: [""],
    });
  }

  get f() {
    return this.form.controls;
  }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    console.log(this.form.value);
    this.destinationService
      .create(this.form.value)
      .pipe(first())
      .subscribe(
        (data) => {
          this.alertService.success("Destination creation successful", {
            keepAfterRouteChange: true,
          });
          // this.router.navigate(['../login'], { relativeTo: this.route });
          this.router.navigate(["/admin"]);
        },
        (error) => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
  }
}
