import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GowhereComponent } from './gowhere.component';

describe('GowhereComponent', () => {
  let component: GowhereComponent;
  let fixture: ComponentFixture<GowhereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GowhereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GowhereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
