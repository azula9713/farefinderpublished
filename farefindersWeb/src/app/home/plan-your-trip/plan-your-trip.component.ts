import { Component, OnInit } from '@angular/core';
import { DestinationService } from 'src/app/services/destination.service';

@Component({
  selector: 'app-plan-your-trip',
  templateUrl: './plan-your-trip.component.html',
  styleUrls: ['./plan-your-trip.component.css']
})
export class PlanYourTripComponent implements OnInit {
  currentdestination: any;
  destinations: any;
  cards: any
  
  constructor(
    private destinationService: DestinationService
  ) { }

  ngOnInit(): void {
    this.retrievedestinations();
  }

  

  retrievedestinations(): void {
    this.destinationService.getAll().subscribe(
      (data) => {
        this.destinations = data;
        console.log(data);
        this.cards = [
          { 'path': this.destinations[0].destinationImage, 'Heading': this.destinations[0].destinationName, 'subText': 'Flights' },
          { 'path': this.destinations[1].destinationImage, 'Heading': this.destinations[1].destinationName, 'subText': 'Flights' },
          { 'path': this.destinations[2].destinationImage, 'Heading': this.destinations[2].destinationName, 'subText': 'Flights' },
          { 'path': this.destinations[3].destinationImage, 'Heading': this.destinations[3].destinationName, 'subText': 'Flights' },
          { 'path': this.destinations[4].destinationImage, 'Heading': this.destinations[4].destinationName, 'subText': 'Flights' },
          { 'path': this.destinations[5].destinationImage, 'Heading': this.destinations[5].destinationName, 'subText': 'Flights' },
          { 'path': this.destinations[6].destinationImage, 'Heading': this.destinations[6].destinationName, 'subText': 'Flights' },
          { 'path': this.destinations[7].destinationImage, 'Heading': this.destinations[7].destinationName, 'subText': 'Flights' },
          { 'path': this.destinations[8].destinationImage, 'Heading': this.destinations[8].destinationName, 'subText': 'Flights' },
        ];
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
