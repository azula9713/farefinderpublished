import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { FlyListRoutingModule } from './fly-list-routing.module';
import { MainpageComponent } from './mainpage/mainpage.component';
import { HomeModule } from '../home/home.module';

import { BsDatepickerModule  } from 'ngx-bootstrap/datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NgInitFlyListDirective } from './ng-init-fly-list.directive';
import { NgxSpinnerModule } from "ngx-spinner";
import { TicketComponent } from './ticket/ticket.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';


@NgModule({
  declarations: [MainpageComponent, NgInitFlyListDirective, TicketComponent],
  imports: [
    CommonModule,
    NgxSkeletonLoaderModule,
    FlyListRoutingModule,
    HomeModule, FormsModule,ReactiveFormsModule,
    BsDatepickerModule.forRoot() , BrowserAnimationsModule , TypeaheadModule.forRoot(), NgxSpinnerModule
    
  ],
  exports:[MainpageComponent,TicketComponent],
  bootstrap: []
})
export class FlyListModule { }
