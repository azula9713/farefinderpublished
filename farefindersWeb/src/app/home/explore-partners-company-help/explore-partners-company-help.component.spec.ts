import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExplorePartnersCompanyHelpComponent } from './explore-partners-company-help.component';

describe('ExplorePartnersCompanyHelpComponent', () => {
  let component: ExplorePartnersCompanyHelpComponent;
  let fixture: ComponentFixture<ExplorePartnersCompanyHelpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExplorePartnersCompanyHelpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExplorePartnersCompanyHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
