import { Component, OnInit } from "@angular/core";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { ActivatedRoute, NavigationEnd, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AccountService } from "src/app/services/account.service";
import { AlertService } from "src/app/services/alert.service";
import { first } from "rxjs/operators";

@Component({
  selector: "app-nav-header",
  templateUrl: "./nav-header.component.html",
  styleUrls: ["./nav-header.component.css"],
})
export class NavHeaderComponent implements OnInit {
  display = false;
  loginbtn = true;
  closeResult: string;
  form: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  roles: any;
  username: any;
  firstName: any;
  lastName: any;
  email: any;
  fullname: string;

  constructor(
    public router: Router,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private accountService: AccountService,
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });

    this.form = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
    });

    // get return url from route parameters or default to '/'
    // this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";

    this.getUserDetails();
  }

  open(content) {
    this.display = !this.display;
    this.modalService
      .open(content, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  getUserDetails(): void {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user !== null) {
      this.loginbtn = false;
      this.username = user.username;
      this.firstName = user.firstName;
      this.lastName = user.lastName;
      this.email = user.email;
      this.fullname = this.firstName + " " + this.lastName;
      console.log(this.username);
    }
  }

  handleSignOut() {
    this.router.navigate(["/"]);
    localStorage.clear();
    location.reload();
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  get f() {
    return this.form.controls;
  }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    this.accountService
      .login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        (data) => {
          let loggedUser = JSON.parse(localStorage.getItem("user"));

          if(loggedUser.isApproved ==false){
            localStorage.clear();
            location.reload();
          }
          this.roles = loggedUser.userrole;
          if (loggedUser.userrole.includes("admin")) {
            localStorage.clear();
            location.reload();
          }
          console.log("logged in data is " + this.roles);
          let urlparam = "/user";
          console.log(urlparam);
          this.router.navigate([urlparam]);
          this.getUserDetails();
        },
        (error) => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
  }
}
