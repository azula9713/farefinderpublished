import { Component, OnInit } from "@angular/core";
import { PostService } from "src/app/services/posts.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-post-details",
  templateUrl: "./post-details.component.html",
  styleUrls: ["./post-details.component.css"],
})
export class PostDetailsComponent implements OnInit {
  currentpost = null;
  message = "";
  img = "";
  imgURL1: any;
  image1: any;

  constructor(
    private postService: PostService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.message = "";
    this.getpost(this.route.snapshot.paramMap.get("id"));
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      if (
        event.target.files[0].type === "image/png" ||
        event.target.files[0].type === "image/jpg"
      ) {
        reader.onload = (_event) => {
          // called once readAsDataURL is completed
          this.imgURL1 = reader.result;
          this.image1 = this.imgURL1.toString().slice(22);
        };
      } else if (event.target.files[0].type === "image/jpeg") {
        reader.onload = (_event) => {
          // called once readAsDataURL is completed
          this.imgURL1 = reader.result;
          this.image1 = this.imgURL1.toString().slice(23);
        };
      }
    }
  }

  getpost(id): void {
    this.postService.getById(id).subscribe(
      (data) => {
        this.currentpost = data;
        this.img = this.currentpost.img;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  updatepublished(status): void {
    const data = {
      city: this.currentpost.city,
      description: this.currentpost.description,
      published: status,
    };

    this.postService.update(this.currentpost.id, data).subscribe(
      (response) => {
        this.currentpost.published = status;
        console.log(response);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  updatepost(): void {
    this.postService.update(this.currentpost.id, this.currentpost).subscribe(
      (response) => {
        console.log(response);
        this.message = "The post was updated successfully!";
      },
      (error) => {
        console.log(error);
      }
    );
    this.router.navigate(["/admin"]);
  }

  deletepost(): void {
    this.postService.delete(this.currentpost.id).subscribe(
      (response) => {
        console.log(response);
        this.router.navigate(["/posts"]);
      },
      (error) => {
        console.log(error);
      }
    );
    this.router.navigate(["/admin"]);
  }
}
