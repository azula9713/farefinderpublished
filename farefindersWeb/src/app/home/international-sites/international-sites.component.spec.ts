import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternationalSitesComponent } from './international-sites.component';

describe('InternationalSitesComponent', () => {
  let component: InternationalSitesComponent;
  let fixture: ComponentFixture<InternationalSitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternationalSitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternationalSitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
