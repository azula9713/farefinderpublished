const { DataTypes, STRING } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        city: { type: DataTypes.STRING, allowNull: false },
        country: { type: DataTypes.STRING, allowNull: false },
        category: { type: DataTypes.STRING, allowNull: false },
        description: { type: DataTypes.STRING, allowNull: true },
        img: { type: DataTypes.STRING, allowNull: true }
    };

    return sequelize.define('Posts', attributes);
}