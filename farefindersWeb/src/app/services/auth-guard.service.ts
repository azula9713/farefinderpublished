import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private Router: Router) { }

  // canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot)
  //   : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   if (this.authService.isAuthenticate) {
  //     return true;
  //   } else {
  //     this.Router.navigate(['/login']);
  //     return false;
  //   }
  //   return true;
  // }
  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.authService.isAuthenticate) {
      return true;
    } else {
      this.Router.navigate(['/login']);
      return false;
    }
  }
}