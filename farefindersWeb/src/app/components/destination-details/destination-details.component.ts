import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { DestinationService } from "src/app/services/destination.service";

@Component({
  selector: "app-destination-details",
  templateUrl: "./destination-details.component.html",
  styleUrls: ["./destination-details.component.css"],
})
export class DestinationDetailsComponent implements OnInit {
  currentdestination = null;
  message = "";
  img = "";
  imgURL1: any;
  image1: any;

  constructor(
    private destinationService: DestinationService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.message = "";
    this.getdestination(this.route.snapshot.paramMap.get("id"));
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      if (
        event.target.files[0].type === "image/png" ||
        event.target.files[0].type === "image/jpg"
      ) {
        reader.onload = (_event) => {
          // called once readAsDataURL is completed
          this.imgURL1 = reader.result;
          this.image1 = this.imgURL1.toString().slice(22);
        };
      } else if (event.target.files[0].type === "image/jpeg") {
        reader.onload = (_event) => {
          // called once readAsDataURL is completed
          this.imgURL1 = reader.result;
          this.image1 = this.imgURL1.toString().slice(23);
        };
      }
    }
  }

  getdestination(id): void {
    this.destinationService.getById(id).subscribe(
      (data) => {
        this.currentdestination = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  updatedestination(): void {
    this.destinationService
      .update(this.currentdestination.id, this.currentdestination)
      .subscribe(
        (response) => {
          console.log(response);
          this.message = "The destination was updated successfully!";
        },
        (error) => {
          console.log(error);
        }
      );
  }
}
