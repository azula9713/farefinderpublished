import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorInternalserverComponent } from './error-internalserver.component';

describe('ErrorInternalserverComponent', () => {
  let component: ErrorInternalserverComponent;
  let fixture: ComponentFixture<ErrorInternalserverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErrorInternalserverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorInternalserverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
