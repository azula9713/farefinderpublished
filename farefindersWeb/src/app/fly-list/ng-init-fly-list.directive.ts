import {Directive, OnInit, Output, EventEmitter} from '@angular/core';

@Directive({
  selector: '[NgInitFlyList]'
})
export class NgInitFlyListDirective  implements OnInit{
  @Output()
  NgInitFlyList: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
      this.NgInitFlyList.emit();
  }
}




