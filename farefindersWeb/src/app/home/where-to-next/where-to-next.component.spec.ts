import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhereToNextComponent } from './where-to-next.component';

describe('WhereToNextComponent', () => {
  let component: WhereToNextComponent;
  let fixture: ComponentFixture<WhereToNextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhereToNextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhereToNextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
