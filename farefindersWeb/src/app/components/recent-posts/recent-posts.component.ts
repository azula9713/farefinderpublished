import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-recent-posts',
  templateUrl: './recent-posts.component.html',
  styleUrls: ['./recent-posts.component.css']
})
export class RecentPostsComponent implements OnInit {

  posts: any;
  currentpost = null;
  currentIndex = -1;
  title = '';
  img = '';

  constructor(private postService: PostService) { }

  ngOnInit(): void {
    this.retrieveposts();
  }

  retrieveposts(): void {
    this.postService.getAll()
      .subscribe(
        data => {
          this.posts = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  setActivepost(post, index): void {
    this.currentpost = post;
    this.currentIndex = index;
    this.img = this.currentpost.img;
  }

  // searchTitle(): void {
  //   this.postService.findByTitle(this.title)
  //     .subscribe(
  //       data => {
  //         this.posts = data;
  //         console.log(data);
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }
}
