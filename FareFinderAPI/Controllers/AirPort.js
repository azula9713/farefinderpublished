var express = require('express');
var router = express.Router();
var Logger = require('../Logger').Logger;
const axios = require('axios').default;

let airPort = [];


router.get('/AirPort', async (req, resultSet) => {

    try {
        airPort = [];
        const resp = await axios.get('http://api.travelpayouts.com/data/en/airports.json',
            {
                headers: { 'Content-Type': 'application/json' }
            });
        resultSet.send(createNewJson(resp.data));
    } catch (err) {
        Logger.error("Error - /AirPort " + err);
        resultSet.send([]);
    }

});





function createNewJson(body) {
    body.forEach(airportInfo => {
        airPort.push(createAirPortObj(airportInfo));
    });
    return airPort;
}



function createAirPortObj(airport) {
    return {
        country: airport.country_code,
        flightName: airport.name,
        searchName: airport.name + "(" + airport.code + ")",
        flightCode: airport.code
    }
}





module.exports = router;