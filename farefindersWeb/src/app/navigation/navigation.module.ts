import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { NavigationRoutingModule } from "./navigation-routing.module";
import { NavFooterComponent } from "./nav-footer/nav-footer.component";
import { NavHeaderComponent } from "./nav-header/nav-header.component";
import { InternationalSitesComponent } from "../home/international-sites/international-sites.component";
import { CovidBannerComponent } from "./covid-banner/covid-banner.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [NavHeaderComponent, NavFooterComponent, CovidBannerComponent],
  imports: [
    CommonModule,
    NavigationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [NavHeaderComponent, NavFooterComponent, CovidBannerComponent],
})
export class NavigationModule {}
