import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { apiUrlPrevix } from 'src/app/app-global';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http: HttpClient) { }

  getFlights() {
    return this.http.get<any[]>(apiUrlPrevix + 'AirPort').toPromise();
  }

  getFlights1(): Observable<any[]> {
    return this.http.get<any[]>(apiUrlPrevix + 'AirPort');
  }

  getAvilableTickets(body) {
    return this.http.post<any[]>(apiUrlPrevix + 'getAgentFares', body);
  }


}
