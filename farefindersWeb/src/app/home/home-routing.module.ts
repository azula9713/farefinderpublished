import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from '../fly-list/mainpage/mainpage.component';


const routes: Routes = [

  {path: 'Flights', component: MainpageComponent},

  {path: 'Flights/:flightType/:fromFlight/:toFlight/:fromFlightDate', component: MainpageComponent},
  {path: 'Flights/:flightType/:fromFlight/:toFlight/:fromFlightDate/:toFlightDate', component: MainpageComponent},

  {path: 'Flights/:flightType/:fromFlight1/:toFlight1/:fromFlightDate1/:fromFlight2/:toFlight2/:fromFlightDate2', component: MainpageComponent},
  {path: 'Flights/:flightType/:fromFlight1/:toFlight1/:fromFlightDate1/:fromFlight2/:toFlight2/:fromFlightDate2/:fromFlight3/:toFlight3/:fromFlightDate3', component: MainpageComponent},
  {path: 'Flights/:flightType/:fromFlight1/:toFlight1/:fromFlightDate1/:fromFlight2/:toFlight2/:fromFlightDate2/:fromFlight3/:toFlight3/:fromFlightDate3/:fromFlight4/:toFlight4/:fromFlightDate4', component: MainpageComponent},
  {path: 'Flights/:flightType/:fromFlight1/:toFlight1/:fromFlightDate1/:fromFlight2/:toFlight2/:fromFlightDate2/:fromFlight3/:toFlight3/:fromFlightDate3/:fromFlight4/:toFlight4/:fromFlightDate4/:fromFlight5/:toFlight5/:fromFlightDate5', component: MainpageComponent}


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
