import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { environment } from "src/environments/environment";
import { Destination } from "../models/destination";

@Injectable({
  providedIn: "root",
})
export class DestinationService {
  private destinationSubject: BehaviorSubject<Destination>;

  constructor(private http: HttpClient) {}

  public get destinationValue(): Destination {
    return this.destinationSubject.value;
  }

  create(destination: Destination) {
    return this.http.post(
      `${environment.apiUrl}/destination/create`,
      destination
    );
  }

  getAll() {
    return this.http.get<Destination[]>(
      `${environment.apiUrl}/destination/all`
    );
  }

  getById(id: string) {
    return this.http.get<Destination>(
      `${environment.apiUrl}/destination/${id}`
    );
  }

  update(id, params) {
    return this.http.put(`${environment.apiUrl}/destination/${id}`, params);
  }

  delete(id: string) {
    return this.http.delete(`${environment.apiUrl}/destination/${id}`);
  }
}
