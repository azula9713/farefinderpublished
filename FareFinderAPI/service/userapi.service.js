const config = require("../config.json");
const db = require("../helpers/db");

module.exports = {
  getAll,
  getById,
  create,
  getApiByUser,
  delete: _delete,
};

async function getAll() {
  return await db.Userapi.findAll();
}

async function getById(id) {
  return await getApi(id);
}

async function create(params) {
  // save post
  await db.Userapi.create(params);
}

async function _delete(id) {
  const userapi = await getApi(id);
  await userapi.destroy();
}

// helper functions

async function getApi(id) {
  const userapi = await db.Userapi.findByPk(id);
  if (!userapi) throw "API not found";
  return userapi;
}

async function getApiByUser(username) {
  const userapi = await db.Userapi.findAll({
    where: {
      apiusername: username,
    },
  });
  if (!userapi) throw "API not found";
  return userapi;
}
