import { Component, OnInit } from "@angular/core";
import { AccountService } from "src/app/services/account.service";

@Component({
  selector: "app-pending-users",
  templateUrl: "./pending-users.component.html",
  styleUrls: ["./pending-users.component.css"],
})
export class PendingUsersComponent implements OnInit {
  userID: any;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  company: string;
  role: any;
  apis: any;
  users: any;
  currentUser: any;
  isApproved: boolean;
  message: string;
  currentIndex: any;

  constructor(private AccountService: AccountService) {}

  ngOnInit() {
    this.AccountService.getUserPrivilage();
    this.getUserDetails();
  }

  getUserDetails(): void {
    this.AccountService.getAllPending().subscribe(
      (data) => {
        this.users = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  setActiveUser(user, index): void {
    this.currentUser = user.id;
    this.currentIndex = index;
  }

  approveUser(): void {
    const data = {
      isApproved: true,
    };
    console.log("current user is: " + this.currentUser);
    this.AccountService.approve(this.currentUser, data).subscribe(
      (response) => {
        console.log(response);
        this.message = "The User was approved successfully!";
      },
      (error) => {
        console.log(error);
      }
    );
    location.reload();
  }

  rejectUser(): void {
    console.log("current user is: " + this.currentUser);
    this.AccountService.delete(this.currentUser).subscribe(
      (response) => {
        console.log(response);
        this.message = "The User was deleted successfully!";
      },
      (error) => {
        console.log(error);
      }
    );
    location.reload();
  }
}
