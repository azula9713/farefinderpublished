const config = require('../config.json');
const db = require('../helpers/db');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await db.Destination.findAll();
}

async function getById(id) {
    return await getDestination(id);
}

async function create(params) {
    // validate
    if (await db.Destination.findOne({ where: { destinationName: params.destinationName } })) {
        throw 'destinationName "' + params.destinationName + '" is already existing';
    }

    // save categry
    await db.Destination.create(params);
}

async function update(id, params) {
    const Destination = await getDestination(id);

    // validate
    const destinationNameChanged = params.destinationName && Destination.destinationName !== params.destinationName;
    if (destinationNameChanged && await db.Destination.findOne({ where: { destinationName: params.destinationName } })) {
        throw 'destinationName "' + params.destinationName + '" is already taken';
    }
    // copy params to Destination and save
    Object.assign(Destination, params);
    await Destination.save();
}

async function _delete(id) {
    const Destination = await getDestination(id);
    await Destination.destroy();
}

// helper functions

async function getDestination(id) {
    const Destination = await db.Destination.findByPk(id);
    if (!Destination) throw 'Destination not found';
    return Destination;
}