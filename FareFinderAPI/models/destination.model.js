const { DataTypes, STRING } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        destinationName: { type: DataTypes.STRING, allowNull: false },
        destinationImage: { type: DataTypes.STRING, allowNull: true }
    };

    return sequelize.define('Destination', attributes);
}