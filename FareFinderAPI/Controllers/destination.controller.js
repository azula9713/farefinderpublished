const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('../middleware/validate_request');
const destinationService = require('../service/destination.service');

const prefix = '/destination'
    // routes
router.post(prefix + '/create', createSchema, create);
router.get(prefix + '/all', getAllCategories);
router.get(prefix + '/current', getCurrent);
router.get(prefix + '/:id', getById);
router.put(prefix + '/:id', updateSchema, update);
router.delete(prefix + '/:id', _delete);

module.exports = router;

function createSchema(req, res, next) {
    const schema = Joi.object({
        destinationName: Joi.string().required(),
        destinationImage: Joi.string()
    });
    validateRequest(req, next, schema);
}

function create(req, res, next) {
    console.log(req.body);
    destinationService.create(req.body)
        .then(() => res.json({ message: 'Creating Destination successful' }))
        .catch(next);
}

function getAllCategories(req, res, next) {
    destinationService.getAll()
        .then(destination => res.json(destination))
        .catch(next);
}

function getCurrent(req, res, next) {
    res.json(req.destination);
}

function getById(req, res, next) {
    destinationService.getById(req.params.id)
        .then(destination => res.json(destination))
        .catch(next);
}

function updateSchema(req, res, next) {
    const schema = Joi.object({
        destinationName: Joi.string().empty(''),
        destinationImage: Joi.string().empty(''),
    });
    validateRequest(req, next, schema);
}

function update(req, res, next) {
    destinationService.update(req.params.id, req.body)
        .then(destination => res.json(destination))
        .catch(next);
}

function _delete(req, res, next) {
    destinationService.delete(req.params.id)
        .then(() => res.json({ message: 'Destination deleted successfully' }))
        .catch(next);
}