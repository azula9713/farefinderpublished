import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HomeRoutingModule } from "./home-routing.module";
import { ContentComponent } from "./content/content.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RecomandationComponent } from "./recomandation/recomandation.component";
import { MydealsComponent } from "./mydeals/mydeals.component";
import { GowhereComponent } from "./gowhere/gowhere.component";
import { PlanYourTripComponent } from "./plan-your-trip/plan-your-trip.component";
import { ExplorePartnersCompanyHelpComponent } from "./explore-partners-company-help/explore-partners-company-help.component";
import { InternationalSitesComponent } from "./international-sites/international-sites.component";
import { WhereToNextComponent } from "./where-to-next/where-to-next.component";

import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { TypeaheadModule } from "ngx-bootstrap/typeahead";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { HttpClientModule } from "@angular/common/http";
import { NgInitDirective } from "./ng-init.directive";

import { NgxSpinnerModule } from "ngx-spinner";
import { SocialComponent } from "./social/social.component";

@NgModule({
  declarations: [
    ContentComponent,
    RecomandationComponent,
    MydealsComponent,
    GowhereComponent,
    PlanYourTripComponent,
    ExplorePartnersCompanyHelpComponent,
    InternationalSitesComponent,
    WhereToNextComponent,
    NgInitDirective,
    SocialComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    BrowserAnimationsModule,
    HttpClientModule,
    TypeaheadModule.forRoot(),
    NgxSpinnerModule,
  ],
  exports: [
    ContentComponent,
    RecomandationComponent,
    MydealsComponent,
    GowhereComponent,
    PlanYourTripComponent,
    ExplorePartnersCompanyHelpComponent,
    InternationalSitesComponent,
    WhereToNextComponent
  ],
  bootstrap: [],
})
export class HomeModule {}
