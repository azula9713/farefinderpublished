const express = require("express");
const router = express.Router();
const Joi = require("joi");
const validateRequest = require("../middleware/validate_request");
const apiService = require("../service/userapi.service");

const prefix = "/user/api";

// routes
router.post(prefix + "/create", createSchema, create);
router.get(prefix + "/all", getAllPosts);
router.get(prefix + "/current", getCurrent);
router.get(prefix + "/:id", getById);
router.delete(prefix + "/:id", _delete);
router.get(prefix + "/findby/q", getByCategory);

module.exports = router;

function createSchema(req, res, next) {
  const schema = Joi.object({
    apiurl: Joi.string().required(),
    apiname: Joi.string().required(),
    apiusername: Joi.string().required(),
    apidescription: Joi.string(),
  });
  validateRequest(req, next, schema);
}

function create(req, res, next) {
  console.log(req.body);
  apiService
    .create(req.body)
    .then(() => res.json({ message: "Creating API successful" }))
    .catch(next);
}

function getAllAPI(req, res, next) {
  apiService
    .getAll()
    .then((post) => res.json(post))
    .catch(next);
}

function getById(req, res, next) {
  apiService
    .getById(req.params.id)
    .then((post) => res.json(post))
    .catch(next);
}

function _delete(req, res, next) {
  apiService
    .delete(req.params.id)
    .then(() => res.json({ message: "API deleted successfully" }))
    .catch(next);
}

async function getApiByUser(req, res, next) {
  queryparam = req.query.username;
  apiService
    .getApiByUser(queryparam)
    .then((post) => res.json(post))
    .catch(next);
}
