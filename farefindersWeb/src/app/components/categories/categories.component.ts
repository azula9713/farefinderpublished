import { Component, OnInit } from "@angular/core";
import { CategoryService } from "src/app/services/category.service";
import { AccountService } from "src/app/services/account.service";
import { PostService } from 'src/app/services/posts.service';

@Component({
  selector: "app-categories",
  templateUrl: "./categories.component.html",
  styleUrls: ["./categories.component.css"],
})
export class CategoriesComponent implements OnInit {
  categories: any;
  currentcategory: any;
  currentIndex: any;
  posts: any;

  constructor(
    private categoryService: CategoryService,
    private AccountService: AccountService,
    private postService: PostService,
  ) {}

  ngOnInit(): void {
    this.AccountService.getUserPrivilage();
    this.retrievecategories();
  }

  retrievecategories(): void {
    this.categoryService.getAll().subscribe(
      (data) => {
        this.categories = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  setActivecategory(category, index): void {
    this.retrieveposts(category.categoryName);
    this.currentcategory = category;
    this.currentIndex = index;
  }

  refreshList(): void {
    this.retrievecategories();
    this.currentcategory = null;
    this.currentIndex = -1;
  }

  deletecategory(): void {
    this.categoryService.delete(this.currentcategory.id).subscribe(
      (response) => {
        console.log(response);
        location.reload();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  retrieveposts(category): void {
    console.log(category);
    this.postService.findByCategory(category).subscribe(
      (data) => {
        this.posts = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  // removeAllcategories(): void {
  //   this.categoryService.deleteAll()
  //     .subscribe(
  //       response => {
  //         console.log(response);
  //         this.retrievecategories();
  //       },
  //       error => {
  //         console.log(error);
  //       });
  // }
}
