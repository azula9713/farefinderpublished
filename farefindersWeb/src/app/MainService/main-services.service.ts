import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { apiUrlPrevix } from 'src/app/app-global';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MainServicesService {

  constructor(private http: HttpClient) { }

  // CreateFlightOffers(obj, type): Observable<any[]> {
  //     return this.http.post<any[]>(apiUrlPrevix + 'Flight', obj);
  // }


  // CreateFlightOffers(obj) {
  //   return this.http.post<any[]>(apiUrlPrevix + 'Flight', obj).toPromise();
  // }



  getAllAirLines() {
    return this.http.get<any[]>(apiUrlPrevix + 'AirLine').toPromise();
  }


  getSearchId(obj) {
    return this.http.post<any>(apiUrlPrevix + 'Flight', obj).toPromise();
  }

  getDirectTickets(searchId, searchType, isDirect) {
    return this.http.get<any[]>(apiUrlPrevix + 'Flight/' + searchId + '/' + searchType + '/' + isDirect).toPromise();
  }

  getDirectTickets1(searchId, searchType, isDirect): Observable<any[]> {
    return this.http.get<any[]>(apiUrlPrevix + 'Flight/' + searchId + '/' + searchType + '/' + isDirect);
  }

  getSearchId1(obj) {
    return this.http.post<any>(apiUrlPrevix + 'Flight', obj);
  }

  // get the agent website link
  getAgentSiteLink(obj) {
    return this.http.post<any>(apiUrlPrevix + 'Flight/getagentlink', obj);
  }


}
