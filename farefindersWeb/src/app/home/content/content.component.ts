import { Component, OnInit } from "@angular/core";
import { FormControl, FormBuilder, FormGroup, FormArray } from "@angular/forms";
import { CategoryService } from "src/app/services/category.service";
import { PostService } from "src/app/services/posts.service";
import { TokenStorageService } from "src/app/services/token-storage.service";

@Component({
  selector: "app-content",
  templateUrl: "./content.component.html",
  styleUrls: ["./content.component.css"],
})
export class ContentComponent implements OnInit {
  categories: any;
  categoryName: any;
  posts: any;
  currentpost = null;
  currentIndex = -1;
  title = "";
  img = "";
  post1 = null;
  post2 = null;
  post3 = null;
  list = [];
  loading: boolean = true;

  constructor(
    private fb: FormBuilder,
    private postService: PostService,
    private tokenStorage: TokenStorageService,
    private categoryService: CategoryService
  ) {}

  ngOnInit(): void {
    this.retrievecategories();
    this.getPostsByCategory();
  }

  onLoad() {
    this.loading = false;
  }

  retrievecategories(): void {
    this.categoryService.getAll().subscribe(
      (data) => {
        this.categories = data;
        console.log(this.categories);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getPostsByCategory(): void {
    this.categoryService.getAll().subscribe(
      (data) => {
        this.categories = data;

        for (let category of this.categories) {
          let catMap = new Map();
          catMap.set("categoryName", category.categoryName);
          this.categoryName = category.categoryName;
          this.postService.findByCategory(this.categoryName).subscribe(
            (postdata) => {
              this.posts = postdata;
              let newarr = [];
              for (let post of this.posts) {
                let postMap = new Map();
                postMap.set("city", post.city);
                postMap.set("country", post.country);
                postMap.set("description", post.description);
                postMap.set("image", post.img);
                newarr.push(postMap);
              }
              catMap.set("posts", newarr);
            },
            (error) => {
              console.log(error);
            }
          );
          this.list.push(catMap);
          catMap.get("categoryName");
          console.log(catMap.get("categoryName"));
          console.log(this.list);
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
