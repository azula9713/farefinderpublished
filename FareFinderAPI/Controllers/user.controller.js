const express = require("express");
const router = express.Router();
const Joi = require("joi");
const validateRequest = require("../middleware/validate_request");
const userService = require("../service/user.service");

// routes
router.post("/authenticate", authenticateSchema, authenticate);
router.post("/register", registerSchema, register);
router.get("/users", getAll);
router.get("/current", getCurrent);
router.get("/:id", getById);
router.put("/:id", updateSchema, update);
router.delete("/:id", _delete);
router.get("/pending/q", pendingUsers);

module.exports = router;

function authenticateSchema(req, res, next) {
  const schema = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required(),
  });
  validateRequest(req, next, schema);
}

function authenticate(req, res, next) {
  userService
    .authenticate(req.body)
    .then((user) => res.json(user))
    .catch(next);
}

function registerSchema(req, res, next) {
  const schema = Joi.object({
    username: Joi.string().required(),
    email: Joi.string().required(),
    password: Joi.string().min(3).required(),
    userrole: Joi.string().required(),
    country: Joi.string(),
    company: Joi.string(),
    telephone: Joi.string(),
    address: Joi.string(),
    iataNo: Joi.string(),
    jobTitle: Joi.string(),
    website: Joi.string(),
    companyLocation: Joi.string(),
    companyRegion: Joi.string(),
    flightPartnerType: Joi.string(),
    help: Joi.string(),
    firstName: Joi.string(),
    lastName: Joi.string(),
    isApproved: Joi.boolean(),
  });
  validateRequest(req, next, schema);
}

function register(req, res, next) {
  console.log(req.body);
  userService
    .create(req.body)
    .then(() => res.json({ message: "Registration successful" }))
    .catch(next);
}

function getAll(req, res, next) {
  userService
    .getAll()
    .then((users) => res.json(users))
    .catch(next);
}

function getCurrent(req, res, next) {
  res.json(req.user);
}

function getById(req, res, next) {
  userService
    .getById(req.params.id)
    .then((user) => res.json(user))
    .catch(next);
}

function updateSchema(req, res, next) {
  const schema = Joi.object({
    firstName: Joi.string().empty(""),
    lastName: Joi.string().empty(""),
    username: Joi.string().empty(""),
    password: Joi.string().min(3).empty(""),
    country: Joi.string().empty(""),
    company: Joi.string().empty(""),
    telephone: Joi.string().empty(""),
    address: Joi.string().empty(""),
    iataNo: Joi.string().empty(""),
    jobTitle: Joi.string().empty(""),
    website: Joi.string().empty(""),
    companyLocation: Joi.string().empty(""),
    companyRegion: Joi.string().empty(""),
    isApproved: Joi.boolean().empty(""),
  });
  validateRequest(req, next, schema);
}

function update(req, res, next) {
  userService
    .update(req.params.id, req.body)
    .then((user) => res.json(user))
    .catch(next);
}

function _delete(req, res, next) {
  userService
    .delete(req.params.id)
    .then(() => res.json({ message: "User deleted successfully" }))
    .catch(next);
}

async function pendingUsers(req, res, next) {
  queryparam = req.query.isApproved;
  userService
    .getPendingUsers(queryparam)
    .then((user) => res.json(user))
    .catch(next);
}
