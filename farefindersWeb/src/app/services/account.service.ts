import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";

import { environment } from "../../environments/environment";
import { User } from "../models/user";
import { LocalStorageService, SessionStorageService } from "ngx-webstorage";

@Injectable({ providedIn: "root" })
export class AccountService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;
  roles: any;

  constructor(
    private router: Router,
    private http: HttpClient,
    private localSt: LocalStorageService
  ) {
    this.userSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem("user"))
    );
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  login(username, password) {
    return this.http
      .post<User>(`${environment.apiUrl}/authenticate`, { username, password })
      .pipe(
        map((user) => {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem("user", JSON.stringify(user));
          this.userSubject.next(user);
          return user;
        })
      );
  }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem("user");
    this.userSubject.next(null);
    this.router.navigate(["/account/login"]);
  }

  register(user: User) {
    return this.http.post(`${environment.apiUrl}/register`, user);
  }

  getAll() {
    return this.http.get<User[]>(`${environment.apiUrl}/users`);
  }

  getAllPending() {
    let params = new URLSearchParams();
    params.append("isApproved", "0");
    return this.http.get<User[]>(`${environment.apiUrl}/pending/q?${params}`);
  }

  getAllApproved() {
    let params = new URLSearchParams();
    params.append("isApproved", "1");
    // return this.http.get<User[]>(`${environment.apiUrl}/pending/q`,params);
    return this.http.get(`${environment.apiUrl}/pending/q?${params}`);
  }

  getById(id: string) {
    return this.http.get<User>(`${environment.apiUrl}/${id}`);
  }

  approve(id, params) {
    return this.http.put(`${environment.apiUrl}/${id}`, params);
  }

  update(id, params) {
    return this.http.put(`${environment.apiUrl}/${id}`, params).pipe(
      map((x) => {
        // update stored user if the logged in user updated their own record
        if (id == this.userValue.id) {
          // update local storage
          const user = { ...this.userValue, ...params };
          localStorage.setItem("user", JSON.stringify(user));

          // publish updated user to subscribers
          this.userSubject.next(user);
        }
        return x;
      })
    );
  }

  delete(id: string) {
    return this.http.delete(`${environment.apiUrl}/${id}`).pipe(
      map((x) => {
        // auto logout if the logged in user deleted their own record
        if (id == this.userValue.id) {
          this.logout();
        }
        return x;
      })
    );
  }

  getUserPrivilage() {
    //   let item = localStorage.getItem("user");
    let item = JSON.parse(localStorage.getItem("user"));

    console.log(item);
    if (item == null) {
      this.router.navigate(["/"]);
    } else {
      this.roles = item.userrole;
      console.log(this.roles);
      if (this.roles == "admin") {
        this.roles = true;
        console.log("has admin privilage " + this.roles);
      } else if (this.roles == "user") {
        console.log("user role " + this.roles);
      } else {
        this.router.navigate(["/"]);
      }
    }
  }
}
