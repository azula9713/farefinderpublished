import { Component, OnInit } from "@angular/core";
import { AccountService } from "src/app/services/account.service";

@Component({
  selector: "app-user-table",
  templateUrl: "./user-table.component.html",
  styleUrls: ["./user-table.component.css"],
})
export class UserTableComponent implements OnInit {
  userID: any;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  company: string;
  role: any;
  apis: any;
  users: any;

  constructor(private AccountService: AccountService) {}

  ngOnInit() {
    this.AccountService.getUserPrivilage();
    this.getUserDetails();
  }

  getUserDetails(): void {
    this.AccountService.getAllApproved().subscribe(
      (data) => {
        this.users = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
