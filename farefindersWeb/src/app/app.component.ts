import { Component } from "@angular/core";
import { setTheme } from "ngx-bootstrap/utils";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "flysc";
  cookieMessage: "We are using cookies";
  cookieDismiss: "Close";
  cookieLinkText: "Got It";
  constructor(public router: Router) {
    setTheme("bs4");

    let cc = window as any;
    cc.cookieconsent.initialise({
      palette: {
        popup: {
          background: "#164969",
        },
        button: {
          background: "#ffe000",
          text: "#164969",
        },
      },
      theme: "classic",
      content: {
        message:
          "This website uses cookies We inform you that this site uses own, technical and third parties cookies to make sure our web page is user-friendly and to guarantee a high functionality of the webpage. By continuing to browse this website, you declare to accept the use of cookies.",
        dismiss: "Got It",
        // link: "Learn More"
        href: "https://farefinder.co.uk/cookiepolicy"
      },
    }); // or 'bs4'
  }
}
