import { Injectable } from '@angular/core';
import { callbackify } from 'util';

declare var $: any;

@Injectable({
  providedIn: 'root'
})
export class JqueryValidationManagerService {
  constructor() {}

  public maltiRangepicker(
    sliderId_return = '__departureTimes-return-slider',
    sliderId_outbound = '__departureTimes-outbound-slider',

    loweEndId = 'pH-lower-end',
    upperEndId = 'pH-upper-end'
  ) {
    $('#' + sliderId_return).ionRangeSlider({
      hide_min_max: true,
      skin: 'round',
      grid: true,
      grid_num: 1,
      onStart: function(data) {
        $('#' + loweEndId).val(data.from);
        $('#' + upperEndId).val(data.to);
      },
      onChange: function(data) {
        $('#' + loweEndId).val(data.from);
        $('#' + upperEndId).val(data.to);
      }
    });



    $('#' + sliderId_outbound).ionRangeSlider({
      hide_min_max: true,
      skin: 'round',
      grid: true,
      grid_num: 1,
      onStart: function(data) {
        console.log("sssss", data.from, data.to);
        $('#' + loweEndId).val(data.from);
        $('#' + upperEndId).val(data.to);
      },
      onChange: function(data) {
        console.log("cccccccccc", data.from, data.to);
        $('#' + loweEndId).val(data.from);
        $('#' + upperEndId).val(data.to);
      },
      onFinish: function (data) {
        console.log('ffffffffffff', data.from, data.to);
    },
    });
    

    $("#__jurneyDuration-slider").ionRangeSlider({
      hide_min_max: true,
      grid: true,
      grid_num: 1,
      skin:"round",
      min: 9.5,
      max: 54.5,
      from: 25
     
  });








  }
}
