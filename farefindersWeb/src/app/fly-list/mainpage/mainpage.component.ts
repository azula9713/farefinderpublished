import { Component, OnInit, ViewChild } from "@angular/core";
import { BsDatepickerConfig } from "ngx-bootstrap/datepicker";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormArray, FormGroup, Validators } from "@angular/forms";
import { HomeService } from "src/app/home/service/home.service";
import { MainServicesService } from "src/app/MainService/main-services.service";

import { NgxSpinnerService } from "ngx-spinner";
import { JqueryValidationManagerService } from "../../validations/jquery-validation-manager.service";

declare var $: any;
declare var jQuery: any;

@Component({
  selector: "app-mainpage",
  templateUrl: "./mainpage.component.html",
  styleUrls: ["./mainpage.component.css"],
})
export class MainpageComponent implements OnInit {
  ver_css_id1; // best flight search compoent id for active or not
  ver_css_id1_data: any;
  ver_css_id2;
  ver_css_id2_data: any;
  ver_css_id3;
  ver_css_id3_data: any;

  initialResult: any = []; // for saving spliced direct checkbox
  initialOneStop: any = []; // for saving spliced one stop checkbox
  initialTwoStop: any = []; // for saving spliced two stop checkbox
  beforeSilderChangeData: any = []; // outbound departure time before slider data
  beforeSliderReturnData: any = []; // return departure time before slider data
  beforeSliderJurneyData: any = []; // jurney duration slider before data
  initialAFResult: any = []; // for AirFrance checkbox change
  initialAIResult: any = []; // for Air India checkbox change
  initialBAResult: any = []; // for British Away checkbox change
  initialOthersResult: any = []; // for other Air lines check box change
  initialSelectedOption: any = 0; // dropdown filter change inital data

  mymodel: any;

  directSearchId: any;
  inDirectSearchId: any;

  disable;
  style_M;
  style_R;
  isdeleteAccept;
  Form: FormGroup;
  MultipleArray: FormGroup;
  m_City: FormArray;
  BsDatepickerConfig: Partial<BsDatepickerConfig>;

  selectCabin: string = "Economy";
  adultsCount: number = 1;
  childCount: number = 0;
  travelCount: number = 1;

  travellers: string = "adult";
  adltsminactive: boolean = false;
  adltsplusactive: boolean = true;
  childList: any = [];
  btnadlMinDisable: boolean = true;
  btnadlPlusDisable: boolean = false;
  btnChildMinDisable: boolean = true;
  btnchildPlusDisable: boolean = false;
  isCovid: boolean =true;

  body: any;
  params: any;
  AvilableFlights: any[] = [];
  isDataLoaded: boolean = false;
  states: any[] = [];
  childrenV2: FormArray;
  resultCount: number = 0;
  isRecordFound: boolean = false;
  isRecordRow: boolean = false;
  curtime = 1;

  tickets = [];
  allTickets = [];
  airLines = [];

  airLineCodeTemp;
  airLineCodeArrayTemp = [];
  isOneAirLineTemp = false;

  show = 10;

  isWeb: boolean = false;
  isMobile: boolean = false;

  isActiveTicketsPage: boolean = true;
  isActiveTicketAgenPriceCheckForm: boolean = false;

  selectedTicketObject = {
    //inilize the object

    Id: 0,
    IsDirect: true,
    ProposalId: 0,
    Segment: [],
    Agents: [],
  };

  cabinclassNusersInfo = {
    //inilize the object
    EndPoint: "",
    adultsCount: "",
    childrenCount: "",
    cabinClass: "",
    searchType: "",
    detail: [],
  };

  currency_rates: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private HomeService: HomeService,
    private ActivatedRoute: ActivatedRoute,
    private mainServicesService: MainServicesService,
    private spinner: NgxSpinnerService,
    private jqm: JqueryValidationManagerService
  ) {
    this.switchScrenn();

    this.BsDatepickerConfig = Object.assign(
      {},
      {
        isAnimated: true,
        containerClass: "theme-dark-blue",
        dateInputFormat: "YYYY-MM-DD",
      }
    );
  }

  ngOnInit(): void {
    this.spinner.show();

    this.initialVariableSetup();
    this.loadForm();
    this.addToCropArray();
    this.getAllAirLines();
    this.loadHeader();
    this.jqHandler();
  }

  ngAfterViewInit() {
    // this.jqm.maltiRangepicker();
    this.jquerySliderInit();
    $(function () {
      var current_progress = 0;
      var interval = setInterval(function () {
        current_progress += 2;
        $("#dynamic")
          .css("width", current_progress + "%")
          .attr("aria-valuenow", current_progress);
        if (current_progress >= 100) clearInterval(interval);
      }, 1000);
    });
  }

  async delay(ms: number) {
    await new Promise((resolve) => setTimeout(() => resolve(), ms)).then(() =>
      console.log("fired")
    );
  }

  jquerySliderInit() {
    var mainpageSender = this;

    let sliderId_return = "__departureTimes-return-slider";
    let sliderId_outbound = "__departureTimes-outbound-slider";

    let loweEndId = "pH-lower-end";
    let upperEndId = "pH-upper-end";

    $("#" + sliderId_return).ionRangeSlider({
      hide_min_max: true,
      skin: "round",
      grid: true,
      grid_num: 1,
      onStart: function (data) {
        $("#" + loweEndId).val(data.from);
        $("#" + upperEndId).val(data.to);
      },
      onChange: function (data) {
        $("#" + loweEndId).val(data.from);
        $("#" + upperEndId).val(data.to);
      },
      onFinish: function (data) {
        console.log(
          "slider change detected",
          Number(data.from),
          Number(data.to)
        );

        // add before slider changed data
        for (let j = 0; j < mainpageSender.beforeSliderReturnData.length; j++) {
          mainpageSender.tickets.push(mainpageSender.beforeSliderReturnData[j]);
        }

        mainpageSender.beforeSliderReturnData = [];
        for (let i = mainpageSender.tickets.length - 1; i >= 0; i--) {
          if (
            Number(
              mainpageSender.tickets[i].Segment[1].departureTime.replace(
                ":",
                "."
              )
            ) < Number(data.from) ||
            Number(
              mainpageSender.tickets[i].Segment[1].departureTime.replace(
                ":",
                "."
              )
            ) > Number(data.to)
          ) {
            // save beforeSliderReturnData before change.
            mainpageSender.beforeSliderReturnData.push(
              mainpageSender.tickets[i]
            );
            // remove the mismatched data
            mainpageSender.tickets.splice(i, 1);
          }
        }

        mainpageSender.tickets = mainpageSender.tickets;
        mainpageSender.resultCount = mainpageSender.tickets.length;

        mainpageSender.filterDropdownChange(
          mainpageSender.initialSelectedOption
        );
        mainpageSender.resetRecommendedResults();

        console.log(
          "Slider return departuretime Filter",
          mainpageSender.tickets
        );
      },
    });

    $("#" + sliderId_outbound).ionRangeSlider({
      hide_min_max: true,
      skin: "round",
      grid: true,
      grid_num: 1,
      onStart: function (data) {
        $("#" + loweEndId).val(data.from);
        $("#" + upperEndId).val(data.to);
      },
      onChange: function (data) {
        $("#" + loweEndId).val(data.from);
        $("#" + upperEndId).val(data.to);
      },
      onFinish: function (data) {
        console.log(
          "slider change detected",
          Number(data.from),
          Number(data.to)
        );

        // add before slider changed data
        for (let j = 0; j < mainpageSender.beforeSilderChangeData.length; j++) {
          mainpageSender.tickets.push(mainpageSender.beforeSilderChangeData[j]);
        }

        mainpageSender.beforeSilderChangeData = [];
        for (let i = mainpageSender.tickets.length - 1; i >= 0; i--) {
          if (
            Number(
              mainpageSender.tickets[i].Segment[0].departureTime.replace(
                ":",
                "."
              )
            ) < Number(data.from) ||
            Number(
              mainpageSender.tickets[i].Segment[0].departureTime.replace(
                ":",
                "."
              )
            ) > Number(data.to)
          ) {
            // save beforeSilderChangeData before change.
            mainpageSender.beforeSilderChangeData.push(
              mainpageSender.tickets[i]
            );
            // remove the mismatched data
            mainpageSender.tickets.splice(i, 1);
          }
        }

        mainpageSender.tickets = mainpageSender.tickets;
        mainpageSender.resultCount = mainpageSender.tickets.length;

        mainpageSender.filterDropdownChange(
          mainpageSender.initialSelectedOption
        );
        mainpageSender.resetRecommendedResults();

        console.log(
          "Slider outbounded departuretime Filter",
          mainpageSender.tickets
        );
      },
    });

    $("#__jurneyDuration-slider").ionRangeSlider({
      hide_min_max: true,
      grid: true,
      grid_num: 1,
      skin: "round",
      min: 0,
      max: 100,
      from: 100,
      onFinish: function (data) {
        console.log("Jurney duration change", data.from);

        // add before slider changed data
        for (let j = 0; j < mainpageSender.beforeSliderJurneyData.length; j++) {
          mainpageSender.tickets.push(mainpageSender.beforeSliderJurneyData[j]);
        }

        mainpageSender.beforeSliderJurneyData = [];
        for (let i = mainpageSender.tickets.length - 1; i >= 0; i--) {
          if (
            Number(
              mainpageSender.tickets[i].Segment[0].duration.replace("h", "")
            ) +
              Number(
                mainpageSender.tickets[i].Segment[1].duration.replace("h", "")
              ) >
            Number(data.from)
          ) {
            // save beforeSliderJurneyData before change.
            mainpageSender.beforeSliderJurneyData.push(
              mainpageSender.tickets[i]
            );
            // remove the mismatched data
            mainpageSender.tickets.splice(i, 1);
          }
        }

        mainpageSender.tickets = mainpageSender.tickets;
        mainpageSender.resultCount = mainpageSender.tickets.length;

        mainpageSender.filterDropdownChange(
          mainpageSender.initialSelectedOption
        );
        mainpageSender.resetRecommendedResults();

        console.log(
          "Slider outbounded departuretime Filter",
          mainpageSender.tickets
        );
      },
    });
  }

  //-----------------------------------------------------------------------

  initialVariableSetup() {
    this.ver_css_id1 = "active";
    this.ver_css_id2 = "inactive";
    this.ver_css_id3 = "inactive";

    this.disable = false;
    this.style_M = "display: none;";
    this.style_R = "";
    this.isdeleteAccept = true;
  }

  switchScrenn() {
    if (window.screen.width <= 600) {
      //mobile
      this.isWeb = false;
      this.isMobile = true;
    } else {
      //web
      this.isMobile = false;
      this.isWeb = true;
    }
  }

  //-----------------------------------------------------------------------

  loadForm() {
    this.Form = this.fb.group({
      c_Return: [true],
      c_OneWay: [false],
      c_Multiple: [false],
      ro_From: [""],
      ro_FromCode: [""],
      ro_To: [""],
      ro_ToCode: [""],
      ro_Depart: [""],
      ro_Return: [""],
      ro_Cabin: [""],
      ro_F_addN: [false],
      ro_T_addN: [false],
      ro_Direct: [false],
      m_City: this.fb.array([this.loadMultipleArray()]),
      m_Adlt: [""],
      cabinClass: ["Economy", Validators.required],
      travellers: [1, Validators.required],
      adults: [1, Validators.required],
      children: [0, Validators.required],
      childrenV2: this.fb.array([]),

      FormFilterations: this.fb.group({
        stops: this.fb.group({
          direct: true,
          oneStop: true,
          morethanOneStop: true,
        }),
      }),
    });
  }

  loadMultipleArray() {
    this.MultipleArray = this.fb.group({
      m_From: [""],
      m_FromCode: [""],
      m_To: [""],
      m_ToCode: [""],
      m_date: [""],
    });
    return this.MultipleArray;
  }

  loadChildren(): FormGroup {
    return this.fb.group({
      Age: [-1],
    });
  }

  //-----------------------------------------------------------------------

  addChildren() {
    this.formChildrenAge.push(this.loadChildren());
  }

  removeChildren(index: number) {
    if (this.formChildrenAge.length >= 0) {
      this.formChildrenAge.removeAt(index);
    }
  }

  addToCropArray() {
    this.formMultiCity.push(this.loadMultipleArray());
  }

  addFlight() {
    if (this.formMultiCity.length <= 4) {
      this.isdeleteAccept = false;
      this.addToCropArray();
    }
  }

  removeFlight(index: number) {
    if (this.formMultiCity.length > 2) {
      this.formMultiCity.removeAt(index);
    }
    if (this.formMultiCity.length <= 2) {
      this.isdeleteAccept = true;
    } else {
      this.isdeleteAccept = false;
    }
  }

  updateMultiCityFlightArrayFlightCode(fromFlight, toFlight, index) {
    this.formMultiCity.at(index).patchValue({
      m_FromCode: fromFlight.split(/[()]/)[1],
      m_ToCode: toFlight.split(/[()]/)[1],
    });
  }

  updateMultiCityFlightArrayDates(date, index) {
    this.formMultiCity.at(index).patchValue({
      m_date: date,
    });
  }

  AgentBooking_FlightTypeInfo() {
    return {
      DisplayFlightTypeName: "",
      date: "",
    };
  }

  //-----------------------------------------------------------------------

  get formMultiCity() {
    return this.Form.get("m_City") as FormArray;
  }

  get formChildrenAge() {
    return this.Form.get("childrenV2") as FormArray;
  }

  get filterations() {
    return this.Form.get("FormFilterations") as FormArray;
  }

  //-----------------------------------------------------------------------

  onApplyPickerMultiWay(id, index) {
    $("#" + id).datepicker({
      dateFormat: "yy-mm-dd",
      minDate: new Date(),
      onClose: (dateText, inst) => {
        this.updateMultiCityFlightArrayDates(dateText, index);
      },
    });
  }

  onSearch() {}

  onTest() {}

  onCloseCabinTravellersSelectDropDown() {}

  onChangeFilterCheckBoxStops(event) {
    console.log("Stop Filter called", event.target.name, event.target.checked);
    // save first whole datum
    console.log(this.initialResult.length);
    if (event.target.name == "direct") {
      if (event.target.checked == true) {
        console.log("direct called");
        for (let j = 0; j < this.initialResult.length; j++) {
          if (this.initialResult[j].IsDirect == true) {
            this.tickets.push(this.initialResult[j]);
          }
        }
      } else {
        console.log("direct rejected");
        this.initialResult = [];
        for (let i = this.tickets.length - 1; i >= 0; i--) {
          if (this.tickets[i].IsDirect == true) {
            this.initialResult.push(this.tickets[i]);
            this.tickets.splice(i, 1);
          }
        }
      }

      this.tickets = this.tickets;
      this.resultCount = this.tickets.length;
      // console.log('Direct filter Result:', this.tickets);
    }

    if (event.target.name == "onestop") {
      if (event.target.checked == true) {
        console.log("one stop added");
        for (let j = 0; j < this.initialOneStop.length; j++) {
          if (
            this.initialOneStop[j].IsDirect == false &&
            this.initialOneStop[j].Segment[0].transfer.indexOf("1") > -1 &&
            this.initialOneStop[j].Segment[1].transfer.indexOf("1") > -1
          ) {
            this.tickets.push(this.initialOneStop[j]);
          }
        }
      } else {
        this.initialOneStop = [];
        console.log("one stop rejected");
        for (let i = this.tickets.length - 1; i >= 0; i--) {
          if (
            this.tickets[i].IsDirect == false &&
            this.tickets[i].Segment[0].transfer.indexOf("1") > -1 &&
            this.tickets[i].Segment[1].transfer.indexOf("1") > -1
          ) {
            this.initialOneStop.push(this.tickets[i]);
            this.tickets.splice(i, 1);
          }
        }
      }

      this.tickets = this.tickets;
      this.resultCount = this.tickets.length;
      // console.log('One stop filter Result:', this.tickets);
    }

    if (event.target.name == "twostop") {
      if (event.target.checked == true) {
        console.log("two stop added");
        for (let j = 0; j < this.initialTwoStop.length; j++) {
          if (
            (this.initialTwoStop[j].IsDirect == false &&
              this.initialTwoStop[j].Segment[0].transfer.indexOf("1") === -1 &&
              this.initialTwoStop[j].Segment[1].transfer.indexOf("1") === -1) ||
            (this.initialTwoStop[j].IsDirect == false &&
              this.initialTwoStop[j].Segment[0].transfer.indexOf("1") > -1 &&
              this.initialTwoStop[j].Segment[1].transfer.indexOf("1") === -1) ||
            (this.initialTwoStop[j].IsDirect == false &&
              this.initialTwoStop[j].Segment[0].transfer.indexOf("1") === -1 &&
              this.initialTwoStop[j].Segment[1].transfer.indexOf("1") > -1)
          ) {
            this.tickets.push(this.initialTwoStop[j]);
          }
        }
      } else {
        console.log("two stop rejected");
        this.initialTwoStop = [];
        for (let i = this.tickets.length - 1; i >= 0; i--) {
          if (
            (this.tickets[i].Segment[0].transfer.indexOf("1") === -1 &&
              this.tickets[i].Segment[1].transfer.indexOf("1") === -1 &&
              this.tickets[i].IsDirect == false) ||
            (this.tickets[i].Segment[0].transfer.indexOf("1") > -1 &&
              this.tickets[i].Segment[1].transfer.indexOf("1") === -1 &&
              this.tickets[i].IsDirect == false) ||
            (this.tickets[i].Segment[0].transfer.indexOf("1") === -1 &&
              this.tickets[i].Segment[1].transfer.indexOf("1") > -1 &&
              this.tickets[i].IsDirect == false)
          ) {
            // console.log(this.tickets[i]);
            this.initialTwoStop.push(this.tickets[i]);
            this.tickets.splice(i, 1);
          }
        }
      }

      this.tickets = this.tickets;
      this.resultCount = this.tickets.length;
      // console.log('Two stop filter Result:', this.tickets);
    }

    this.filterDropdownChange(this.initialSelectedOption);
    this.resetRecommendedResults();
  }

  //-----------------------------------------------------------------------

  getFlightsResultSet() {
    this.isDataLoaded = true;

    this.ActivatedRoute.queryParams.subscribe((params) => {
      this.body = [
        {
          from: params.from,
          to: params.to,
          depart: params.depart,
          return: params.return,
          travellers: params.travellers,
          adults: params.adults,
          children: params.children,
          childrenV2: params.childrenV2,
          cabinClass: params.cabinClass,
        },
      ];
    });

    this.HomeService.getAvilableTickets(this.body[0] as FormGroup).subscribe(
      (resultSet) => {
        this.AvilableFlights = resultSet;
        this.resultCount = this.AvilableFlights.length;
        this.isDataLoaded = false;

        if (this.AvilableFlights.length == 0) {
          this.isRecordFound = true;
          this.isRecordRow = true;
        }
      }
    );
  }

  getAllFlights() {
    return this.HomeService.getFlights();
  }

  async loadHeader() {
    this.ActivatedRoute.queryParams.subscribe((body) => {
      this.body = body;
    });

    this.ActivatedRoute.params.subscribe((params) => {
      this.params = params;
    });

    if (+this.params.flightType === 0) {
      //one way

      this.states = await this.getAllFlights();
      this.Form.controls["ro_From"].setValue(
        this.states.filter((x) => x.flightCode === this.params.fromFlight)[0][
          "searchName"
        ]
      );
      this.Form.controls["ro_To"].setValue(
        this.states.filter((x) => x.flightCode === this.params.toFlight)[0][
          "searchName"
        ]
      );

      this.Form.controls["ro_FromCode"].setValue(this.params.fromFlight);
      this.Form.controls["ro_ToCode"].setValue(this.params.toFlight);
      this.Form.controls["ro_Depart"].setValue(
        this.params.fromFlightDate.substring(0, 4) +
          "-" +
          this.params.fromFlightDate.substring(4, 6) +
          "-" +
          this.params.fromFlightDate.substring(6, 8)
      );

      (<HTMLInputElement>(
        document.getElementById("customRadio1")
      )).checked = false;
      (<HTMLInputElement>(
        document.getElementById("customRadio2")
      )).checked = true;
      (<HTMLInputElement>(
        document.getElementById("customRadio3")
      )).checked = false;
      this.OnewayClick();

      if (Boolean(JSON.parse(this.body.preferdirects)) === true)
        this.Form.controls["ro_Direct"].setValue(1);
      else this.Form.controls["ro_Direct"].setValue(0);

      if (Boolean(JSON.parse(this.body.outboundaltsenabled)) === true)
        this.Form.controls["ro_T_addN"].setValue(1);
      else this.Form.controls["ro_T_addN"].setValue(0);

      if (Boolean(JSON.parse(this.body.inboundaltsenabled)) === true)
        this.Form.controls["ro_F_addN"].setValue(1);
      else this.Form.controls["ro_F_addN"].setValue(0);

      this.cabinclassNusersInfo.EndPoint = this.Form.controls["ro_To"].value;
      this.cabinclassNusersInfo.searchType = "one way";

      let tempObj = {
        DisplayFlightTypeName: "Outbound",
        date: this.Form.controls["ro_Depart"].value,
      };

      this.cabinclassNusersInfo.detail.push(tempObj);
    } else if (+this.params.flightType === 1) {
      //return way

      this.states = await this.getAllFlights();
      this.Form.controls["ro_From"].setValue(
        this.states.filter((x) => x.flightCode === this.params.fromFlight)[0][
          "searchName"
        ]
      );
      this.Form.controls["ro_To"].setValue(
        this.states.filter((x) => x.flightCode === this.params.toFlight)[0][
          "searchName"
        ]
      );

      this.Form.controls["ro_FromCode"].setValue(this.params.fromFlight);
      this.Form.controls["ro_ToCode"].setValue(this.params.toFlight);
      this.Form.controls["ro_Depart"].setValue(
        this.params.fromFlightDate.substring(0, 4) +
          "-" +
          this.params.fromFlightDate.substring(4, 6) +
          "-" +
          this.params.fromFlightDate.substring(6, 8)
      );
      this.Form.controls["ro_Return"].setValue(
        this.params.toFlightDate.substring(0, 4) +
          "-" +
          this.params.toFlightDate.substring(4, 6) +
          "-" +
          this.params.toFlightDate.substring(6, 8)
      );
      this.Form.controls["cabinClass"].setValue(this.body.cabinClass);

      (<HTMLInputElement>(
        document.getElementById("customRadio1")
      )).checked = true;
      (<HTMLInputElement>(
        document.getElementById("customRadio2")
      )).checked = false;
      (<HTMLInputElement>(
        document.getElementById("customRadio3")
      )).checked = false;
      this.ReturnClick();

      if (Boolean(JSON.parse(this.body.preferdirects)) === true)
        this.Form.controls["ro_Direct"].setValue(1);
      else this.Form.controls["ro_Direct"].setValue(0);

      if (Boolean(JSON.parse(this.body.outboundaltsenabled)) === true)
        this.Form.controls["ro_T_addN"].setValue(1);
      else this.Form.controls["ro_T_addN"].setValue(0);

      if (Boolean(JSON.parse(this.body.inboundaltsenabled)) === true)
        this.Form.controls["ro_F_addN"].setValue(1);
      else this.Form.controls["ro_F_addN"].setValue(0);

      this.cabinclassNusersInfo.EndPoint = this.Form.controls["ro_To"].value;
      this.cabinclassNusersInfo.searchType = "return";

      this.cabinclassNusersInfo.detail.push(
        {
          DisplayFlightTypeName: "Outbound",
          date: this.Form.controls["ro_Depart"].value,
        },
        {
          DisplayFlightTypeName: "Return",
          date: this.Form.controls["ro_Return"].value,
        }
      );
    } else if (+this.params.flightType === 2) {
      //multi way

      this.states = await this.getAllFlights();
      var paramsCount = 0;

      if (this.params.fromFlight5 != undefined) paramsCount = 5;
      else if (this.params.fromFlight4 != undefined) paramsCount = 4;
      else if (this.params.fromFlight3 != undefined) paramsCount = 3;
      else paramsCount = 2;

      for (let i = 0; i < paramsCount; i++) {
        this.loadMultiCityRow(this.getMulitiCityRow(i), i);
      }

      (<HTMLInputElement>(
        document.getElementById("customRadio1")
      )).checked = false;
      (<HTMLInputElement>(
        document.getElementById("customRadio2")
      )).checked = false;
      (<HTMLInputElement>(
        document.getElementById("customRadio3")
      )).checked = true;
      this.MultiCityClick();

      this.cabinclassNusersInfo.EndPoint = this.formMultiCity.value[
        this.formMultiCity.length - 1
      ].m_To;
      this.cabinclassNusersInfo.searchType = "multi way";
    }
    this.updateCabinclassDropDown(
      this.body.cabinClass,
      this.body.travellers,
      this.body.adults,
      this.body.children,
      this.body.childrenV2
    );

    this.cabinclassNusersInfo.adultsCount = this.body.adults;
    this.cabinclassNusersInfo.childrenCount = this.body.children;
    this.cabinclassNusersInfo.cabinClass = this.body.cabinClass;

    this.getFlightOffers();
  }

  loadMultiCityRow(obj, index) {
    if (index > 1) this.addFlight();

    this.formMultiCity.at(index).patchValue({
      m_From: obj.m_From,
      m_FromCode: obj.m_FromCode,
      m_To: obj.m_To,
      m_ToCode: obj.m_ToCode,
      m_date: obj.m_date,
    });

    let tempObj = {
      DisplayFlightTypeName: "Flight" + index + 1,
      date: obj.m_date,
    };

    this.cabinclassNusersInfo.detail.push(tempObj);
  }

  getMulitiCityRow(index: number) {
    let multiCityObj;

    if (index === 0) {
      multiCityObj = {
        m_From: this.states.filter(
          (x) => x.flightCode === this.params.fromFlight1
        )[0]["searchName"],
        m_To: this.states.filter(
          (x) => x.flightCode === this.params.toFlight1
        )[0]["searchName"],
        m_FromCode: this.params.fromFlight1,
        m_ToCode: this.params.toFlight1,
        m_date:
          this.params.fromFlightDate1.substring(0, 4) +
          "-" +
          this.params.fromFlightDate1.substring(4, 6) +
          "-" +
          this.params.fromFlightDate1.substring(6, 8),
      };
    } else if (index === 1) {
      multiCityObj = {
        m_From: this.states.filter(
          (x) => x.flightCode === this.params.fromFlight2
        )[0]["searchName"],
        m_To: this.states.filter(
          (x) => x.flightCode === this.params.toFlight2
        )[0]["searchName"],
        m_FromCode: this.params.fromFlight2,
        m_ToCode: this.params.toFlight2,
        m_date:
          this.params.fromFlightDate2.substring(0, 4) +
          "-" +
          this.params.fromFlightDate2.substring(4, 6) +
          "-" +
          this.params.fromFlightDate2.substring(6, 8),
      };
    } else if (index === 2) {
      multiCityObj = {
        m_From: this.states.filter(
          (x) => x.flightCode === this.params.fromFlight3
        )[0]["searchName"],
        m_To: this.states.filter(
          (x) => x.flightCode === this.params.toFlight3
        )[0]["searchName"],
        m_FromCode: this.params.fromFlight3,
        m_ToCode: this.params.toFlight3,
        m_date:
          this.params.fromFlightDate3.substring(0, 4) +
          "-" +
          this.params.fromFlightDate3.substring(4, 6) +
          "-" +
          this.params.fromFlightDate3.substring(6, 8),
      };
    } else if (index === 3) {
      multiCityObj = {
        m_From: this.states.filter(
          (x) => x.flightCode === this.params.fromFlight4
        )[0]["searchName"],
        m_To: this.states.filter(
          (x) => x.flightCode === this.params.toFlight4
        )[0]["searchName"],
        m_FromCode: this.params.fromFlight4,
        m_ToCode: this.params.toFlight4,
        m_date:
          this.params.fromFlightDate4.substring(0, 4) +
          "-" +
          this.params.fromFlightDate4.substring(4, 6) +
          "-" +
          this.params.fromFlightDate4.substring(6, 8),
      };
    } else if (index === 4) {
      multiCityObj = {
        m_From: this.states.filter(
          (x) => x.flightCode === this.params.fromFlight5
        )[0]["searchName"],
        m_To: this.states.filter(
          (x) => x.flightCode === this.params.toFlight5
        )[0]["searchName"],
        m_FromCode: this.params.fromFlight5,
        m_ToCode: this.params.toFlight5,
        m_date:
          this.params.fromFlightDate5.substring(0, 4) +
          "-" +
          this.params.fromFlightDate5.substring(4, 6) +
          "-" +
          this.params.fromFlightDate5.substring(6, 8),
      };
    }

    return multiCityObj;
  }

  getFlightOffers() {
    this.getSearchId();
  }

  async getDirectTickets(searchId: string) {
    this.tickets = await this.mainServicesService.getDirectTickets(
      searchId,
      this.getSearchFlightType(),
      true
    );

    this.resultCount = this.tickets.length;
    this.isRecordRow = true;

    console.log("Direct search was finished", this.tickets);

    await this.getIndrectFlights().then(
      (res) => {
        console.log("Indirect search was finished", res);
        if (this.tickets.length === 0) {
          this.isRecordFound = true;
          this.isRecordRow = true;
        } else {
          this.isRecordRow = true;
        }
      },
      (err) => {
        console.log("Failed", err);
      }
    );

    // show best, fastest and cheapest result
    await this.InitSortFunction(1, 0).then((res) => {
      // console.log('Best Flight', res)
      this.ver_css_id1_data = res;
    });

    await this.InitSortFunction(3, 0).then((res) => {
      // console.log('Fastest Flight', res)
      this.ver_css_id2_data = res;
    });

    await this.InitSortFunction(2, 0).then((res) => {
      // console.log('Cheapest Flight', res)
      this.ver_css_id3_data = res;
    });

    this.spinner.hide();
  }

  getIndrectFlights() {
    console.log("Indirect Search is called Now");
    return new Promise((resolve, reject) => {
      this.mainServicesService
        .getSearchId1(this.Form.getRawValue())
        .subscribe(async (data: any) => {
          this.inDirectSearchId = data.search_id;
          await this.getInDirectTickets(data.search_id);
          resolve("Indirect Searh was Finished");
          reject("Time was Passed");
        });
    });
  }

  getInDirectTickets(searchId: string) {
    return new Promise((resolve, reject) => {
      this.mainServicesService
        .getDirectTickets1(searchId, this.getSearchFlightType(), false)
        .subscribe((res) => {
          // debugger;
          res.forEach((element) => {
            this.tickets.push(element);
            // this.initialResult.push(element);
          });
          this.tickets = this.tickets;
          this.resultCount = this.tickets.length;
          console.log("Indirect Search Result", this.tickets);

          resolve();
        });
    });
  }

  async getSearchId() {
    // console.log('Search ID Get Raw Value:', this.Form.getRawValue());

    let data = await this.mainServicesService.getSearchId(
      this.Form.getRawValue()
    );
    console.log("SEARCH ID RESULT DATA:", data);
    this.directSearchId = data.search_id;
    // console.log('DDDDDDDDDDDDDDDDDDDDDDD', this.directSearchId);
    this.currency_rates = data.currency_rates;
    this.getDirectTickets(data.search_id);
  }

  async getAllAirLines() {
    this.airLines = await this.mainServicesService.getAllAirLines();
  }

  //-----------------------------------------------------------------------

  openNav() {
    document.getElementById("mySidenav").style.width = "100%";
  }

  closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

  //-----------------------------------------------------------------------

  jqHandler() {
    var that = this;

    $(function () {
      $("#datePickerDeparture").datepicker({
        dateFormat: "yy-mm-dd",
        minDate: new Date(),
        onClose: (dateText, inst) => {
          that.Form.controls["ro_Depart"].setValue(dateText);
        },
      });

      $("#datePickerReturn").datepicker({
        dateFormat: "yy-mm-dd",
        minDate: new Date(),
        onClose: (dateText, inst) => {
          that.Form.controls["ro_Return"].setValue(dateText);
        },
      });

      $("#datePickerDepartureMobile").datepicker({
        dateFormat: "yy-mm-dd",
        minDate: new Date(),
        onClose: (dateText, inst) => {
          that.Form.controls["ro_Depart"].setValue(dateText);
        },
      });

      $("#datePickerReturnMobile").datepicker({
        dateFormat: "yy-mm-dd",
        minDate: new Date(),
        onClose: (dateText, inst) => {
          that.Form.controls["ro_Return"].setValue(dateText);
        },
      });

      $(".dropdown-menu").on("click", function (event) {
        event.stopPropagation();
      });
    });
  }

  runProgressBar() {
    var id;
    var resolution = window.screen.width;
    if (resolution <= 600) id = "ProgressBarMobile";
    else id = "ProgressBarWeb";

    $(function () {
      var current_progress = 0;
      var interval = setInterval(function () {
        current_progress += 20;
        $("#" + id)
          //  .css("width", current_progress + "%")
          //  .attr("aria-valuenow", current_progress)
          //  .text(current_progress + "% Complete");
          .css("width", current_progress + "%")
          .attr("aria-valuenow", current_progress);
        if (current_progress >= 100) clearInterval(interval);
      }, 1000);
    });
  }

  //-----------------------------------------------------------------------

  ReturnClick(): void {
    this.Form.controls["ro_Return"].enable();
    this.disable = false;
    this.style_M = "display: none;";
    this.style_R = "";

    this.Form.controls["c_Return"].setValue(true);
    this.Form.controls["c_OneWay"].setValue(false);
    this.Form.controls["c_Multiple"].setValue(false);
  }
  OnewayClick(): void {
    this.Form.controls["ro_Return"].disable();
    this.disable = true;
    this.style_M = "display: none;";
    this.style_R = "";

    this.Form.controls["c_Return"].setValue(false);
    this.Form.controls["c_OneWay"].setValue(true);
    this.Form.controls["c_Multiple"].setValue(false);
  }
  MultiCityClick(): void {
    this.Form.controls["ro_Return"].enable();
    this.disable = false;
    this.style_M = "";
    this.style_R = "display: none;";

    this.Form.controls["c_Return"].setValue(false);
    this.Form.controls["c_OneWay"].setValue(false);
    this.Form.controls["c_Multiple"].setValue(true);
  }

  //-----------------------------------------------------------------------

  selectOption(value) {
    this.Form.controls["cabinClass"].setValue(value);
    this.selectCabin = value;
  }
  checkTraveller() {
    this.travellers = this.travelCount > 1 ? "travellers" : "adult";

    this.Form.controls["travellers"].setValue(this.travelCount);
    this.Form.controls["adults"].setValue(this.adultsCount);
    this.Form.controls["children"].setValue(this.childCount);
  }
  adultsMin() {
    if (this.adultsCount > 2) {
      this.adultsCount -= 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.btnadlMinDisable = false;
      this.btnadlPlusDisable = false;
    } else {
      this.adultsCount -= 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.btnadlMinDisable = true;
    }
    this.checkTraveller();
  }
  adultsPlus() {
    if (this.adultsCount < 7) {
      this.adultsCount += 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.btnadlMinDisable = false;
    } else {
      this.adultsCount += 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.btnadlPlusDisable = true;
    }
    this.checkTraveller();
  }
  childsMin() {
    if (this.childCount > 1) {
      this.childCount -= 1;
      this.travelCount = this.adultsCount + this.childCount;
      // this.childList.pop(this.childCount);
      this.btnChildMinDisable = false;
      this.btnchildPlusDisable = false;
      this.removeChildren(0);
    } else {
      this.childCount -= 1;
      this.travelCount = this.adultsCount + this.childCount;
      // this.childList.pop(this.childCount);
      this.btnChildMinDisable = true;
      this.removeChildren(0);
    }
    this.checkTraveller();
  }
  childsPlus() {
    if (this.childCount < 7) {
      this.childCount += 1;
      this.travelCount = this.adultsCount + this.childCount;
      // this.childList.push(this.childCount);
      this.btnChildMinDisable = false;

      this.addChildren();
    } else {
      this.childCount += 1;
      this.travelCount = this.adultsCount + this.childCount;
      // this.childList.push(this.childCount);
      this.btnchildPlusDisable = true;
      this.addChildren();
    }

    this.checkTraveller();
  }

  updateCabinclassDropDown(
    cabinClass,
    travelCount,
    adultsCount,
    childCount,
    childAgeArray
  ) {
    var childArray = childAgeArray.split(",");

    this.travelCount = +travelCount;
    this.adultsCount = +adultsCount;
    this.childCount = +childCount;

    if (this.adultsCount < 8) {
      this.btnadlMinDisable = false;
      this.btnadlPlusDisable = false;
    } else if (this.adultsCount === 8) {
      this.btnadlMinDisable = false;
      this.btnadlPlusDisable = true;
    }

    if (this.childCount > 0 && this.childCount < 8) {
      this.btnChildMinDisable = false;
      this.btnchildPlusDisable = false;
    } else if (this.childCount === 8) {
      this.btnChildMinDisable = false;
      this.btnchildPlusDisable = true;
    } else if (this.childCount === 0) {
      this.btnChildMinDisable = true;
      this.btnchildPlusDisable = false;
    }

    for (let i = 0; i < this.childCount; i++) {
      this.addChildren();
      this.formChildrenAge.at(i).patchValue({
        Age: +childArray[i],
      });
    }

    this.checkTraveller();
    this.selectOption(cabinClass);
  }

  getLogo(Segment, type) {
    if (type === "Web") {
      if (Segment.Detail.length === 1) {
        return (
          "http://pics.avs.io/100/50/" + Segment.Detail[0].airLine + ".png"
        );
      } else {
        let airLineNames = "";
        let airLineName = "";
        Segment.Detail.forEach((element) => {
          airLineName = this.airLines.filter(
            (airLine) => airLine.code == element.airLine
          )[0].name;
          airLineNames = airLineNames + "  " + airLineName + ",";
        });

        return airLineNames;
      }
    } else if (type === "Mobile") {
      if (this.isOneAirLineTemp === true) {
        return "http://pics.avs.io/50/20/" + this.airLineCodeTemp + ".png";
      } else {
        let airLineNames = "";
        let airLineName = "";

        this.airLineCodeArrayTemp.forEach((airLineCode) => {
          airLineName = this.airLines.filter(
            (airLine) => airLine.code == airLineCode
          )[0].name;
          airLineNames = airLineNames + "  " + airLineName + ",";
        });

        return airLineNames;
      }
    }
  }

  checkPossibleToGetLogo(Segment, Type) {
    if (Type === "Web") {
      if (Segment.Detail.length === 1) return true;
      else return false;
    } else if (Type === "Mobile") {
      this.airLineCodeArrayTemp = [];
      this.isOneAirLineTemp = false;

      Segment.forEach((oneSegment) => {
        oneSegment.Detail.forEach((Detail) => {
          this.airLineCodeArrayTemp.push(Detail.airLine);
          this.airLineCodeTemp = Detail.airLine;
        });
      });

      this.isOneAirLineTemp = this.airLineCodeArrayTemp.every(
        (airline) => airline === this.airLineCodeTemp
      );
      return this.isOneAirLineTemp;
    }
  }

  GetLowestPrice(agents) {
    if (agents.length === 1) {
      let exRate =
        this.currency_rates[agents[0].agentTicketCurrency] /
        this.currency_rates.gbp;

      // return (agents[0].agentTicketCurrency).toUpperCase() + "  " + agents[0].agentTicketPrice;
      return "GBP  " + (agents[0].agentTicketPrice * exRate).toFixed(2);
    } else {
      // debugger;

      let tempAgent = [];
      let exRate = 0;

      agents.forEach((agent) => {
        tempAgent.push(agent.agentTicketPrice);
      });

      let lowestAgenCurrencyType;

      // agents.map(agent=>{
      //   if(agent.agentTicketPrice === Math.min(...tempAgent)){
      //     lowestAgenCurrencyType = agent.agentTicketCurrency;
      //   }

      // })

      // return lowestAgenCurrencyType + Math.min(...tempAgent);

      agents.map((agent) => {
        if (agent.agentTicketPrice === Math.min(...tempAgent)) {
          exRate =
            this.currency_rates[agent.agentTicketCurrency] /
            this.currency_rates.gbp;
          lowestAgenCurrencyType = "GBP ";
        }
      });

      return (
        lowestAgenCurrencyType + (Math.min(...tempAgent) * exRate).toFixed(2)
      );
    }
  }

  increaseShow() {
    this.show += 10;
  }

  onClickTicket(ticket) {
    this.selectedTicketObject = ticket;
    this.isActiveTicketsPage = false;
    this.isActiveTicketAgenPriceCheckForm = true;
  }

  
  backToResults() {
    this.isActiveTicketsPage = !this.isActiveTicketsPage;
    console.log(this.isActiveTicketsPage);
  }

  getTicketAgenPriceCheckFormStaus(event) {
    if (event === true) {
      console.log("Return back Called");
      this.isActiveTicketsPage = true;
      this.isActiveTicketAgenPriceCheckForm = false;
      this.resetTicketPopupOnjects();
      this.jquerySliderInit();
    }
  }

  resetTicketPopupOnjects() {
    this.selectedTicketObject = {
      //inilize the object

      Id: 0,
      IsDirect: true,
      ProposalId: 0,
      Segment: [],
      Agents: [],
    };
  }

  getSearchFlightType() {
    if (this.Form.controls["c_Return"].value === true) return "return";
    else if (this.Form.controls["c_OneWay"].value === true) return "oneWay";
    else if (this.Form.controls["c_Multiple"].value === true) return "multiWay";
    else return "";
  }

  // filter with dropdown select box
  async filterDropdownChange(selectedValue) {
    this.initialSelectedOption = selectedValue;
    var sender = this;
    this.spinner.show();
    setTimeout(async function () {
      await sender.SortFunction(selectedValue);
      sender.spinner.hide();
    }, 100);
  }

  // when do dropdwon filter
  SortFunction(selectedValue) {
    return new Promise((resolve, reject) => {
      // best filter
      if (selectedValue == 1) {
        let done = false;
        while (!done) {
          done = true;
          for (let i = 1; i < this.tickets.length; i += 1) {
            // get same gbp currency cheapest price
            let current = this.GetLowestPriceWithNumber(this.tickets[i].Agents);
            let before = this.GetLowestPriceWithNumber(
              this.tickets[i - 1].Agents
            );

            // get duration
            let currenth = this.tickets[i].Segment[0].duration;
            let beforeh = this.tickets[i - 1].Segment[0].duration;

            let currenthHourFormat = currenth.replace("h", "");
            let beforehHourFormat = beforeh.replace("h", "");

            // check 00.0 and replace with 00.00
            let currentDotIndex = currenthHourFormat.indexOf(".");
            let currentMinute = currenthHourFormat.substr(currentDotIndex + 1);

            if (currentMinute.length == 1) {
              currenthHourFormat.replace(".", ".0");
            }

            let beforeDotIndex = beforehHourFormat.indexOf(".");
            let beforeMinute = beforehHourFormat.substr(beforeDotIndex + 1);

            if (beforeMinute.length == 1) {
              beforehHourFormat.replace(".", ".0");
            }
            // end check

            if (
              Number(before) * 0.5 + Number(beforehHourFormat) * 0.5 >
              Number(current) * 0.5 + Number(currenthHourFormat) * 0.5
            ) {
              done = false;
              let tmp = this.tickets[i - 1];
              this.tickets[i - 1] = this.tickets[i];
              this.tickets[i] = tmp;
            }
          }
        }

        // console.log('Best Result')
        // console.log(this.tickets);

        // set the best result for ver_css_id1_data
        // this.ver_css_id1_data = this.tickets[0];
      }

      // cheapest filter
      if (selectedValue == 2) {
        let done = false;
        while (!done) {
          done = true;
          for (let i = 1; i < this.tickets.length; i += 1) {
            if (
              this.tickets[i].Agents != null &&
              this.tickets[i - 1].Agents != null
            ) {
              // get same gbp currency cheapest price
              let current = this.GetLowestPriceWithNumber(
                this.tickets[i].Agents
              );
              let before = this.GetLowestPriceWithNumber(
                this.tickets[i - 1].Agents
              );

              if (Number(before) > Number(current)) {
                done = false;
                let tmp = this.tickets[i - 1];
                this.tickets[i - 1] = this.tickets[i];
                this.tickets[i] = tmp;
              }
            }
          }
        }
        // console.log('Cheapest Result')
        // console.log(this.tickets);

        // set the best result for ver_css_id1_data
        // this.ver_css_id3_data = this.tickets[0];
      }

      // fastest filter
      if (selectedValue == 3) {
        // get duration
        let done = false;
        while (!done) {
          done = true;
          for (let i = 1; i < this.tickets.length; i += 1) {
            if (
              this.tickets[i].Agents != null &&
              this.tickets[i - 1].Agents != null
            ) {
              if (
                this.tickets[i].Segment[0] != null &&
                this.tickets[i - 1].Segment[0] != null
              ) {
                // get duration
                let current = this.tickets[i].Segment[0].duration;
                let before = this.tickets[i - 1].Segment[0].duration;

                let currentHourFormat = current.replace("h", "");
                let beforeHourFormat = before.replace("h", "");

                // check 00.0 and replace with 00.00
                let currentDotIndex = currentHourFormat.indexOf(".");
                let currentMinute = currentHourFormat.substr(
                  currentDotIndex + 1
                );

                if (currentMinute.length == 1) {
                  currentHourFormat.replace(".", ".0");
                }

                let beforeDotIndex = beforeHourFormat.indexOf(".");
                let beforeMinute = beforeHourFormat.substr(beforeDotIndex + 1);

                if (beforeMinute.length == 1) {
                  beforeHourFormat.replace(".", ".0");
                }
                // end check

                if (Number(beforeHourFormat) > Number(currentHourFormat)) {
                  done = false;
                  let tmp = this.tickets[i - 1];
                  this.tickets[i - 1] = this.tickets[i];
                  this.tickets[i] = tmp;
                }
              }
            }
          }
        }
        // console.log('Fastest Result')
        // console.log(this.tickets);

        // set the best result for ver_css_id1_data
        // this.ver_css_id2_data = this.tickets[0];
      }

      // outbounded:departure time filter
      if (selectedValue == 4) {
        // get departureTime
        let done = false;
        while (!done) {
          done = true;
          for (let i = 1; i < this.tickets.length; i += 1) {
            if (
              this.tickets[i].Agents != null &&
              this.tickets[i - 1].Agents != null
            ) {
              if (
                this.tickets[i].Segment[0] != null &&
                this.tickets[i - 1].Segment[0] != null
              ) {
                // get departureTime
                let current = this.tickets[i].Segment[0].departureTime;
                let before = this.tickets[i - 1].Segment[0].departureTime;

                let currentHourFormat = current.replace(":", "");
                let beforeHourFormat = before.replace(":", "");

                if (Number(beforeHourFormat) > Number(currentHourFormat)) {
                  done = false;
                  let tmp = this.tickets[i - 1];
                  this.tickets[i - 1] = this.tickets[i];
                  this.tickets[i] = tmp;
                }
              }
            }
          }
        }
        // console.log('Outbounded DepartureTime Result')
        // console.log(this.tickets);

        // set the best result for ver_css_id1_data
        // this.ver_css_id1_data = this.tickets[0];
      }

      // return:departure time filter
      if (selectedValue == 5) {
        // get departureTime
        let done = false;
        while (!done) {
          done = true;
          for (let i = 1; i < this.tickets.length; i += 1) {
            if (
              this.tickets[i].Agents != null &&
              this.tickets[i - 1].Agents != null
            ) {
              if (
                this.tickets[i].Segment[0] != null &&
                this.tickets[i - 1].Segment[0] != null
              ) {
                // get departureTime
                let current = this.tickets[i].Segment[1].departureTime;
                let before = this.tickets[i - 1].Segment[1].departureTime;

                let currentHourFormat = current.replace(":", "");
                let beforeHourFormat = before.replace(":", "");

                if (Number(beforeHourFormat) > Number(currentHourFormat)) {
                  done = false;
                  let tmp = this.tickets[i - 1];
                  this.tickets[i - 1] = this.tickets[i];
                  this.tickets[i] = tmp;
                }
              }
            }
          }
        }
        // console.log('Return DepartureTime Result')
        // console.log(this.tickets);

        // set the best result for ver_css_id1_data
        // this.ver_css_id1_data = this.tickets[0];
      }

      resolve();
    });
  }

  InitSortFunction(selectedValue, selectedCard) {
    return new Promise((resolve, reject) => {
      // best filter
      if (selectedValue == 1) {
        let done = false;
        while (!done) {
          done = true;
          for (let i = 1; i < this.tickets.length; i += 1) {
            // get same gbp currency cheapest price
            let current = this.GetLowestPriceWithNumber(this.tickets[i].Agents);
            let before = this.GetLowestPriceWithNumber(
              this.tickets[i - 1].Agents
            );

            // get duration
            let currenth = this.tickets[i].Segment[0].duration;
            let beforeh = this.tickets[i - 1].Segment[0].duration;

            let currenthHourFormat = currenth.replace("h", "");
            let beforehHourFormat = beforeh.replace("h", "");

            // check 00.0 and replace with 00.00
            let currentDotIndex = currenthHourFormat.indexOf(".");
            let currentMinute = currenthHourFormat.substr(currentDotIndex + 1);

            if (currentMinute.length == 1) {
              currenthHourFormat.replace(".", ".0");
            }

            let beforeDotIndex = beforehHourFormat.indexOf(".");
            let beforeMinute = beforehHourFormat.substr(beforeDotIndex + 1);

            if (beforeMinute.length == 1) {
              beforehHourFormat.replace(".", ".0");
            }
            // end check

            if (
              Number(before) * 0.5 + Number(beforehHourFormat) * 0.5 >
              Number(current) * 0.5 + Number(currenthHourFormat) * 0.5
            ) {
              done = false;
              let tmp = this.tickets[i - 1];
              this.tickets[i - 1] = this.tickets[i];
              this.tickets[i] = tmp;
            }
          }
        }

        // console.log('Best Result')
        // console.log(this.tickets);

        // set the best result for ver_css_id1_data
        // this.ver_css_id1_data = this.tickets[0];

        if (selectedCard == 1) {
          this.ver_css_id1 = "active";
          this.ver_css_id2 = "inactive";
          this.ver_css_id3 = "inactive";
        }
      }

      // cheapest filter
      if (selectedValue == 2) {
        let done = false;
        while (!done) {
          done = true;
          for (let i = 1; i < this.tickets.length; i += 1) {
            if (
              this.tickets[i].Agents != null &&
              this.tickets[i - 1].Agents != null
            ) {
              // get same gbp currency cheapest price
              let current = this.GetLowestPriceWithNumber(
                this.tickets[i].Agents
              );
              let before = this.GetLowestPriceWithNumber(
                this.tickets[i - 1].Agents
              );

              if (Number(before) > Number(current)) {
                done = false;
                let tmp = this.tickets[i - 1];
                this.tickets[i - 1] = this.tickets[i];
                this.tickets[i] = tmp;
              }
            }
          }
        }
        // console.log('Cheapest Result')
        // console.log(this.tickets);

        // set the best result for ver_css_id1_data
        // this.ver_css_id1_data = this.tickets[0];

        if (selectedCard == 3) {
          this.ver_css_id1 = "inactive";
          this.ver_css_id2 = "inactive";
          this.ver_css_id3 = "active";
        }
      }

      // fastest filter
      if (selectedValue == 3) {
        // get duration
        let done = false;
        while (!done) {
          done = true;
          for (let i = 1; i < this.tickets.length; i += 1) {
            if (
              this.tickets[i].Agents != null &&
              this.tickets[i - 1].Agents != null
            ) {
              if (
                this.tickets[i].Segment[0] != null &&
                this.tickets[i - 1].Segment[0] != null
              ) {
                // get duration
                let current = this.tickets[i].Segment[0].duration;
                let before = this.tickets[i - 1].Segment[0].duration;

                let currentHourFormat = current.replace("h", "");
                let beforeHourFormat = before.replace("h", "");

                // check 00.0 and replace with 00.00
                let currentDotIndex = currentHourFormat.indexOf(".");
                let currentMinute = currentHourFormat.substr(
                  currentDotIndex + 1
                );

                if (currentMinute.length == 1) {
                  currentHourFormat.replace(".", ".0");
                }

                let beforeDotIndex = beforeHourFormat.indexOf(".");
                let beforeMinute = beforeHourFormat.substr(beforeDotIndex + 1);

                if (beforeMinute.length == 1) {
                  beforeHourFormat.replace(".", ".0");
                }
                // end check

                if (Number(beforeHourFormat) > Number(currentHourFormat)) {
                  done = false;
                  let tmp = this.tickets[i - 1];
                  this.tickets[i - 1] = this.tickets[i];
                  this.tickets[i] = tmp;
                }
              }
            }
          }
        }
        // console.log('Fastest Result')
        // console.log(this.tickets);

        // set the best result for ver_css_id1_data
        // this.ver_css_id1_data = this.tickets[0];

        if (selectedCard == 2) {
          this.ver_css_id1 = "inactive";
          this.ver_css_id2 = "active";
          this.ver_css_id3 = "inactive";
        }
      }

      // outbounded:departure time filter
      if (selectedValue == 4) {
        // get departureTime
        let done = false;
        while (!done) {
          done = true;
          for (let i = 1; i < this.tickets.length; i += 1) {
            if (
              this.tickets[i].Agents != null &&
              this.tickets[i - 1].Agents != null
            ) {
              if (
                this.tickets[i].Segment[0] != null &&
                this.tickets[i - 1].Segment[0] != null
              ) {
                // get departureTime
                let current = this.tickets[i].Segment[0].departureTime;
                let before = this.tickets[i - 1].Segment[0].departureTime;

                let currentHourFormat = current.replace(":", "");
                let beforeHourFormat = before.replace(":", "");

                if (Number(beforeHourFormat) > Number(currentHourFormat)) {
                  done = false;
                  let tmp = this.tickets[i - 1];
                  this.tickets[i - 1] = this.tickets[i];
                  this.tickets[i] = tmp;
                }
              }
            }
          }
        }
        // console.log('Outbounded DepartureTime Result')
        // console.log(this.tickets);

        // set the best result for ver_css_id1_data
        // this.ver_css_id1_data = this.tickets[0];
      }

      // return:departure time filter
      if (selectedValue == 5) {
        // get departureTime
        let done = false;
        while (!done) {
          done = true;
          for (let i = 1; i < this.tickets.length; i += 1) {
            if (
              this.tickets[i].Agents != null &&
              this.tickets[i - 1].Agents != null
            ) {
              if (
                this.tickets[i].Segment[0] != null &&
                this.tickets[i - 1].Segment[0] != null
              ) {
                // get departureTime
                let current = this.tickets[i].Segment[1].departureTime;
                let before = this.tickets[i - 1].Segment[1].departureTime;

                let currentHourFormat = current.replace(":", "");
                let beforeHourFormat = before.replace(":", "");

                if (Number(beforeHourFormat) > Number(currentHourFormat)) {
                  done = false;
                  let tmp = this.tickets[i - 1];
                  this.tickets[i - 1] = this.tickets[i];
                  this.tickets[i] = tmp;
                }
              }
            }
          }
        }
        // console.log('Return DepartureTime Result')
        // console.log(this.tickets);

        // set the best result for ver_css_id1_data
        // this.ver_css_id1_data = this.tickets[0];
      }

      resolve(this.tickets[0]);
    });
  }

  GetLowestPriceWithNumber(agents) {
    if (agents.length === 1) {
      let exRate =
        this.currency_rates[agents[0].agentTicketCurrency] /
        this.currency_rates.gbp;
      return (agents[0].agentTicketPrice * exRate).toFixed(2);
    } else {
      let tempAgent = [];
      let exRate = 0;
      agents.forEach((agent) => {
        tempAgent.push(agent.agentTicketPrice);
      });
      agents.map((agent) => {
        if (agent.agentTicketPrice === Math.min(...tempAgent)) {
          exRate =
            this.currency_rates[agent.agentTicketCurrency] /
            this.currency_rates.gbp;
        }
      });
      return (Math.min(...tempAgent) * exRate).toFixed(2);
    }
  }

  onChangeFilterCheckBoxAirlines(event) {
    console.log(
      "AirLine checkbox Clicked",
      event.target.name,
      event.target.checked
    );

    if (event.target.name == "AF") {
      if (event.target.checked == true) {
        console.log("AF called");
        for (let j = 0; j < this.initialAFResult.length; j++) {
          if (this.initialAFResult[j].Segment[0].Detail[0].airLine == "AF") {
            this.tickets.push(this.initialAFResult[j]);
          }
        }
      } else {
        console.log("AF rejected");
        this.initialAFResult = [];
        for (let i = this.tickets.length - 1; i >= 0; i--) {
          if (this.tickets[i].Segment[0].Detail[0].airLine == "AF") {
            this.initialAFResult.push(this.tickets[i]);
            this.tickets.splice(i, 1);
          }
        }
      }

      this.tickets = this.tickets;
      this.resultCount = this.tickets.length;
      console.log("Direct filter Result:", this.tickets);
    }

    if (event.target.name == "AI") {
      if (event.target.checked == true) {
        console.log("AI called");
        for (let j = 0; j < this.initialAIResult.length; j++) {
          if (this.initialAIResult[j].Segment[0].Detail[0].airLine == "AI") {
            this.tickets.push(this.initialAIResult[j]);
          }
        }
      } else {
        console.log("AI rejected");
        this.initialAIResult = [];
        for (let i = this.tickets.length - 1; i >= 0; i--) {
          if (this.tickets[i].Segment[0].Detail[0].airLine == "AI") {
            this.initialAIResult.push(this.tickets[i]);
            this.tickets.splice(i, 1);
          }
        }
      }

      this.tickets = this.tickets;
      this.resultCount = this.tickets.length;
      console.log("Direct filter Result:", this.tickets);
    }

    if (event.target.name == "BA") {
      if (event.target.checked == true) {
        console.log("BA called");
        for (let j = 0; j < this.initialBAResult.length; j++) {
          if (this.initialBAResult[j].Segment[0].Detail[0].airLine == "BA") {
            this.tickets.push(this.initialBAResult[j]);
          }
        }
      } else {
        console.log("BA rejected");
        this.initialBAResult = [];
        for (let i = this.tickets.length - 1; i >= 0; i--) {
          if (this.tickets[i].Segment[0].Detail[0].airLine == "BA") {
            this.initialBAResult.push(this.tickets[i]);
            this.tickets.splice(i, 1);
          }
        }
      }

      this.tickets = this.tickets;
      this.resultCount = this.tickets.length;
      console.log("Direct filter Result:", this.tickets);
    }

    if (event.target.name == "Others") {
      if (event.target.checked == true) {
        console.log("Others called");
        for (let j = 0; j < this.initialOthersResult.length; j++) {
          if (
            this.initialOthersResult[j].Segment[0].Detail[0].airLine != "BA" &&
            this.initialOthersResult[j].Segment[0].Detail[0].airLine != "AI" &&
            this.initialOthersResult[j].Segment[0].Detail[0].airLine != "AF"
          ) {
            this.tickets.push(this.initialOthersResult[j]);
          }
        }
      } else {
        console.log("Others rejected");
        this.initialOthersResult = [];
        for (let i = this.tickets.length - 1; i >= 0; i--) {
          if (
            this.tickets[i].Segment[0].Detail[0].airLine != "BA" &&
            this.tickets[i].Segment[0].Detail[0].airLine != "AI" &&
            this.tickets[i].Segment[0].Detail[0].airLine != "AF"
          ) {
            this.initialOthersResult.push(this.tickets[i]);
            this.tickets.splice(i, 1);
          }
        }
      }

      this.tickets = this.tickets;
      this.resultCount = this.tickets.length;
      console.log("Others filter Result:", this.tickets);
    }

    this.filterDropdownChange(this.initialSelectedOption);
    this.resetRecommendedResults();
  }

  // responsive function for three top recommended result
  async resetRecommendedResults() {
    // this.initialSelectedOption = selectedValue;
    var sender = this;
    this.spinner.show();
    setTimeout(async function () {
      // show best, fastest and cheapest result
      await sender.InitSortFunction(1, 0).then((res) => {
        // console.log('Best Flight', res)
        sender.ver_css_id1_data = res;
      });

      await sender.InitSortFunction(3, 0).then((res) => {
        // console.log('Fastest Flight', res)
        sender.ver_css_id2_data = res;
      });

      await sender.InitSortFunction(2, 0).then((res) => {
        // console.log('Cheapest Flight', res)
        sender.ver_css_id3_data = res;
      });
      sender.spinner.hide();
    }, 100);
  }

  threeTopSortFunction(selectedValue, selectedCard) {
    this.spinner.show();
    var threeParent = this;
    setTimeout(async function () {
      await threeParent.InitSortFunction(selectedValue, selectedCard);
      threeParent.spinner.hide();
    }, 100);
  }
}
