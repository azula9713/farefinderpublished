import { Component, OnInit, Directive, SimpleChanges } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { HomeService } from '../service/home.service';

import { NgxSpinnerService } from "ngx-spinner";
import { CookieService } from 'ngx-cookie-service';


declare var $: any;
var searchText = "";

@Component({
  selector: 'app-where-to-next',
  templateUrl: './where-to-next.component.html',
  styleUrls: ['./where-to-next.component.css']
})
export class WhereToNextComponent implements OnInit {


  disable;
  style_M;
  style_R;
  isdeleteAccept;

  Form: FormGroup;
  MultipleArray: FormGroup;
  m_City: FormArray;
  BsDatepickerConfig: Partial<BsDatepickerConfig>;
  selectCabin: string = "Economy";
  adultsCount: number = 1;
  childCount: number = 0;
  travelCount: number = 1;

  travellers: string = "adult";
  adltsminactive: boolean = false;
  adltsplusactive: boolean = true;
  childList: any = [];
  btnadlMinDisable: boolean = true;
  btnadlPlusDisable: boolean = false;
  btnChildMinDisable: boolean = true;
  btnchildPlusDisable: boolean = false;

  childrenV2: FormArray;
  FlightsData;

  isWeb: boolean = false;;
  isMobile: boolean = false;

  selected: string;
  states: any[] = [];

  constructor(private fb: FormBuilder, private router: Router, private HomeService: HomeService, private spinner: NgxSpinnerService, private cookieService: CookieService) {
    this.switchScrenn();
    this.BsDatepickerConfig = Object.assign({}, { isAnimated: true, containerClass: 'theme-dark-blue' });

  }


  ngOnInit(): void {

    /** spinner starts on init */
    this.spinner.show();

    this.disable = false;
    this.style_M = "display: none;"
    this.style_R = ""
    this.isdeleteAccept = true;

    this.loadForm();
    this.addToCropArray();
    this.loadFlights();
    this.jqHandler();

  }



  async loadFlights() {
    this.states = [];

    this.states = await this.HomeService.getFlights();
    if (this.states.length > 0) {
      this.spinner.hide();
    }

  }


  switchScrenn() {
    if (window.screen.width <= 600) {//mobile
      this.isWeb = false;
      this.isMobile = true;
    }
    else { //web
      this.isMobile = false;
      this.isWeb = true;
    }
  }


  //-----------------------------------------------------------------------


  loadForm() {
    this.Form = this.fb.group({
      'c_Return': [true],
      'c_OneWay': [false],
      'c_Multiple': [false],
      'ro_From': [''],
      'ro_FromCode': [''],
      'ro_To': [''],
      'ro_ToCode': [''],
      'ro_Depart': [''],
      'ro_Return': [''],
      'ro_Cabin': [''],
      'ro_F_addN': [false],
      'ro_T_addN': [false],
      'ro_Direct': [false],
      'm_City': this.fb.array([this.loadMultipleArray()]),
      'm_Adlt': [''],
      'cabinClass': ["Economy", Validators.required],
      'travellers': [1, Validators.required],
      'adults': [1, Validators.required],
      'children': [0, Validators.required],
      'childrenV2': this.fb.array([])
    });

  }

  loadMultipleArray() {
    this.MultipleArray = this.fb.group({
      'm_From': [''],
      'm_FromCode': [''],
      'm_To': [''],
      'm_ToCode': [''],
      'm_date': [''],
    });
    return this.MultipleArray;
  }

  loadChildren(): FormGroup {
    return this.fb.group({
      'Age': [-1]
    });
  }


  //-----------------------------------------------------------------------

  addChildren() {
    this.childrenV2 = this.Form.get('childrenV2') as FormArray;
    this.childrenV2.push(this.loadChildren());
  }

  removeChildren(index: number) {
    this.childrenV2 = this.Form.get('childrenV2') as FormArray;
    if (this.childrenV2.length >= 0) {
      this.childrenV2.removeAt(index);
    }

  }

  addToCropArray() {
    this.formMultiCity.push(this.loadMultipleArray());
  }

  addFlight() {
    if (this.formMultiCity.length <= 4) {
      this.isdeleteAccept = false;
      this.addToCropArray();
    }
  }

  removeFlight(index: number) {
    if (this.formMultiCity.length > 2) {
      this.formMultiCity.removeAt(index);
    }
    if (this.formMultiCity.length <= 2) {
      this.isdeleteAccept = true;
    }
    else {
      this.isdeleteAccept = false;
    }

  }


  updateMultiCityFlightArrayFlightCode(fromFlight, toFlight, index) {
    this.formMultiCity.at(index).patchValue({
      'm_FromCode': ((fromFlight).split(/[()]/))[1],
      'm_ToCode': ((toFlight).split(/[()]/))[1]
    });
  }

  updateMultiCityFlightArrayDates(date, index) {
    this.formMultiCity.at(index).patchValue({
      'm_date': date
    });
  }


  //-----------------------------------------------------------------------



  ReturnClick(): void {
    this.Form.controls['ro_Return'].reset();
    this.Form.controls['ro_Return'].enable();
    this.disable = false;
    this.style_M = "display: none;"
    this.style_R = ""

    this.Form.controls['c_Return'].setValue(true);
    this.Form.controls['c_OneWay'].setValue(false);
    this.Form.controls['c_Multiple'].setValue(false);

  }
  OnewayClick(): void {

    this.Form.controls['ro_Return'].setValue("(One Way)");
    this.Form.controls['ro_Return'].disable();
    this.disable = true;
    this.style_M = "display: none;"
    this.style_R = ""

    this.Form.controls['c_Return'].setValue(false);
    this.Form.controls['c_OneWay'].setValue(true);
    this.Form.controls['c_Multiple'].setValue(false);

  }
  MultiCityClick(): void {
    this.Form.controls['ro_Return'].enable();
    this.disable = false;
    this.style_M = ""
    this.style_R = "display: none;"


    this.Form.controls['c_Return'].setValue(false);
    this.Form.controls['c_OneWay'].setValue(false);
    this.Form.controls['c_Multiple'].setValue(true);


  }






  //-----------------------------------------------------------------------



  onSearch() {
    // debugger;



    var that = this;


    this.Form.controls['ro_FromCode'].setValue(((this.Form.controls['ro_From'].value).split(/[()]/))[1]);
    this.Form.controls['ro_ToCode'].setValue(((this.Form.controls['ro_To'].value).split(/[()]/))[1]);
    var childrenAge = ((this.Form.controls['childrenV2'].value).map(x => x.Age).join(","));

    console.log('XXXXXXX', this.Form);

    if (this.Form.controls['c_OneWay'].value === true) {
      this.router.navigate(['/Flights', 0, this.Form.controls['ro_FromCode'].value, this.Form.controls['ro_ToCode'].value, this.Form.controls['ro_Depart'].value.replace(/\-/g, '')], {

        queryParams: {

          travellers: this.Form.controls['travellers'].value,
          adults: this.Form.controls['adults'].value,
          children: this.Form.controls['children'].value,
          childrenV2: childrenAge,
          cabinClass: this.Form.controls['cabinClass'].value,
          preferdirects: this.Form.controls['ro_Direct'].value,
          outboundaltsenabled: this.Form.controls['ro_T_addN'].value,
          inboundaltsenabled: this.Form.controls['ro_F_addN'].value,
          ref: 'home'

        }

      });

    }

    else if (this.Form.controls['c_Return'].value === true) {
      this.router.navigate(['/Flights', 1, this.Form.controls['ro_FromCode'].value, this.Form.controls['ro_ToCode'].value, this.Form.controls['ro_Depart'].value.replace(/\-/g, ''), this.Form.controls['ro_Return'].value.replace(/\-/g, '')], {

        queryParams: {

          travellers: this.Form.controls['travellers'].value,
          adults: this.Form.controls['adults'].value,
          children: this.Form.controls['children'].value,
          childrenV2: childrenAge,
          cabinClass: this.Form.controls['cabinClass'].value,
          preferdirects: this.Form.controls['ro_Direct'].value,
          outboundaltsenabled: this.Form.controls['ro_T_addN'].value,
          inboundaltsenabled: this.Form.controls['ro_F_addN'].value,
          ref: 'home'

        }


      });

    }


    else if (this.Form.controls['c_Multiple'].value === true) {

      this.formMultiCity.getRawValue().forEach(function (element, index) {
        that.updateMultiCityFlightArrayFlightCode(element.m_From, element.m_To, index);
      });


      if (this.formMultiCity.length === 2) {
        this.router.navigate(['/Flights', 2,
          this.formMultiCity.controls[0].get('m_FromCode').value, this.formMultiCity.controls[0].get('m_ToCode').value, this.formMultiCity.controls[0].get('m_date').value.replace(/\-/g, ''),
          this.formMultiCity.controls[1].get('m_FromCode').value, this.formMultiCity.controls[1].get('m_ToCode').value, this.formMultiCity.controls[1].get('m_date').value.replace(/\-/g, '')], {

          queryParams: {

            travellers: this.Form.controls['travellers'].value,
            adults: this.Form.controls['adults'].value,
            children: this.Form.controls['children'].value,
            childrenV2: childrenAge,
            cabinClass: this.Form.controls['cabinClass'].value,
            ref: 'home'

          }




        });
      }

      else if (this.formMultiCity.length === 3) {
        this.router.navigate(['/Flights', 2,
          this.formMultiCity.controls[0].get('m_FromCode').value, this.formMultiCity.controls[0].get('m_ToCode').value, this.formMultiCity.controls[0].get('m_date').value.replace(/\-/g, ''),
          this.formMultiCity.controls[1].get('m_FromCode').value, this.formMultiCity.controls[1].get('m_ToCode').value, this.formMultiCity.controls[1].get('m_date').value.replace(/\-/g, ''),
          this.formMultiCity.controls[2].get('m_FromCode').value, this.formMultiCity.controls[2].get('m_ToCode').value, this.formMultiCity.controls[2].get('m_date').value.replace(/\-/g, '')], {

          queryParams: {

            travellers: this.Form.controls['travellers'].value,
            adults: this.Form.controls['adults'].value,
            children: this.Form.controls['children'].value,
            childrenV2: childrenAge,
            cabinClass: this.Form.controls['cabinClass'].value,
            ref: 'home'

          }

        });
      }

      else if (this.formMultiCity.length === 4) {
        this.router.navigate(['/Flights', 2,
          this.formMultiCity.controls[0].get('m_FromCode').value, this.formMultiCity.controls[0].get('m_ToCode').value, this.formMultiCity.controls[0].get('m_date').value.replace(/\-/g, ''),
          this.formMultiCity.controls[1].get('m_FromCode').value, this.formMultiCity.controls[1].get('m_ToCode').value, this.formMultiCity.controls[1].get('m_date').value.replace(/\-/g, ''),
          this.formMultiCity.controls[2].get('m_FromCode').value, this.formMultiCity.controls[2].get('m_ToCode').value, this.formMultiCity.controls[2].get('m_date').value.replace(/\-/g, ''),
          this.formMultiCity.controls[3].get('m_FromCode').value, this.formMultiCity.controls[3].get('m_ToCode').value, this.formMultiCity.controls[3].get('m_date').value.replace(/\-/g, '')], {

          queryParams: {

            travellers: this.Form.controls['travellers'].value,
            adults: this.Form.controls['adults'].value,
            children: this.Form.controls['children'].value,
            childrenV2: childrenAge,
            cabinClass: this.Form.controls['cabinClass'].value,
            ref: 'home'

          }

        });
      }

      else if (this.formMultiCity.length === 5) {
        this.router.navigate(['/Flights', 2,
          this.formMultiCity.controls[0].get('m_FromCode').value, this.formMultiCity.controls[0].get('m_ToCode').value, this.formMultiCity.controls[0].get('m_date').value.replace(/\-/g, ''),
          this.formMultiCity.controls[1].get('m_FromCode').value, this.formMultiCity.controls[1].get('m_ToCode').value, this.formMultiCity.controls[1].get('m_date').value.replace(/\-/g, ''),
          this.formMultiCity.controls[2].get('m_FromCode').value, this.formMultiCity.controls[2].get('m_ToCode').value, this.formMultiCity.controls[2].get('m_date').value.replace(/\-/g, ''),
          this.formMultiCity.controls[3].get('m_FromCode').value, this.formMultiCity.controls[3].get('m_ToCode').value, this.formMultiCity.controls[3].get('m_date').value.replace(/\-/g, ''),
          this.formMultiCity.controls[4].get('m_FromCode').value, this.formMultiCity.controls[4].get('m_ToCode').value, this.formMultiCity.controls[4].get('m_date').value.replace(/\-/g, '')], {

          queryParams: {

            travellers: this.Form.controls['travellers'].value,
            adults: this.Form.controls['adults'].value,
            children: this.Form.controls['children'].value,
            childrenV2: childrenAge,
            cabinClass: this.Form.controls['cabinClass'].value,
            ref: 'home'

          }


        });
      }




    }

  }




  onGetSelectedFlight(flight, type, index, roIndex) {


    if (type == 'fromFlight') {
      this.Form.controls['ro_From'].setValue(flight.name);
      this.Form.controls['ro_FromCode'].setValue(flight.flightCode);
      $('#ro_FromId').slideUp();
    }
    else if (type == 'toFlight') {
      this.Form.controls['ro_To'].setValue(flight.name);
      this.Form.controls['ro_ToCode'].setValue(flight.flightCode);

      $('#ro_ToId').slideUp();
    }

    else if (type == 'fromFlightMobile') {
      this.Form.controls['ro_From'].setValue(flight.name);
      this.Form.controls['ro_FromCode'].setValue(flight.flightCode);

      $('#ro_FromIdMobileDropDownBoxControll').slideUp();
    }
    else if (type == 'toFlightMobile') {
      this.Form.controls['ro_To'].setValue(flight.name);
      this.Form.controls['ro_ToCode'].setValue(flight.flightCode);

      $('#ro_ToIdMobileDropDownBoxControll').slideUp();
    }

    else if (type == 'fromFlightMultiWay') {

      (this.Form.controls['m_City'] as FormArray).at(roIndex).patchValue({
        m_From: flight.name,
        m_FromCode: flight.flightCode
      });
      $('#from' + roIndex).slideUp();
    }

    else if (type == 'toFlightMultiWay') {
      (this.Form.controls['m_City'] as FormArray).at(roIndex).patchValue({
        m_To: flight.name,
        m_ToCode: flight.flightCode
      });
      $('#to' + roIndex).slideUp();

    }

    else if (type == 'fromFlightMultiWayMobile') {
      (this.Form.controls['m_City'] as FormArray).at(roIndex).patchValue({
        m_From: flight.name,
        m_FromCode: flight.flightCode
      });
      $('#mFrom' + roIndex).slideUp();
    }



    else if (type == 'toFlightMultiWayMobile') {
      (this.Form.controls['m_City'] as FormArray).at(roIndex).patchValue({
        m_To: flight.name,
        m_ToCode: flight.flightCode
      });
      $('#mTo' + roIndex).slideUp();
    }



  }


  onCloseCabinTravellersSelectDropDown() {


    if (window.screen.width <= 600) { //mobile
      if (this.Form.controls['c_Multiple'].value === true) {
        $('#cabinClassMenuMultiWayMobile').close();
      }
      else {
        $('#cabinClassMenuOneWayMobile').close();
      }
    }

    else { //web

      if (this.Form.controls['c_Multiple'].value === true) {
        $('#cabinClassMenuMultiWayWeb').hide();
      }
      else {
        $("#cabinClassToggleOneWayWeb").dropdown("toggle");

      }

    }



  }


  onApplyPickerMultiWay(type,id, index) {

    var date;

    if (index === 0 || index === 1) {
      date = new Date();
    }
    else {
      date = this.formMultiCity.value[index - 1]['m_date'];
    }

    $('#' + id).datepicker({
      dateFormat: 'yy-mm-dd',
      minDate: date,
      onClose: (dateText, inst) => {
        this.updateMultiCityFlightArrayDates(dateText, index);
        this.changeMinDateInMultiCity(dateText, id, index, type)
      }
    });

  }

  changeMinDateInMultiCity(date, id, index, type) {

    if (!date) return;

    if (type === 'web') {

      if (id === 'datePickerMultiWay1') {
        $('#datePickerMultiWay2').datepicker('option', 'minDate', new Date(date));
      }
      else if (id === 'datePickerMultiWay2') {
        $('#datePickerMultiWay3').datepicker('option', 'minDate', new Date(date));
      }
      else if (id === 'datePickerMultiWay3') {
        $('#datePickerMultiWay4').datepicker('option', 'minDate', new Date(date));
      }
      else if (id === 'datePickerMultiWay4') {
        $('#datePickerMultiWay5').datepicker('option', 'minDate', new Date(date));
      }

    }
    else{

      if (id === 'datePickerMultiWayMobile1') {
        $('#datePickerMultiWayMobile2').datepicker('option', 'minDate', new Date(date));
      }
      else if (id === 'datePickerMultiWayMobile2') {
        $('#datePickerMultiWayMobile3').datepicker('option', 'minDate', new Date(date));
      }
      else if (id === 'datePickerMultiWayMobile3') {
        $('#datePickerMultiWayMobile4').datepicker('option', 'minDate', new Date(date));
      }
      else if (id === 'datePickerMultiWayMobile4') {
        $('#datePickerMultiWayMobile5').datepicker('option', 'minDate', new Date(date));
      }

    }

  }






  //-----------------------------------------------------------------------

  get formMultiCity() {
    return this.Form.get('m_City') as FormArray;
  }


  getFlights() {
    return this.HomeService.getFlights1();
  }



  //-----------------------------------------------------------------------


  jqHandler() {

    var that = this;

    $(function () {


      $("#datePickerDeparture").datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(),
        autoSize: true,
        onClose: (dateText, inst) => {
          that.Form.controls['ro_Depart'].setValue(dateText);
          that.changeMinDate(dateText, 'web');
        }
      });

      $("#datePickerReturn").datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(),
        autoSize: true,
        onClose: (dateText, inst) => {
          that.Form.controls['ro_Return'].setValue(dateText);
        }
      });



      $("#datePickerDepartureMobile").datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(),
        onClose: (dateText, inst) => {
          that.Form.controls['ro_Depart'].setValue(dateText);
          that.changeMinDate(dateText, 'mobile');
        }
      });

      $("#datePickerReturnMobile").datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: new Date(),
        onClose: (dateText, inst) => {
          that.Form.controls['ro_Return'].setValue(dateText);
        }
      });



      $('#cabinClassMenuOneWayWeb').on('click', function (event) {
        event.stopPropagation();
      });

      $('#cabinClassMenuMultiWayWeb').on('click', function (event) {
        event.stopPropagation();
      });



      $('#cabinClassMenuOneWayMobile').on('click', function (event) {
        event.stopPropagation();
      });

      $('#cabinClassMenuMultiWayMobile').on('click', function (event) {
        event.stopPropagation();
      });



    });
  }


  changeMinDate(date, type) {
    if (!date) return;

    if (type === 'web') {
      $('#datePickerReturn').datepicker('option', 'minDate', new Date(date));
    }
    else {
      $('#datePickerReturnMobile').datepicker('option', 'minDate', new Date(date));
    }


  }


  //-----------------------------------------------------------------------


  selectOption(value) {
    this.Form.controls['cabinClass'].setValue(value);
    this.selectCabin = value
  }
  checkTraveller() {
    this.travellers = (this.travelCount > 1) ? "travellers" : "adult";

    this.Form.controls['travellers'].setValue(this.travelCount);
    this.Form.controls['adults'].setValue(this.adultsCount);
    this.Form.controls['children'].setValue(this.childCount);
  }
  adultsMin() {
    if (this.adultsCount > 2) {
      this.adultsCount -= 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.btnadlMinDisable = false;
      this.btnadlPlusDisable = false;
    }
    else {
      this.adultsCount -= 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.btnadlMinDisable = true;
    }
    this.checkTraveller();
  }
  adultsPlus() {
    if (this.adultsCount < 7) {
      this.adultsCount += 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.btnadlMinDisable = false;
    } else {
      this.adultsCount += 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.btnadlPlusDisable = true;
    }
    this.checkTraveller();
  }
  childsMin() {
    if (this.childCount > 1) {
      this.childCount -= 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.childList.pop(this.childCount);
      this.btnChildMinDisable = false;
      this.btnchildPlusDisable = false;
      this.removeChildren(0);
    }
    else {
      this.childCount -= 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.childList.pop(this.childCount);
      this.btnChildMinDisable = true;
      this.removeChildren(0);

    }
    this.checkTraveller();
  }
  childsPlus() {
    if (this.childCount < 7) {
      this.childCount += 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.childList.push(this.childCount);
      this.btnChildMinDisable = false;

      this.addChildren();

    }
    else {
      this.childCount += 1;
      this.travelCount = this.adultsCount + this.childCount;
      this.childList.push(this.childCount);
      this.btnchildPlusDisable = true;
      this.addChildren();

    }
    this.checkTraveller();

  }


}
