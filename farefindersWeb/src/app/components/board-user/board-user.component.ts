import { Component, OnInit } from "@angular/core";
import { AccountService } from "src/app/services/account.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-board-user",
  templateUrl: "./board-user.component.html",
  styleUrls: ["./board-user.component.css"],
})
export class BoardUserComponent implements OnInit {
  content: string;
  currentUser = null;
  username = "";
  firstName: any;
  lastName: any;
  email: any;
  fullname: any;
  hideName: boolean = true;
  roles: string;

  constructor(private AccountService: AccountService, private router: Router) {}

  ngOnInit(): void {
    this.AccountService.getUserPrivilage();
    this.getUserDetails();
  }

  getUserDetails(): void {
    let user = JSON.parse(localStorage.getItem("user"));
    this.username = user.username;
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.email = user.email;
    this.fullname = this.firstName + " " + this.lastName;
    if (this.roles == "admin") {
      this.router.navigate(["/"]);
    } 
    console.log(this.username);
    console.log(this.username);
  }
}
