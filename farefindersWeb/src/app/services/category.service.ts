import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Category } from '../models/category';
import { environment } from 'src/environments/environment';

// const baseUrl = 'http://localhost:3001/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private categorySubject: BehaviorSubject<Category>;

  constructor(private http: HttpClient) { }

  public get categoryValue(): Category {
    return this.categorySubject.value;
  }

  create(category: Category) {
    return this.http.post(`${environment.apiUrl}/category/create`, category);
  }

  getAll() {
    return this.http.get<Category[]>(`${environment.apiUrl}/category/all`);
  }

  getById(id: string) {
    return this.http.get<Category>(`${environment.apiUrl}/category/${id}`);
  }

  update(id, params) {
    return this.http.put(`${environment.apiUrl}/category/${id}`, params);
  }

  delete(id: string) {
    return this.http.delete(`${environment.apiUrl}/category/${id}`);
  }
}
