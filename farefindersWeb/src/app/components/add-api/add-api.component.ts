import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AlertService } from "src/app/services/alert.service";
import { CategoryService } from "src/app/services/category.service";
import { PostService } from "src/app/services/posts.service";
import { AccountService } from "src/app/services/account.service";
import { first } from 'rxjs/operators';

@Component({
  selector: "app-add-api",
  templateUrl: "./add-api.component.html",
  styleUrls: ["./add-api.component.css"],
})
export class AddApiComponent implements OnInit {
  form: FormGroup;
  loading = false;
  submitted = false;
  categories: any;
  imgURL1: any;
  image1: any;

  constructor(
    private categoryService: CategoryService,
    private postService: PostService,
    private formBuilder: FormBuilder,
    private AccountService: AccountService,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.AccountService.getUserPrivilage();
    this.form = this.formBuilder.group({
      category: ["", Validators.required],
      city: ["", Validators.required],
      country: ["", Validators.required],
      description: [""],
      img: [""],
    });
    this.retrievecategories();
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      if (
        event.target.files[0].type === "image/png" ||
        event.target.files[0].type === "image/jpg"
      ) {
        reader.onload = (_event) => {
          // called once readAsDataURL is completed
          this.imgURL1 = reader.result;
          this.image1 = this.imgURL1.toString().slice(22);
        };
      } else if (event.target.files[0].type === "image/jpeg") {
        reader.onload = (_event) => {
          // called once readAsDataURL is completed
          this.imgURL1 = reader.result;
          this.image1 = this.imgURL1.toString().slice(23);
        };
      }
    }
  }

  get f() {
    return this.form.controls;
  }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    console.log(this.form.value);
    this.postService
      .create(this.form.value)
      .pipe(first())
      .subscribe(
        (data) => {
          this.alertService.success("Post creation successful", {
            keepAfterRouteChange: true,
          });
          // this.router.navigate(['../login'], { relativeTo: this.route });
          this.router.navigate(["/admin"]);
        },
        (error) => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
  }

  retrievecategories(): void {
    this.categoryService.getAll().subscribe(
      (data) => {
        this.categories = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
