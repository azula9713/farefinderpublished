import { Component, OnInit } from '@angular/core';
import { DestinationService } from 'src/app/services/destination.service';
import { AccountService } from "src/app/services/account.service";

@Component({
  selector: 'app-destination-list',
  templateUrl: './destination-list.component.html',
  styleUrls: ['./destination-list.component.css']
})
export class DestinationListComponent implements OnInit {categories: any;
  currentdestination: any;
  currentIndex: any;
  destinations: any;
  message: string;

  constructor(
    private destinationService: DestinationService,
    private AccountService: AccountService
  ) {}

  ngOnInit(): void {
    this.AccountService.getUserPrivilage();
    this.retrievedestinations();
  }

  retrievedestinations(): void {
    this.destinationService.getAll().subscribe(
      (data) => {
        this.destinations = data;
        console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  setActivedestination(destination, index): void {
    this.currentdestination = destination;
    this.currentIndex = index;
  }

  refreshList(): void {
    this.retrievedestinations();
    this.currentdestination = null;
    this.currentIndex = -1;
  }

  updatedestination(): void {
    // this.currentdestination.img = this.imgURL1;
    // console.log("image is "+ this.imgURL1)
    this.destinationService.update(this.currentdestination.id, this.currentdestination)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'The destination was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }
}
