export class Destination {
  id: string;
  destinationName: string;
  destinationDescription: string;
}