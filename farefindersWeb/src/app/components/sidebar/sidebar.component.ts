import { Component, OnInit } from "@angular/core";
import { AccountService } from "src/app/services/account.service";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"],
})
export class SidebarComponent implements OnInit {
  users: any;
  pendingcount: any;

  constructor(private AccountService: AccountService) {}

  ngOnInit(): void {
    this.AccountService.getAllPending().subscribe(
      (data) => {
        this.users = data;
        this.pendingcount = this.users.length;
        console.log(this.pendingcount); 
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
