import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AccountService } from "src/app/services/account.service";

@Component({
  selector: "app-topbar",
  templateUrl: "./topbar.component.html",
  styleUrls: ["./topbar.component.css"],
})
export class TopbarComponent implements OnInit {
  content: string;
  currentUser = null;
  username = "";
  firstName: any;
  lastName: any;
  email: any;
  fullname: any;
  hideName: boolean = true;
  now: Date;
  greet: string;

  constructor(private AccountService: AccountService, private router: Router) {}

  ngOnInit(): void {
    this.AccountService.getUserPrivilage();
    this.getUserDetails();
    this.greeting();
  }

  getUserDetails(): void {
    let user = JSON.parse(localStorage.getItem("user"));
    this.username = user.username;
    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.email = user.email;
    this.fullname = this.firstName + " " + this.lastName;
    console.log(this.username);
  }

  handleSignOut() {
    this.router.navigate(["/"]);
    localStorage.clear();
  }

  greeting() {
    var time = new Date();
    var currtime = time.toLocaleString('en-US', { hour: 'numeric', hour12: true });   
    if (currtime.includes('AM')) {
     this.greet= 'Morning'
    } else if (currtime.includes('PM')) {
      this.greet = 'Evening'
    }
    console.log(this.greet);
  }
}
