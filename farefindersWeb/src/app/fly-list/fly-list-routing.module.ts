import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from './mainpage/mainpage.component';
import { TicketComponent } from './ticket/ticket.component';


const routes: Routes = [

  {path: 'Ticket', component: TicketComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FlyListRoutingModule { }
