import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { PostService } from 'src/app/services/posts.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-posts-category',
  templateUrl: './posts-category.component.html',
  styleUrls: ['./posts-category.component.css']
})
export class PostsCategoryComponent implements OnInit {
  username: any;
  posts: any;
  currentpost = null;
  currentIndex = -1;
  categories: any;
  category: any;
  catTitle = ''

  constructor(private postService: PostService, private tokenStorage: TokenStorageService, private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.getUserDetails();
    this.retrievecategories();
    // this.getPostsByCategory()
  }
  getUserDetails(): void{
    this.username = this.tokenStorage.getUser().username;
  }

  retrievecategories(): void {
    this.categoryService.getAll()
      .subscribe(
        data => {
          this.categories = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  getPostsByCategory(): void{
    // this.categoryService.getAll()
    //   .subscribe(
    //     data => {
    //       this.categories = data;
    //       for (let category of this.categories) {
    //         this.catTitle = category.catTitle;
    //         this.postService.findByCategory(this.catTitle)
    //           .subscribe(
    //             data => {
    //               this.posts = data;
    //               console.log("cat =" + this.catTitle);
    //               console.log(data);
    //             },
    //             error => {
    //               console.log(error);
    //             });
    //     }
    //   },
    //   error => {
    //     console.log(error);
    //   });
  }
}
