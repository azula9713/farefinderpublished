const { DataTypes, STRING } = require('sequelize');

module.exports = model;

function model(sequelize) {
    const attributes = {
        apiurl: { type: DataTypes.STRING, allowNull: false },
        apiname: { type: DataTypes.STRING, allowNull: false },
        apiusername: { type: DataTypes.STRING, allowNull: false },
        apidescription: { type: DataTypes.STRING, allowNull: true }
    };

    return sequelize.define('Userapi', attributes);
}