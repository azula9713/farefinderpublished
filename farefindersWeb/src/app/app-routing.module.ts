import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ContentComponent } from "./home/content/content.component";
import { PostsListComponent } from "./components/posts-list/posts-list.component";
import { PostDetailsComponent } from "./components/post-details/post-details.component";
import { AddPostComponent } from "./components/add-posts/add-posts.component";
import { HomeComponent } from "./components/home/home.component";
import { ProfileComponent } from "./components/profile/profile.component";
import { BoardUserComponent } from "./components/board-user/board-user.component";
import { BoardModeratorComponent } from "./components/board-moderator/board-moderator.component";
import { BoardAdminComponent } from "./components/board-admin/board-admin.component";
import { AddCategoryComponent } from "./components/add-category/add-category.component";
import { PostsCategoryComponent } from "./components/posts-category/posts-category.component";
import { CategoriesComponent } from "./components/categories/categories.component";
import { DestinationAddComponent } from "./components/destination-add/destination-add.component";
import { DestinationListComponent } from "./components/destination-list/destination-list.component";
import { DestinationDetailsComponent } from "./components/destination-details/destination-details.component";
import { AboutUsComponent } from "./components/about-us/about-us.component";
import { TermsConditionsComponent } from "./components/terms-conditions/terms-conditions.component";
import { CookiePolicyComponent } from "./components/cookie-policy/cookie-policy.component";
import { PrivacyPolicyComponent } from "./components/privacy-policy/privacy-policy.component";
import { ErrorNotfoundComponent } from "./components/error-notfound/error-notfound.component";
import { ErrorInternalserverComponent } from "./components/error-internalserver/error-internalserver.component";
import { MainpageComponent } from "./fly-list/mainpage/mainpage.component";
import { UserTableComponent } from './components/user-table/user-table.component';
import { AddApiComponent } from './components/add-api/add-api.component';
import { PendingUsersComponent } from './components/pending-users/pending-users.component';
const accountModule = () =>
  import("./components/account/account.module").then((x) => x.AccountModule);

const routes: Routes = [
  { path: "", component: ContentComponent },
  { path: "account", loadChildren: accountModule },
  { path: "posts", component: PostsListComponent },
  { path: "cookiepolicy", component: CookiePolicyComponent },
  { path: "privacypolicy", component: PrivacyPolicyComponent },
  { path: "categories", component: CategoriesComponent },
  { path: "posts/:id", component: PostDetailsComponent },
  { path: "createpost/add", component: AddPostComponent },
  { path: "createcategory/add", component: AddCategoryComponent },
  { path: "createdestination/add", component: DestinationAddComponent },
  { path: "destination/:id", component: DestinationDetailsComponent },
  { path: "destinations", component: DestinationListComponent },
  { path: "registeredusers", component: UserTableComponent },
  { path: "pendingusers", component: PendingUsersComponent },
  { path: "home", component: HomeComponent },
  { path: "profile", component: ProfileComponent },
  // { path: "user", component: BoardUserComponent },
  { path: "mod", component: BoardModeratorComponent },
  { path: "admin", component: BoardAdminComponent },
  { path: "about", component: AboutUsComponent },
  { path: "terms", component: TermsConditionsComponent },
  { path: "user", component: BoardUserComponent },
  { path: "user/addapi", component: AddApiComponent },
  { path: "post/category", component: PostsCategoryComponent },
  { path: "", redirectTo: "home", pathMatch: "full" },

  {
    path: "Flights/:flightType/:fromFlight/:toFlight/:fromFlightDate",
    component: MainpageComponent,
  },
  {
    path:
      "Flights/:flightType/:fromFlight/:toFlight/:fromFlightDate/:toFlightDate",
    component: MainpageComponent,
  },

  {
    path:
      "Flights/:flightType/:fromFlight1/:toFlight1/:fromFlightDate1/:fromFlight2/:toFlight2/:fromFlightDate2",
    component: MainpageComponent,
  },
  {
    path:
      "Flights/:flightType/:fromFlight1/:toFlight1/:fromFlightDate1/:fromFlight2/:toFlight2/:fromFlightDate2/:fromFlight3/:toFlight3/:fromFlightDate3",
    component: MainpageComponent,
  },
  {
    path:
      "Flights/:flightType/:fromFlight1/:toFlight1/:fromFlightDate1/:fromFlight2/:toFlight2/:fromFlightDate2/:fromFlight3/:toFlight3/:fromFlightDate3/:fromFlight4/:toFlight4/:fromFlightDate4",
    component: MainpageComponent,
  },
  {
    path:
      "Flights/:flightType/:fromFlight1/:toFlight1/:fromFlightDate1/:fromFlight2/:toFlight2/:fromFlightDate2/:fromFlight3/:toFlight3/:fromFlightDate3/:fromFlight4/:toFlight4/:fromFlightDate4/:fromFlight5/:toFlight5/:fromFlightDate5",
    component: MainpageComponent,
  },
  { path: "404", component: ErrorNotfoundComponent },
  { path: "account/login", component: ErrorNotfoundComponent },
  { path: "500", component: ErrorInternalserverComponent },
  { path: "**", redirectTo: "/404" },
];
// , canActivate: [AuthGuard]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
