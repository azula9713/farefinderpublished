export class Post {
  id: string;
  city: string;
  country: string;
  category: string;
  description: string;
  img: string;
}
