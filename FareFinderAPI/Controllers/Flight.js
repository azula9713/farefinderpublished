var express = require('express');
var router = express.Router();
var Logger = require('../Logger').Logger;
var Help = require('../AppHelp/Help');
var crypto = require('crypto');
var http = require("http");
var tesingJsonArray = require('../AppHelp/TestJson');
const axios = require('axios').default;

var createTravelPayOutObj = {};
var createTravelPayOutObjSegment = [];
var search_id;








//---------------------------------------------------------------------------------------------------------------


router.post('/Flight', async(request, result) => {

    let body = request.body;
    let flightType = "";
    let tripClass = "";
    let stringSignarutre = "";
    let flightTypeName = "return";


    if (body.c_OneWay === true) {
        flightType = 0;
        flightTypeName = "oneWay";
    } else if (body.c_Return === true) {
        flightType = 1;
        flightTypeName = "return";
    } else if (body.c_Multiple === true) {
        flightType = 2;
        flightTypeName = "multiWay"
    };

    if (body.cabinClass === 'Economy') { tripClass = 'Y'; } else if (body.cabinClass === 'Premium Economy') { tripClass = 'Y'; } else if (body.cabinClass === 'Business Class') { tripClass = 'C'; } else if (body.cabinClass === 'First Class') { tripClass = 'C'; }



    if (flightType === 0) {

        stringSignarutre = Help.userToken + ':' + Help.host + ':' + Help.language + ':' + Help.marker +
            ':' + body.adults + ':' + body.children + ':' + 0 +
            ':' + body.ro_Depart + ':' + body.ro_ToCode + ':' + body.ro_FromCode +
            ':' + tripClass + ':' + Help.userIp

    } else if (flightType === 1) {

        stringSignarutre = Help.userToken + ':' + Help.host + ':' + Help.language + ':' + Help.marker +
            ':' + body.adults + ':' + body.children + ':' + 0 +
            ':' + body.ro_Depart + ':' + body.ro_ToCode + ':' + body.ro_FromCode +
            ':' + body.ro_Return + ':' + body.ro_FromCode + ':' + body.ro_ToCode +
            ':' + tripClass + ':' + Help.userIp

    } else if (flightType === 2) {

        var tempSegments = "";

        body.m_City.forEach(x => {
            tempSegments = tempSegments + x.m_date + ':' + x.m_ToCode + ':' + x.m_FromCode + ':'
        });


        stringSignarutre = Help.userToken + ':' + Help.host + ':' + Help.language + ':' + Help.marker +
            ':' + body.adults + ':' + body.children + ':' + 0 +
            ':' + tempSegments +
            tripClass + ':' + Help.userIp
    }



    var md5Key = generateMd5Key(stringSignarutre);
    var creatTravelPayOutJsonObjectRes = creatTravelPayOutJsonObject(body, flightType, tripClass, md5Key);

    try {

        let data = await createNewToken(creatTravelPayOutJsonObjectRes);
        result.send(data);

    } catch (er) {
        console.log(er);
    }

});


// get agent website link
router.post('/Flight/getagentlink', async(req, res) => {
    console.log('get agent website link post request is called now!')
    console.log(req.body);
    let result = {};
    await axios.get('http://api.travelpayouts.com/v1/flight_searches/' + req.body.searchId + '/clicks/' + req.body.termsUrl + '.json').then(resp => {
        console.log('response from agent website link get request')
        console.log(resp.data);
        result = resp.data
    });



    res.send(result);
});




router.get('/Flight/:searchId/:searchType/:isDirect', async(req, resultSet) => {
    try {
        getFlightSearchDataSet(req.params.searchId).then(flightsInfo => {
            createNewJson(req.params.searchType, req.params.isDirect, flightsInfo, (err, res) => {
                if (!err) {
                    resultSet.send(res);
                } else resultSet.send([]);
            });


        })
    } catch (Ex) {
        console.log(Ex);
    }
});










//#region  Helping methods ------------------------------------------------------------------------------------------------------



function generateMd5Key(stringSignarutre) {
    try {
        return (crypto.createHash('md5').update(stringSignarutre).digest("hex"));
    } catch (ex) {
        return ex.message;
    }
}

function creatTravelPayOutJsonObject(body, flightType, tripClass, md5Res) {


    try {

        createTravelPayOutObj = {};
        createTravelPayOutObjSegment = [];


        if (flightType == 0) {
            createTravelPayOutObjSegment.push(createSegmentObj('', flightType, body, 0));
        } else if (flightType === 1) {
            for (let i = 0; i < 2; i++) {
                createTravelPayOutObjSegment.push(createSegmentObj('', flightType, body, i));
            }

        } else if (flightType === 2) {
            body.m_City.forEach(x => {
                createTravelPayOutObjSegment.push(createSegmentObj(x, flightType, body, 0));
            });
        }


        createTravelPayOutObj = {

            'signature': md5Res,
            'marker': "" + Help.marker,
            'host': Help.host,
            'user_ip': Help.userIp,
            'locale': Help.language,
            'trip_class': tripClass,
            'passengers': {
                'adults': +body.adults,
                'children': +body.children,
                'infants': 0
            },
            'segments': createTravelPayOutObjSegment
        }


        return createTravelPayOutObj;


    } catch (ex) {
        return ex.message;
    }




}


function createSegmentObj(segmentObj, flightType, body, roIndex) {

    try {

        if (flightType === 0) {

            return {
                'origin': body.ro_FromCode,
                'destination': body.ro_ToCode,
                'date': body.ro_Depart
            }

        } else if (flightType === 1) {

            if (roIndex === 0) {
                return {
                    'origin': body.ro_FromCode,
                    'destination': body.ro_ToCode,
                    'date': body.ro_Depart
                }
            } else {
                return {
                    'origin': body.ro_ToCode,
                    'destination': body.ro_FromCode,
                    'date': body.ro_Return
                }
            }
        } else if (flightType === 2) {

            return {
                'origin': segmentObj.m_FromCode,
                'destination': segmentObj.m_ToCode,
                'date': segmentObj.m_date
            }

        }


    } catch (ex) {
        return ex.message;
    }



}

function convertToHoursNMinitues(duration) { //convert minitues to hrs and mis

    let rHours = Math.floor((duration / 60));
    let rMinutes = Math.round(((duration / 60) - rHours) * 60);
    return (rHours + 'h.' + rMinutes);

}


function createNewJson(flightTypeName, isDirect, flightResultSet, cb) {

    let DataSet = [];
    let AgentSet = [];
    let Tickets = [];

    try {


        flightResultSet.forEach((element, mainIndex) => {

            DataSet = [];
            AgentSet = [];
            DataSet[0] = element;



            if (DataSet[0].meta) {


                let agentIds = ((DataSet.map(array => array.meta.gates))[0]).map(x => x.id);
                agentIds.forEach((agentId) => {
                    DataSet.map(array => {
                        let agent = array.gates_info[agentId.toString()];
                        if (agent.id === agentId) {
                            AgentSet.push(creakeAgentObject(agent));
                        }
                    });
                });


                let proposals = DataSet.map(array => array.proposals)[0];

                proposals.forEach((proposal, index) => {

                    let segments_airports = proposal.segments_airports;
                    let agents = [];
                    let flights = [];
                    let SegmentArray = [];
                    let SegmentObject = {};
                    let oneTicket = {};

                    let duration = 0;

                    AgentSet.forEach(agent => {
                        if ((proposal.terms[agent.id])) {
                            agents.push(createTicketAgentObject(agent, proposal.terms[agent.id].price, proposal.terms[agent.id].currency, proposal.terms[agent.id].url));
                        }
                    });


                    let departureTimeTemp;


                    (proposal.segment).forEach((oneSegment, segmentIndex) => {


                        SegmentObject = {};
                        flights = [];
                        duration = 0;


                        let numberOfStops = "";
                        let stopsAirports = "";
                        let transfer = "";
                        let label = "";
                        let arrivalDate = "";
                        let arrivalTime = "";
                        let departureDate = "";
                        let departureTime = "";


                        oneSegment.flight.forEach((flight, flightIndex) => {

                            flights.push(createTicketFlightObject(flight));
                            duration = duration + flight.duration;

                            arrivalDate = flight.arrival_date;
                            arrivalTime = flight.arrival_time;

                            if (flightIndex === 0) {
                                departureDate = flight.departure_date;
                                departureTime = flight.departure_time;
                            }

                        });

                        if (oneSegment.transfers) {
                            numberOfStops = oneSegment.transfers.length;
                            oneSegment.transfers.forEach(transfer => {

                                let flight = flights.filter(flight => flight.arrival === transfer.at);


                                duration = duration + (transfer.duration_seconds / 60); //convert to minitues
                                transfer.airports.forEach(airport => {
                                    stopsAirports = stopsAirports + " " + airport;

                                    if (transfer.airports.length === 1 && flight[0].arrival === airport) {
                                        label = "Connect in Airport";
                                    } else {
                                        label = "Connect in " + stopsAirports + "Airports";
                                    }
                                });

                                let transitObject = {
                                    "duration": convertToHoursNMinitues((transfer.duration_seconds / 60)),
                                    "stopsAirports": stopsAirports,
                                    "label": label
                                }

                                flight[0].transit.push(transitObject);


                            });
                        } else numberOfStops = "Direct";


                        if (numberOfStops != "Direct" && numberOfStops > 1) {
                            numberOfStops = numberOfStops + ' Stops';
                        } else if (numberOfStops != "Direct" && numberOfStops === 1) {
                            numberOfStops = numberOfStops + ' Stop';
                        }
                        transfer = numberOfStops + ',' + stopsAirports;



                        SegmentObject = {
                            "status": flightTypeName,
                            "origin": segments_airports[segmentIndex][0],
                            "destination": segments_airports[segmentIndex][1],
                            "transfer": transfer,
                            "duration": convertToHoursNMinitues(duration),
                            "arrivalDate": arrivalDate,
                            "arrivalTime": arrivalTime,
                            "departureDate": departureDate,
                            "departureTime": departureTime,
                            "Detail": flights,
                        }

                        SegmentArray.push(SegmentObject);




                        departureTimeTemp = departureTime;

                    });



                    if (isDirect === "true") {
                        if (proposal.is_direct === true) {

                            let isSameBool = false;

                            oneTicket = {
                                "Id": mainIndex + 1,
                                "ProposalId": index + 1,
                                "IsDirect": proposal.is_direct,
                                "Agents": agents,
                                "Segment": SegmentArray
                            }


                            if (Tickets.length != 0) {

                                Tickets.forEach((ticket, index) => {


                                    if (ticket.Segment.length === oneTicket.Segment.length) {

                                        let isSame = [];


                                        ticket.Segment.forEach((segment, ii) => {

                                            if (
                                                segment.duration === oneTicket.Segment[ii]['duration'] &&
                                                segment.departureDate === oneTicket.Segment[ii]['departureDate'] && segment.departureTime === oneTicket.Segment[ii]['departureTime'] &&
                                                segment.arrivalDate === oneTicket.Segment[ii]['arrivalDate'] && segment.arrivalTime === oneTicket.Segment[ii]['arrivalTime'] &&
                                                segment.origin === oneTicket.Segment[ii]['origin'] && segment.destination === oneTicket.Segment[ii]['destination']
                                            ) {
                                                isSame.push(true);
                                            } else {
                                                isSame.push(false);
                                            }

                                        });


                                        let status = isSame.filter(x => x === true).length;
                                        if (status === 0) isSameBool = false;
                                        else isSameBool = true;

                                        if (ticket.Segment.length === isSame.filter(x => x === true).length) {


                                            let agentObj = {
                                                "agentId": agents[0].agentId,
                                                "agentName": agents[0].agentName,
                                                "agentTicketPrice": agents[0].agentTicketPrice,
                                                "agentTicketCurrency": agents[0].agentTicketCurrency,
                                                "termsUrl": agents[0].termsUrl
                                            }
                                            ticket.Agents.push(agentObj)
                                        }
                                    }
                                })

                            }




                            if (isSameBool === false) {
                                Tickets.push(oneTicket);
                            }


                        }
                    } else {
                        if (proposal.is_direct === false) {

                            let isSameBool = false;

                            oneTicket = {
                                "Id": mainIndex + 1,
                                "ProposalId": index + 1,
                                "IsDirect": proposal.is_direct,
                                "Agents": agents,
                                "Segment": SegmentArray
                            }


                            if (Tickets.length != 0) {

                                Tickets.forEach((ticket, index) => {


                                    if (ticket.Segment.length === oneTicket.Segment.length) {

                                        let isSame = [];


                                        ticket.Segment.forEach((segment, ii) => {

                                            if (
                                                segment.duration === oneTicket.Segment[ii]['duration'] &&
                                                segment.departureDate === oneTicket.Segment[ii]['departureDate'] && segment.departureTime === oneTicket.Segment[ii]['departureTime'] &&
                                                segment.arrivalDate === oneTicket.Segment[ii]['arrivalDate'] && segment.arrivalTime === oneTicket.Segment[ii]['arrivalTime'] &&
                                                segment.origin === oneTicket.Segment[ii]['origin'] && segment.destination === oneTicket.Segment[ii]['destination']
                                            ) {
                                                isSame.push(true);
                                            } else {
                                                isSame.push(false);
                                            }

                                        });


                                        let status = isSame.filter(x => x === true).length;
                                        if (status === 0) isSameBool = false;
                                        else isSameBool = true;

                                        if (ticket.Segment.length === isSame.filter(x => x === true).length) {


                                            let agentObj = {
                                                "agentId": agents[0].agentId,
                                                "agentName": agents[0].agentName,
                                                "agentTicketPrice": agents[0].agentTicketPrice,
                                                "agentTicketCurrency": agents[0].agentTicketCurrency,
                                                "termsUrl": agents[0].termsUrl
                                            }
                                            ticket.Agents.push(agentObj)
                                        }
                                    }
                                })

                            }




                            if (isSameBool === false) {
                                Tickets.push(oneTicket);
                            }


                        }


                    }





                });

            }



        });

        cb(null, Tickets);

    } catch (ex) {
        Logger.error(JSON.stringify(DataSet[0]));
        Logger.error(JSON.stringify(flightResultSet));
        cb("Error", ex.message);
    }

}


function creakeAgentObject(agent) {
    return {
        id: agent.id,
        name: agent.label,
        currencyCode: agent.currency_code
    }

}

function createTicketAgentObject(agentInfo, price, agentTicketCurrency, termsUrl) {

    return {
        agentId: agentInfo.id,
        agentName: agentInfo.name,
        agentTicketPrice: price,
        agentTicketCurrency: agentTicketCurrency,
        termsUrl: termsUrl
    }


}


function createTicketFlightObject(flightObj) {

    return {

        departure: flightObj.departure,
        departureDate: flightObj.departure_date,
        departureTime: flightObj.departure_time,
        arrival: flightObj.arrival,
        arrivalDate: flightObj.arrival_date,
        arrivalTime: flightObj.arrival_time,
        duration: convertToHoursNMinitues(flightObj.duration),
        flightName: flightObj.aircraft,
        airLine: flightObj.marketing_carrier,
        transit: []
    }


}

//#endregion End Helping methods ------------------------------------------------------------------------------------------------------







//using function declaration - nodejs Http ----------------------------------------------------------

function getFlightSearchDataSet(searchId) {
    console.log('SEARCH ID:', searchId);
    return new Promise((resolve, reject) => {
        setTimeout(() => {
                try {
                    console.log("GET FLIGHT SEARCH CALLED")
                    const options = {
                        hostname: 'api.travelpayouts.com',
                        path: '/v1/flight_search_results?uuid=' + searchId,
                        method: 'GET'
                    };
                    const req = http.request(options, (res) => {
                        var buffer = [],
                            bufsize = 0;

                        res.on('data', (data) => {
                            buffer.push(data);
                            bufsize += data.length;
                        });

                        res.on('end', () => {
                            if (res.statusCode === 200) {

                                var body = Buffer.concat(buffer, bufsize);
                                // console.log((body));
                                resolve(JSON.parse(body));
                                res.resume();

                            }
                        });
                    });

                    req.on('error', (e) => {
                        console.error(`problem with request: ${e.message}`);
                        reject(e.message);
                    });

                    req.end();
                } catch (e) {
                    reject(e.message);
                }
            },
            6000
        );
    });

}





//uing function expression---------------------------------------------------------

const createNewToken = async(ProcessJsonObj) => {

    console.log('PROCESS JSON OBJ:', JSON.stringify(ProcessJsonObj));

    try {
        const resp = await axios.post('http://api.travelpayouts.com/v1/flight_search',
            JSON.stringify(ProcessJsonObj), {
                headers: { 'Content-Type': 'application/json' }
            });

        console.log('GETTING RESULTS:', resp.data.search_id);
        return resp.data;

    } catch (err) {
        console.error(err);
        return err.message;
    }


}







module.exports = router;