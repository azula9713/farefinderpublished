var express = require('express');
var router = express.Router();
var Logger = require('../Logger').Logger;
const axios = require('axios').default;

router.get('/AirLine', async (request, resultSet) => {

    try {
        const resp = await axios.get('http://api.travelpayouts.com/data/en/airlines.json',
            {
                headers: { 'Content-Type': 'application/json' }
            });
        resultSet.send(resp.data);
    } catch (err) {
        Logger.error("Error - /AirLine " + err);
        resultSet.send([]);
    }

});


module.exports = router;