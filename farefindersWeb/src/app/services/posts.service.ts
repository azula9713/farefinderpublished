import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { Post } from "../models/post";
import { environment } from "src/environments/environment";

// const baseUrl = "http://localhost:3001/posts";

@Injectable({
  providedIn: "root",
})
export class PostService {
  private categorySubject: BehaviorSubject<Post>;

  constructor(private http: HttpClient) {}

  public get categoryValue(): Post {
    return this.categorySubject.value;
  }

  create(post: Post) {
    return this.http.post(`${environment.apiUrl}/post/create`, post);
  }

  getAll() {
    return this.http.get<Post[]>(`${environment.apiUrl}/post/all`);
  }

  getById(id: string) {
    return this.http.get<Post>(`${environment.apiUrl}/post/${id}`);
  }

  update(id, params) {
    return this.http.put(`${environment.apiUrl}/post/${id}`, params);
  }

  delete(id: string) {
    return this.http.delete(`${environment.apiUrl}/post/${id}`);
  }

  findByCategory(category) {
    return this.http.get(`${environment.apiUrl}/post/findby/q?category=${category}`);
    return null;
  }
}
